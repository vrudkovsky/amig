﻿using ActimaDbEntities.Entities;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Models.CodesResponseJsonObject;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData;
using AmigScheduleApi.Models.HelpClasses;
using AmigScheduleApi.Services.IServices;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using static ActimaDbEntities.Services.EmployeeDetailsLogic;

namespace AmigScheduleApi.Services.Services
{
    public class MonthPlanningExcelReport : IMonthPlanningExcelReport
    {
        private int CurrentRow { get; set; }
        private ExcelWorksheet Worksheet { get; set; }
        private int MaxColumns { get; set; }
        private readonly IDepartmentLogic _departmentLogic;
        private readonly IEmployeeDetailsLogic _employeeLogic;
        private readonly INormatives _normatives;
        private readonly ITimeTablesLogic _timeTablesLogic;


        public MonthPlanningExcelReport(IDepartmentLogic departmentLogic, IEmployeeDetailsLogic employeeLogic, INormatives normatives, ITimeTablesLogic timeTablesLogic)
        {
            _departmentLogic = departmentLogic;
            _employeeLogic = employeeLogic;
            _normatives = normatives;
            _timeTablesLogic = timeTablesLogic;
        }

        public async Task GenerateMonthPlanExcel(ExcelPackage package, Branch4MonthJsonData branchData,
        DateTime monthDate, int allTimetableId, int groupId, int departmentId)
        {
            var maxMonthDays = DateTime.DaysInMonth(monthDate.Year, monthDate.Month);
            Worksheet = package.Workbook.Worksheets.Add("Month plan");
            CurrentRow = 2;
            await GenerateHeaderAsync(monthDate, allTimetableId, branchData.Code);
            var startRow = CurrentRow;
            int Count = 0;
            foreach (var s in branchData.Departments)
            {
                foreach (var depart in s.Groups)
                {
                    foreach (var emp in depart.EmployeesData)
                    {
                        Count++;
                    }
                }
            }
            GenerateMuzzle(maxMonthDays, monthDate);
            await GenerateContent(branchData, monthDate, allTimetableId, groupId, departmentId);
            //MergeSignatureColumns(maxMonthDays + 2);
            Worksheet.Cells[4, 1, CurrentRow - 1, maxMonthDays + 3].SetBorders(ExcelBorderStyle.Thin);
            // GenerateBorders(startRow, CurrentRow - 1, startColumn, MaxColumns + conclusionLength);
            GeneratePostDataAndSave(package, Count);
        }


        private async Task<string> Header(DateTime monthDate, int timeTableId, string code)
        {
            var monthTranslateName = DateDictionaries.Months.FirstOrDefault(month => month.Key.Equals(monthDate.Month))
                .Value;
            var timeTableName = await _timeTablesLogic.GetTimetableName(timeTableId);
            return $"Філія: {code}. Графік: {timeTableName.ToString()} на {monthTranslateName} {monthDate:yyyy}.";
        }

        private async Task GenerateHeaderAsync(DateTime monthDate, int timeTableId, string code)
        {
            string header = await Header(monthDate, timeTableId, code);
            Worksheet.Cells[CurrentRow, 12, CurrentRow, 18].MergeColoredCellsAndSetValue(header)
                .SetExcelDefaultBorders();
            Worksheet.Cells[CurrentRow, 1].Style.HorizontalAlignment =
                                    ExcelHorizontalAlignment.Left;
            CurrentRow += 2;
        }



        private void GenerateMuzzle(int monthMaxDays, DateTime month)
        {
            Worksheet.Row(4).Height = 25;
            Worksheet.Column(2).Width = 21;
            Worksheet.Column(3).Width = 21;
            var currentColumn = 1;
            Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = "Таб №";
            Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = "П.І.Б.";
            Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = "Посада";

            for (var i = 1; i <= monthMaxDays; i++)
            {
                if (i < 10)
                {
                    if (month.Month < 10)
                        Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = "0" + i + ".0" + month.Month;
                }
                if (i < 10)
                {
                    if (month.Month >= 10)
                        Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = "0" + i + "." + month.Month;
                }
                if (i >= 10)
                {
                    if (month.Month >= 10)
                        Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = i + "." + month.Month;
                }
                if (i >= 10)
                {
                    if (month.Month < 10)
                        Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = i + ".0" + month.Month;
                }
            }
            Worksheet.Cells[CurrentRow, currentColumn].SetBorders(ExcelBorderStyle.Thin).Value = "Всього";
            Worksheet.Column(monthMaxDays + 5).Width = 20;
            Worksheet.Cells[CurrentRow, ++currentColumn].SetBorders(ExcelBorderStyle.Thin).Value = "Перевищення норми\n  (год.)";
            Worksheet.Column(monthMaxDays + 6).Width = 20;
            Worksheet.Cells[CurrentRow, ++currentColumn].SetBorders(ExcelBorderStyle.Thin).Value = "Підпис працівника";
        }

        private async Task GenerateContent(Branch4MonthJsonData branchData,
            DateTime date, int timeTableId, int groupId, int departmentId)
        {
            double ALLHOURS = 0;
            var ALLHOURSOVERTIMEBOYZ = new List<double>();
            var employeesWithShortDays = await _employeeLogic.GetEmployeesIdWithShortDays();
            int numberOfDay = 1;
            var fridays = GetFridaysInMonth(date.Year, date.Month);
            var holidays = await _normatives.GetHolidaysForMonthToIntList(date);
            int maxColumns = DateTime.DaysInMonth(date.Year, date.Month) + 4;
            const int employeeDescriptionStartColumn = 1;
            List<double> DayHours = new List<double>();
            var branchDayHours = new List<List<double>>();
            foreach (var department in branchData.Departments)
            {
                var departmentDayHours = new List<List<double>>();
                var departmentOverTimeHours = new List<double>();
                double departmentSummary = 0;
                CurrentRow++;
                Worksheet.Cells[CurrentRow, 1, CurrentRow, maxColumns + 2].MergeColoredCellsAndSetValue($"Підрозділ {department.Code}").SetBorders(ExcelBorderStyle.Thin);
                Worksheet.Cells[CurrentRow, 1].Style.HorizontalAlignment =
                                ExcelHorizontalAlignment.Left;
                CurrentRow++;
                foreach (var group in department.Groups)
                {
                    var groupOverTimeHours = new List<double>();
                    var groupDayHours = new List<List<double>>();
                    double summaryGroup = 0;
                    if (group.NameGroup != null)
                        Worksheet.Cells[CurrentRow, 1, CurrentRow, maxColumns + 2]
                                .MergeColoredCellsAndSetValue($"Група {group.Code} ({group.NameGroup})").SetBorders(ExcelBorderStyle.Thin);
                    else
                        Worksheet.Cells[CurrentRow, 1, CurrentRow, maxColumns + 2]
                             .MergeColoredCellsAndSetValue("Працівники без групи").SetBorders(ExcelBorderStyle.Thin);
                    Worksheet.Cells[CurrentRow, 1].Style.HorizontalAlignment =
                                ExcelHorizontalAlignment.Left;
                    CurrentRow++;

                    foreach (var employee in group.EmployeesData)
                    {
 
                        double hoursMoreThanWorkingNorm = 0;

                        double dayHours = 0;
                        double summary = 0;
                        numberOfDay = 1;
                        SetEmployeeInfo(employee.TabelNumber, employee.Name, employee.Position, employeeDescriptionStartColumn);
                        for (int i = 4; i < maxColumns; i++)
                        {
                            groupDayHours.Add(new List<double>());
                            groupDayHours[i - 4].Add(0);

                            if (employee.Days[i - 4].UnFlexibleNormatives != null && employee.Days[i - 4].UnFlexibleNormatives.IsOutlet)
                            {
                                Worksheet.Cells[CurrentRow, i, CurrentRow + 1,
                                    i].MergeColoredCellsAndSetValue("Вихідний", Color.ForestGreen);
                            }

                            else if (employee.Days[i - 4].AbsenceCause != null)
                            {
                                Worksheet.Cells[CurrentRow, i, CurrentRow + 1,
                                     i].MergeColoredCellsAndSetValue("Планова неявка", Color.LightBlue);
                                groupDayHours[i - 4].Add(Math.Round(8.0, 1));
                                summary += 8;

                            }
                            else if (employee.Days[i - 4].Intervals != null && employee.Days[i - 4].Intervals[0].ShiftEnd.HasValue && employee.Days[i - 4].Intervals[0].ShiftStart.HasValue)
                            {


                                if (employee.Days[i - 4].Intervals[0].ShiftEnd <= employee.Days[i - 4].Intervals[0].ShiftStart && employee.Days[i - 4].Intervals[0].ShiftStart.Value.Hour != 0)
                                    throw new AggregateException(
                                        "intervalStart start before or in the same time with intervalEnd");


                                var intervalStart = employee.Days[i - 4].Intervals[0].ShiftStart;
                                var intervalEnd = employee.Days[i - 4].Intervals[0].ShiftEnd;
                                Worksheet.Cells[CurrentRow, i].Value = intervalStart?.ToString("HH:mm");
                                Worksheet.Cells[CurrentRow, i].Style.HorizontalAlignment =
                                ExcelHorizontalAlignment.Left;
                                Worksheet.Cells[CurrentRow + 1, i].Value =
                                   intervalEnd?.ToString("HH:mm");
                                Worksheet.Cells[CurrentRow + 1, i].Style.HorizontalAlignment =
                                    ExcelHorizontalAlignment.Left;
                                dayHours = intervalEnd.Value.Subtract(intervalStart.Value).TotalHours;
                                if (employeesWithShortDays.Contains(employee.EmployeeId) && (fridays.Contains(numberOfDay)))
                                {
                                    if (dayHours - 1 > 0)
                                        dayHours -= 1;
                                }
                                if (dayHours > 4)
                                {
                                    if (dayHours == 4.5)
                                        dayHours -= 0.5;
                                    else
                                        dayHours -= 1;
                                }

                                summary += Math.Round(dayHours, 1);
                                if (employee.Days[i - 4].NormOFHoursForDay - dayHours < 0)
                                {
                                    hoursMoreThanWorkingNorm += Math.Round(dayHours - employee.Days[i - 4].NormOFHoursForDay, 1);
                                    groupOverTimeHours.Add(hoursMoreThanWorkingNorm);
                                }

                                groupDayHours[i - 4].Add(Math.Round(dayHours, 1));
                            }
                            else
                            {
                                Worksheet.Cells[CurrentRow, i].Value = "-";
                                Worksheet.Cells[CurrentRow, i].Style.HorizontalAlignment =
                                    ExcelHorizontalAlignment.Left;
                                Worksheet.Cells[CurrentRow + 1, i].Value =
                                  "-";
                                Worksheet.Cells[CurrentRow + 1, i].Style.HorizontalAlignment =
                                    ExcelHorizontalAlignment.Left;
                            }
                            numberOfDay++;
                        }

                        summaryGroup += summary;
                        //Сотрудник
                        
                        if (employee.NormOfHours < summary)
                        {
                            Worksheet.Cells[CurrentRow, maxColumns + 1, CurrentRow + 1, maxColumns + 1].MergeColoredCellsAndSetValue(Math.Round(summary - employee.NormOfHours, 1)).SetBorders(ExcelBorderStyle.Thin);
                            groupOverTimeHours.Add(summary - employee.NormOfHours);
                        }
                        else
                            Worksheet.Cells[CurrentRow, maxColumns + 1, CurrentRow + 1, maxColumns + 1].MergeColoredCellsAndSetValue(hoursMoreThanWorkingNorm).SetBorders(ExcelBorderStyle.Thin);
                        Worksheet.Cells[CurrentRow, maxColumns + 2, CurrentRow + 1, maxColumns + 2].MergeColoredCellsAndSetValue("").SetBorders(ExcelBorderStyle.Thin);

                        Worksheet.Cells[CurrentRow, numberOfDay + 3, CurrentRow + 1, numberOfDay + 3].MergeColoredCellsAndSetValue(summary).SetBorders(ExcelBorderStyle.Thin);
                        Worksheet.Cells[CurrentRow, numberOfDay + 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        
                        groupOverTimeHours.Add(hoursMoreThanWorkingNorm);
                        Worksheet.Cells[CurrentRow, maxColumns + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        CurrentRow += 2;
                    }

                    //Группа
                    departmentSummary += summaryGroup;
                    Worksheet.Cells[CurrentRow, 1, CurrentRow, 3].MergeColoredCellsAndSetValue($"Всього по группі {group.NameGroup}:").SetBorders(ExcelBorderStyle.Thin);
                    Worksheet.Cells[CurrentRow, maxColumns].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                    for (int j = 4; j < maxColumns; j++)
                    {
                        Worksheet.Cells[CurrentRow, j].Value = groupDayHours[j - 4].Aggregate((x, y) => x + y);
                        Worksheet.Cells[CurrentRow, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        departmentDayHours.Add(new List<double>());
                        departmentDayHours[j - 4].Add(groupDayHours[j - 4].Aggregate((x, y) => x + y));
                    }
                    Worksheet.Cells[CurrentRow, maxColumns].Value = summaryGroup;
                    Worksheet.Cells[CurrentRow, maxColumns].SetBorders(ExcelBorderStyle.Thin);
                        Worksheet.Cells[CurrentRow, maxColumns + 1].Value = groupOverTimeHours.Aggregate((x, y) => x + y);
                        Worksheet.Cells[CurrentRow, maxColumns + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    departmentOverTimeHours.Add(groupOverTimeHours.Aggregate((x, y) => x + y));
                    CurrentRow++;
                }
                //Подразделение 
                Worksheet.Cells[CurrentRow, 1, CurrentRow, 3].MergeColoredCellsAndSetValue($"Всього по підрозділу {department.NameDepartment}:").SetBorders(ExcelBorderStyle.Thin);
                for (int j = 4; j < maxColumns; j++)
                {
                    if (departmentDayHours.Count != 0)
                    {
                        Worksheet.Cells[CurrentRow, j].Value = departmentDayHours[j - 4].Aggregate((x, y) => x + y);
                        Worksheet.Cells[CurrentRow, j].SetBorders(ExcelBorderStyle.Thin);
                        Worksheet.Cells[CurrentRow, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        branchDayHours.Add(new List<double>());
                        branchDayHours[j - 4].Add(departmentDayHours[j - 4].Aggregate((x, y) => x + y));
                    }
                }
                Worksheet.Cells[CurrentRow, maxColumns].Value = departmentSummary;
                Worksheet.Cells[CurrentRow, maxColumns ].SetBorders(ExcelBorderStyle.Thin);
                Worksheet.Cells[CurrentRow, maxColumns].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                if(departmentOverTimeHours.Count != 0)
                Worksheet.Cells[CurrentRow, maxColumns + 1].Value = departmentOverTimeHours.Aggregate((x, y) => x + y);
                Worksheet.Cells[CurrentRow, maxColumns + 1].SetBorders(ExcelBorderStyle.Thin);

                Worksheet.Cells[CurrentRow, maxColumns + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                if (departmentOverTimeHours.Count != 0)
                    ALLHOURSOVERTIMEBOYZ.Add(departmentOverTimeHours.Aggregate((x, y) => x + y));
                CurrentRow++;
                ALLHOURS += departmentSummary;
            }


            //Всего
            Worksheet.Cells[CurrentRow, 1, CurrentRow, 3].MergeColoredCellsAndSetValue($"Всього:").SetBorders(ExcelBorderStyle.Thin);
            for (int j = 4; j < maxColumns; j++)
            {
                if (branchDayHours.Count != 0)
                    Worksheet.Cells[CurrentRow, j].Value = branchDayHours[j - 4].Aggregate((x, y) => x + y);
                Worksheet.Cells[CurrentRow, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                Worksheet.Cells[CurrentRow, j].SetBorders(ExcelBorderStyle.Thin);
            }
            Worksheet.Cells[CurrentRow, maxColumns].Value = ALLHOURS;
            Worksheet.Cells[CurrentRow, maxColumns].SetBorders(ExcelBorderStyle.Thin);
            Worksheet.Cells[CurrentRow, maxColumns].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            if (ALLHOURSOVERTIMEBOYZ.Count != 0)
                Worksheet.Cells[CurrentRow, maxColumns + 1].Value = ALLHOURSOVERTIMEBOYZ.Aggregate((x, y) => x + y);
            Worksheet.Cells[CurrentRow, maxColumns + 1].SetBorders(ExcelBorderStyle.Thin);
            Worksheet.Cells[CurrentRow, maxColumns + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            CurrentRow++;
        }


        private void SetEmployeeInfo(decimal tableNumber, string name, string empPosition, int currentColumn)
        {
            Worksheet.Cells[CurrentRow, currentColumn, CurrentRow + 1, currentColumn++].MergeColoredCellsAndSetValue(tableNumber);
            Worksheet.Cells[CurrentRow, currentColumn, CurrentRow + 1, currentColumn++].MergeColoredCellsAndSetValue(name);
            Worksheet.Cells[CurrentRow, currentColumn, CurrentRow + 1, currentColumn++].MergeColoredCellsAndSetValue(empPosition);
        }

        private void GeneratePostDataAndSave(ExcelPackage package, int countEmployees)
        {
            CurrentRow += 1;
            const int column = 1;
            GenerateGraphicInfo(countEmployees, column);
            package.Save();
            package.Stream.Position = 0;
        }

        private void GenerateGraphicInfo(int countEmployees, int column)
        {
            Worksheet.Cells[CurrentRow, column, CurrentRow++, column + 1]
                .MergeColoredCellsAndSetValue($"Всього працівників в звіті: {countEmployees}",
                    horizontalAlignment: ExcelHorizontalAlignment.Right).SetExcelDefaultBorders();

            Worksheet.Cells[CurrentRow, column, CurrentRow, column + 1]
                .MergeColoredCellsAndSetValue("Управляючий:", horizontalAlignment: ExcelHorizontalAlignment.Right)
                .SetExcelDefaultBorders();
            Worksheet.Cells[CurrentRow, column + 2, CurrentRow++, column + 5].Style.Border.Bottom.Style
                = ExcelBorderStyle.Medium;
        }

        private static IEnumerable<int> DaysInMonth(int year, int month)
        {
            DateTime monthStart = new DateTime(year, month, 1);
            var saturdays = Enumerable.Range(0, DateTime.DaysInMonth(year, month))
                .Select(day => monthStart.AddDays(day))
                .Where(date => date.DayOfWeek == DayOfWeek.Saturday)
                .Select(date => date.Day);

            var sundays = Enumerable.Range(0, DateTime.DaysInMonth(year, month))
                .Select(day => monthStart.AddDays(day))
                .Where(date => date.DayOfWeek == DayOfWeek.Sunday)
                .Select(date => date.Day);

            return saturdays.Union(sundays);

        }

        private IEnumerable<int> GetFridaysInMonth(int year, int month)
        {
            DateTime monthStart = new DateTime(year, month, 1);
            return Enumerable.Range(0, DateTime.DaysInMonth(year, month))
                .Select(day => monthStart.AddDays(day))
                .Where(date => date.DayOfWeek == DayOfWeek.Friday)
                .Select(date => date.Day);
        }

    }
}
