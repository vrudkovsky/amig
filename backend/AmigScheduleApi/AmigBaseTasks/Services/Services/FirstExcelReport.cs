using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Models.HelpClasses;
using AmigScheduleApi.Services.IServices;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.Style;
using Log = Serilog.Log;

namespace AmigScheduleApi.Services.Services
{
    public class FirstExcelReport :IFirstExcelReport
    {
        private const int TimeZoneStartColumnNumber = 8;
        private const int TimeZoneEndColumnNumber = 55;

        private string GetAllTimeTableSubTitle(int tableType)
        {
            return tableType == 1 || tableType == 2
                ? "Основний " :"Понаднормовий ";
        }
        private string GetAlltimetableTitle(string name, int timetableType, DateTime date)
        {

            var allTimeTableSubName = timetableType == 1 || timetableType == 3 ? " (базовий)" : string.Empty;
            return $"{GetAllTimeTableSubTitle(timetableType)} графік виходів на {date:dd.MM.yyyyг}{allTimeTableSubName}";
        }

        public string GetAlltimetableName(string name, int timetableType, DateTime date)
        {
            return $"{GetAllTimeTableSubTitle(timetableType)} графік {name} на {date:yyyy-MM-dd}";
        }

        
        private  void SetCellsWidthAndHeight(ExcelWorksheet worksheet)
        {
            worksheet.Column(1).Width = 8.11;
            worksheet.Column(2).Width = 23.12;
            worksheet.Column(3).Width = 35.56;
            worksheet.Column(4).Width = 8.78;
            worksheet.Column(5).Width = 11;
            worksheet.Column(6).Width = 9.44;
            worksheet.Column(7).Width = 11.68;
            const int forLoop = TimeZoneEndColumnNumber - TimeZoneStartColumnNumber;
            for (var i = 0; i <= forLoop; i++)
                worksheet.Column(TimeZoneStartColumnNumber+i).Width = 3.22;
            worksheet.Row(1).Height = 16.20;
            worksheet.Row(2).Height = 15;
            worksheet.Row(3).Height = 23.40;
            worksheet.Row(4).Height = 14.40;
            worksheet.Row(5).Height = 40.80;
        }
        private  void GenerateMuzzle(ExcelWorksheet worksheet, DateTime date, int allTimeTableType, 
            string allTimeTableName )
        {
            SetCellsWidthAndHeight(worksheet);
            worksheet.MergeCellsAndSetValue("a1", "c1",
                $"Графік згенерований : \"{DateTime.Now:yyyy-MM-dd hh:mm:ss} \"");
            worksheet.MergeCellsAndSetValue("m2", "ah2", 
                GetAlltimetableTitle(allTimeTableName,allTimeTableType,date));
            worksheet.MergeCellsAndSetValue("a3", "c3", "Назва графіка :" +
                                                        $" \"{allTimeTableName}\"");
            worksheet.MergeCellsAndSetValue("a4", "a5", "Таб №",
                ExcelVerticalAlignment.Bottom);
            worksheet.MergeCellsAndSetValue("b4", "b5", "П.І.Б.",
                ExcelVerticalAlignment.Bottom);
            worksheet.MergeCellsAndSetValue("c4", "c5", "Посада",
                ExcelVerticalAlignment.Bottom);
            worksheet.MergeCellsAndSetValue("d4", "d5", "Норма годин",
                ExcelVerticalAlignment.Bottom);
            worksheet.MergeCellsAndSetValue("e4", "e5",
                "Заплановано годин", ExcelVerticalAlignment.Bottom);
            worksheet.MergeCellsAndSetValue("f4", "f5", "Початок зміни",
                ExcelVerticalAlignment.Bottom);
            worksheet.MergeCellsAndSetValue("g4", "g5", "Кінець зміни",
                ExcelVerticalAlignment.Bottom);
            var tableHeaderCells = worksheet.Cells["A4:BC5"];
            
            tableHeaderCells.Style.Fill.PatternType = ExcelFillStyle.Solid;
            tableHeaderCells.Style.Fill.BackgroundColor.SetColor(Color.MediumSlateBlue);

            const int maxHourInDay = 24;
            var rowNumber = 4;
            var rowWithMinutesNumber = rowNumber + 1;
            var columnStartNumber = 8;
            var zeroMinutes = 0;
            var thirtyMinutes = 30;
            for (var i = 0; i < maxHourInDay; i++)
            {
                var nextColumnAfterStartNumber = columnStartNumber + 1;
                worksheet.Cells[rowNumber, columnStartNumber, rowNumber, nextColumnAfterStartNumber].Merge = true;
                var startCell = worksheet.Cells[rowNumber, columnStartNumber];
                startCell.Style.Font.Bold = true;
                startCell.Value = i;
                startCell.NormalizeCell();
                var cellWithZeroMinutes = worksheet.Cells[rowWithMinutesNumber, columnStartNumber];                
                cellWithZeroMinutes.Value = zeroMinutes;
                cellWithZeroMinutes.NormalizeCell();
                cellWithZeroMinutes.Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;

                var cellWithThirtyMinutes = worksheet.Cells[rowWithMinutesNumber, nextColumnAfterStartNumber];
                cellWithThirtyMinutes.Value = thirtyMinutes;
                cellWithThirtyMinutes.NormalizeCell();
                cellWithThirtyMinutes.Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;
                columnStartNumber += 2;
            }
        }

        public void CreateExcel(BranchJsonData branchData, ExcelPackage package)
        {
                var worksheet = package.Workbook.Worksheets.Add("Schedule");
                var currentRow = 6;
                var day = branchData.Date;
                const int timeZoneStartColumnNumber = 8;
                const int timeZoneEndColumnNumber = 55;
                const string groupStartLetterHeaderInfo = "D";
                const string groupEndLetterHeaderInfo = "F";
                var alltimeTable = branchData.AllTimetables;
                GenerateMuzzle(worksheet,branchData.Date, alltimeTable.TypeId.Value, alltimeTable.Name);
                double plannedHoursForBranchFromMonthStart = 0;
                var сumulativeIntervalForBranch = new int[48];
                foreach (var department in branchData.Departments)
                {
                    var сumulativeIntervalForDepartment = new int[48];
                    double plannedHoursForDepartmentFromMonthStart = 0;
                    worksheet.Cells[currentRow, 2].Value = $"Підрозділ: {department.Code}";
                    currentRow++;
                    foreach (var group in department.Groups)
                    {
                        worksheet.Row(currentRow).Height = 14.40;
                        worksheet.MergeCellsAndSetValue(groupStartLetterHeaderInfo + currentRow,
                            groupEndLetterHeaderInfo + currentRow,group.NameGroup!=null?$"{group.Code} ({group.NameGroup})":"Працівники без групи");
                        currentRow++;
                        foreach (var employee in group.EmployeesData)
                        {
                            worksheet.Cells[currentRow, 1].Value = employee.TabelNumber;
                            worksheet.Cells[currentRow, 2].Value = employee.Name;
                            worksheet.Cells[currentRow, 3].Value = employee.Position;
                            worksheet.Cells[currentRow, 4].Value = employee.NormOfHours;
                            worksheet.Cells[currentRow, 5].Value = employee.CountHours;
                            worksheet.Row(currentRow).Style.WrapText = true;

                            if (employee.UnFlexibleNormatives != null && employee.UnFlexibleNormatives.IsOutlet)
                            {
                                worksheet.Cells[currentRow, timeZoneStartColumnNumber, currentRow,
                                    timeZoneEndColumnNumber].MergeColoredCellsAndSetValue("Вихідний",Color.Wheat);
                                //if need to comment need to know where and how (cells)
                            }
                            else if (employee.AbsenceCause != null)
                            {
                                worksheet.Cells[currentRow, timeZoneStartColumnNumber, currentRow,
                                    timeZoneEndColumnNumber].MergeColoredCellsAndSetValue("Причина планової неявки : "+
                                        $"{employee.AbsenceCause}",Color.Blue, 
                                        horizontalAlignment: ExcelHorizontalAlignment.Left)
                                    .Style.Font
                                    .Color.SetColor(Color.White);
                            }
                            else if (employee.Intervals?.Any() == true)
                            {
                                worksheet.Cells[currentRow, 6].Value = string.Join('\n',
                                    employee.Intervals.Select(interval =>
                                        interval.ShiftStart?.ToString("HH:mm")));
                                worksheet.Cells[currentRow, 6].Style.HorizontalAlignment =
                                    ExcelHorizontalAlignment.Left;
                                worksheet.Cells[currentRow, 7].Value = string.Join('\n',
                                    employee.Intervals.Select(interval =>
                                        interval.ShiftEnd?.ToString("HH:mm")));
                                worksheet.Cells[currentRow, 7].Style.HorizontalAlignment =
                                    ExcelHorizontalAlignment.Left;
                                var isEmployeeHasUnflexibleSchedule = employee.UnFlexibleNormatives != null;
                                foreach (var interval in employee.Intervals
                                    .Where(interval => interval.ShiftEnd.HasValue && interval.ShiftStart.HasValue))
                                {
                                    var intervalStart = interval.ShiftStart.Value;
                                    var intervalEnd = interval.ShiftEnd.Value;
                                    if (intervalEnd <= intervalStart)
                                        throw new AggregateException(
                                            "intervalStart start before or in the same time with intervalEnd");

                                    var intervalStartColumn = timeZoneStartColumnNumber + (intervalStart.Hour * 2);
                                    if (intervalStart.Minute == 30) intervalStartColumn++;
                                    //-1 из-за разницы между началом и концом интервала, начало "с", а конец "до"
                                    var intervalEndColumn = (timeZoneStartColumnNumber-1) + (intervalEnd.Hour * 2);
                                    switch (intervalEnd.Minute)
                                    {
                                        case 30:
                                            intervalEndColumn++;
                                            break;
                                        case 59 when intervalEnd.Hour == 23:
                                            intervalEndColumn = timeZoneEndColumnNumber;
                                            break;
                                    }
                                    var intervalLength = (intervalEnd - intervalStart);
                                    if(alltimeTable.TypeId != 3 && alltimeTable.TypeId != 4 )
                                        if (intervalLength.Hours >= 5)
                                            intervalLength = intervalLength
                                                .Add(new TimeSpan(0, -1, 0, 0));
                                        else if(intervalLength.Hours == 4 && intervalLength.Minutes == 30)
                                            intervalLength = new TimeSpan(0,4,0,0);
                                        
                                    worksheet.Cells[currentRow, intervalStartColumn, currentRow,intervalEndColumn]
                                        .MergeColoredCellsAndSetValue(intervalLength.ToString("hh\\:mm"),
                                        isEmployeeHasUnflexibleSchedule? Color.Coral:Color.Lime);
                                }
                            }
                            var countIntervals = employee.Intervals?.Count(interval =>
                                interval.ShiftEnd.HasValue &&
                                interval.ShiftStart.HasValue);
                            worksheet.Row(currentRow).Height = 14 * (countIntervals ?? 1);
                            
                            currentRow++;
                        }
                        var plannedHoursForGroupFromMonthStart = group.EmployeesData.Sum(data => 
                                                                                            data.CountHours);
                        var empIntervals = group.EmployeesData.Where(data => data.Intervals != null &&
                                                                             data.Intervals.Any())
                            .SelectMany(data => data.Intervals.Where(interval =>
                                interval.ShiftEnd.HasValue && interval.ShiftStart.HasValue));                          

                        var сumulativeIntervalForGroup = new int[48];
                       
                        for (var i = 0; i < сumulativeIntervalForGroup.Length; i++)
                        {
                            var intervalStart = (double) i / 2;
                            var intervalEnd = (double) (i + 1)/ 2 ;
                            сumulativeIntervalForGroup[i] += empIntervals.Sum(interval =>
                                    (interval.ShiftStart.Value.Hour + (double)interval.ShiftStart.Value.Minute/60 
                                     <=intervalStart &&
                                     interval.ShiftEnd.Value.Hour + (double)interval.ShiftEnd.Value.Minute/60 
                                     >=intervalEnd  || 
                                     interval.ShiftEnd.Value.Hour ==23 && interval.ShiftEnd.Value.Minute==59 )
                                        ? 1
                                        : 0);
                            
                            сumulativeIntervalForDepartment[i] += сumulativeIntervalForGroup[i];
                            сumulativeIntervalForBranch[i] += сumulativeIntervalForGroup[i];
                        };  
                        FillStatisticCell(worksheet, currentRow, "Всього по групі",
                            plannedHoursForGroupFromMonthStart,сumulativeIntervalForGroup.ToList() );                                        
                        plannedHoursForDepartmentFromMonthStart += plannedHoursForGroupFromMonthStart;

                        currentRow++;
                    }
                    FillStatisticCell(worksheet, currentRow, "Всього по підрозділу",
                        plannedHoursForDepartmentFromMonthStart ,сumulativeIntervalForDepartment.ToList(),true); 
                    plannedHoursForBranchFromMonthStart += plannedHoursForDepartmentFromMonthStart;
                    currentRow++;
                }

                FillStatisticCell(worksheet, currentRow, "Всього", plannedHoursForBranchFromMonthStart, 
                    сumulativeIntervalForBranch.ToList(),true);
                var tableTopStartPosition = "A4";
                var tableBottomEndPosition = $"BC{currentRow}";
                worksheet.Cells[tableTopStartPosition + ":" +tableBottomEndPosition].SetBorders(ExcelBorderStyle.Thin);
                package.Save();
                package.Stream.Position = 0;
        }


        public  void FillStatisticCell(ExcelWorksheet worksheet, int currentRow, 
            string typeStatistics, double statisticsData, List<int> cumulativeData, bool boldFont = false)
        {
            worksheet.Row(currentRow).Height = 14;
            var columnForPlannedHourNumber = 5;
            var range = worksheet.Cells[currentRow, 2];
            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
            range.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
            range.Style.Font.Bold = boldFont;
            range.Value = typeStatistics;
            var dataRange = worksheet.Cells[currentRow, columnForPlannedHourNumber];
            dataRange.Value = statisticsData;
            dataRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            for (var i = 0; i < cumulativeData.Count; i++)
                worksheet.Cells[currentRow, TimeZoneStartColumnNumber + i].Value = cumulativeData[i];
        }
    }
}