using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Models.CodesResponseJsonObject;
using AmigScheduleApi.Models.GraphicPerMonth;
using AmigScheduleApi.Models.HelpClasses;
using AmigScheduleApi.Services.IServices;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace AmigScheduleApi.Services.Services
{
    public class SecondExcelReport:ISecondExcelReport
    {
        private int CurrentRow { get; set;}
        private ExcelWorksheet Worksheet { get; set;}
        private int MaxColumns { get; set; }
        private readonly IDepartmentLogic _departmentLogic;
        private readonly IEmployeeDetailsLogic _employeeLogic;

        public SecondExcelReport(IDepartmentLogic departmentLogic, IEmployeeDetailsLogic employeeLogic)
        {
            _departmentLogic = departmentLogic;
            _employeeLogic = employeeLogic;
        }

        public string Header(DateTime monthDate)
        {
            var monthTranslateName = DateDictionaries.Months.FirstOrDefault(month => month.Key.Equals(monthDate.Month))
                .Value;
            return $"Графік виходів на {monthTranslateName} {monthDate:yyyy}";
        }

        private void GenerateHeader(DateTime monthDate)
        {
            Worksheet.Cells[CurrentRow, 12, CurrentRow, 18].MergeColoredCellsAndSetValue(Header(monthDate))
                .SetExcelDefaultBorders();
            CurrentRow += 2;
        }

        private void GenerateMuzzle(int monthMaxDays)
        {
            Worksheet.Column(2).Width = 21;
            var currentColumn = 1;
            Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = "Таб №";
            Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = "П.І.Б.";
            for (var i = 1; i <= monthMaxDays; i++)
                Worksheet.Cells[CurrentRow, currentColumn++].SetBoldStyle().Value = i;
            Worksheet.Cells[CurrentRow, currentColumn].SetBoldStyle().Value = "Всього";
            CurrentRow++;
        }

        public void GenerateExcelPerMouth(ExcelPackage package, EmployeesByDepartment employeesByDepartment,
            DateTime monthDate, string filterDescription)
        {
            Worksheet= package.Workbook.Worksheets.Add("PerMonth");
            const int empInfoColumns = 2;
            var maxMonthDays = monthDate.GetDateWithMonthDate().Day;
            MaxColumns = empInfoColumns + maxMonthDays;
            const int conclusionLength = 1;
            CurrentRow = 2;
            const int startColumn = 1;
            GenerateHeader(monthDate);
            var startRow= CurrentRow;
            GenerateMuzzle(maxMonthDays);
            GenerateContent(employeesByDepartment, maxMonthDays, filterDescription);
            GenerateBorders(startRow, CurrentRow-1, startColumn, MaxColumns + conclusionLength);
            GeneratePostDataAndSave(package, employeesByDepartment.Departments.SelectMany(dep =>dep.Employees).Count());
        }

        private void GeneratePreContent(string name, decimal tabelNumber, int currentColumn)
        {
            Worksheet.Cells[CurrentRow, currentColumn++].Value = tabelNumber;
            SetInformation(name,"Початок зміни","Кінець зміни",
                "План годин",currentColumn);
        }

        private void GenerateContent(EmployeesByDepartment employeesByDepartment, int monthMaxDays, string filtersDescription)
        {
            const int informationAboutDaysStartColumnIndex = 2;
            const int employeeDescriptionStartColumn = 1;
            foreach (var department in employeesByDepartment.Departments)
            {
                if (filtersDescription != null)
                {
                    Worksheet.Cells[CurrentRow, 1, CurrentRow, MaxColumns].MergeColoredCellsAndSetValue(
                        filtersDescription,
                        horizontalAlignment: ExcelHorizontalAlignment.Left);
                    CurrentRow++;
                }
                foreach (var employee in department.Employees   )
                {
                    GeneratePreContent(employee.Name, employee.TabelNumber,employeeDescriptionStartColumn);
                    for (var index = 1; index <= monthMaxDays; index++)
                    {
                        var availableDay  = employee.Days.Where(day => day.Number.Equals(index)).ToList();
                        if (availableDay.Any())
                            SetInformationForDay(availableDay, informationAboutDaysStartColumnIndex + index);
                        else
                            SetDayIsWeekend(informationAboutDaysStartColumnIndex + index);
                    }

                    var conclusionStatisticsRowNumber = informationAboutDaysStartColumnIndex + monthMaxDays + 1;
                    GenerateStatisticForEmployee(employee, conclusionStatisticsRowNumber);
                    CurrentRow += 4;
                }

                Worksheet.Cells[CurrentRow, 1, CurrentRow, 2]
                    .MergeColoredCellsAndSetValue("Всьго" + (filtersDescription == null ? "" : " по "+ filtersDescription),
                        horizontalAlignment: ExcelHorizontalAlignment.Left);
                for (var index = 1; index <= monthMaxDays; index++)
                {
                    var enumerable = department.Employees
                        .SelectMany(data =>data.Days
                            .Where(day =>day.Number.Equals(index))
                        .Select(day =>day.Plan))
                        .Sum();
                    Worksheet.Cells[CurrentRow, 2 + index].MergeColoredCellsAndSetValue(enumerable);
                }
                CurrentRow++;
            }
        }

        private void GenerateStatisticForEmployee(EmployeeAppearanceData employeeData, int cellNumber)
        {
            var daysCount = employeeData.Days.Select(day=>day.Number).Distinct().Count();
            Worksheet.Cells[CurrentRow, cellNumber].MergeColoredCellsAndSetValue(daysCount);
            var hoursCount = employeeData.Days.Where(day=>day.TypeDay=="work").Sum(day=>day.Plan);
            Worksheet.Cells[CurrentRow + 3, cellNumber].MergeColoredCellsAndSetValue(hoursCount);
        }

        private void SetDayIsWeekend(int column)
        {
            Worksheet.Cells[CurrentRow, column].Value = "вд";
        }

        private void SetInformationForDay(IEnumerable<InformationForDay> days, int column)
        {
            days = days.OrderBy(day => day.Start);
            SetInformation("work", days.First().Start, days.Last().End, days.Sum(day=>day.Plan),column);
        }

        private void SetInformation(object firstCell, object secondCell, object thirdCell, object fourthCell, 
            int column)
        {
            var areaCurrentRow = CurrentRow;
            Worksheet.Cells[areaCurrentRow++, column].SetWrapText().Value = firstCell;
            Worksheet.Cells[areaCurrentRow++, column].SetWrapText().Value = secondCell;
            Worksheet.Cells[areaCurrentRow++, column].SetWrapText().Value = thirdCell;
            Worksheet.Cells[areaCurrentRow, column].SetWrapText().Value = fourthCell;
        }
        private void GenerateBorders(int startRow, int endRow, int startColumn, int endColumn)
        {
            Worksheet.Cells[startRow, startColumn, endRow, endColumn].SetBorders(ExcelBorderStyle.Thin);
        }
        private void GeneratePostDataAndSave(ExcelPackage package, int countEmployees)
        {
            CurrentRow+=4;
            const int column = 1;
            GenerateGraphicInfo(countEmployees, column);
            package.Save();
            package.Stream.Position = 0;
        }

        private void GenerateGraphicInfo(int countEmployees, int column)
        {
            Worksheet.Cells[CurrentRow, column,CurrentRow++,column+1]
                .MergeColoredCellsAndSetValue($"Всього в звіті: {countEmployees}",
                    horizontalAlignment:ExcelHorizontalAlignment.Right).SetExcelDefaultBorders();

            Worksheet.Cells[CurrentRow, column,CurrentRow,column+1]
                .MergeColoredCellsAndSetValue("Звіт підготував(ла):",horizontalAlignment:ExcelHorizontalAlignment.Right)
                .SetExcelDefaultBorders();
            Worksheet.Cells[CurrentRow, column + 2, CurrentRow++, column + 5].Style.Border.Bottom.Style 
                = ExcelBorderStyle.Medium;

            Worksheet.Cells[CurrentRow, column,CurrentRow,column+1]
                .MergeColoredCellsAndSetValue("Ознайомлений:",horizontalAlignment:ExcelHorizontalAlignment.Right)
                .SetExcelDefaultBorders();
            Worksheet.Cells[CurrentRow, column + 2, CurrentRow++, column + 5].Style.Border.Bottom.Style 
                = ExcelBorderStyle.Medium;
        }

        public async Task<string> GetFiltersDescription(string departmentCode, int groupId)
        {
            if ((departmentCode == null || departmentCode == "0") && groupId == 0) return null;
            var strToReturn = string.Empty;
            if (groupId > 0)
            {
                var groups = await _employeeLogic.GetDictionaryForGroupNamesWoTracking();
                var group = groups.FirstOrDefault(pair => pair.Key.Equals(groupId));
                strToReturn += $"Група {group.Value} ";
            }

            if (departmentCode == null || departmentCode == "0")
            {
                strToReturn += $"Пiдроздiл {departmentCode}";
            }
            return strToReturn.Trim();
        }
    }
}