using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData;
using AmigScheduleApi.Services.IServices;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using static ActimaDbEntities.Services.EmployeeDetailsLogic;

namespace AmigScheduleApi.Services.Services
{
    public class EmployeeResponseObjectLogic : IEmployeeResponseObjectLogic
    {
        private const int NormOfHoursForDay = 8;
        private readonly INormatives _normatives;
        private readonly ITimeTablesLogic _timeTablesLogic;
        private readonly IEmployeeDetailsLogic _employeeDetailsLogic;

        public EmployeeResponseObjectLogic(INormatives normatives, ITimeTablesLogic timeTablesLogic, IEmployeeDetailsLogic employeeDetailsLogic)
        {
            _normatives = normatives;
            _timeTablesLogic = timeTablesLogic;
            _employeeDetailsLogic = employeeDetailsLogic;
        }

        private string GenerateEmployeeFullName(EmployeesDetails employees)
        {
            return $"{employees.LastName} {employees.FirstName.Substring(0, 1).ToUpper()}." +
                   $"{employees.MiddleName.Substring(0, 1).ToUpper()}.";
        }
        public EmployeeData SetDataFromEmployee(EmployeesDetails employees, int groupKey, double flexibleHoursToCurrentDate,
            double countFlexibleHoursFromYearStart, bool shortFriday, bool shortPreHoliday, double countFlexibleHoursToEndDate)
        {
            var normatives = employees.WorkTimeNormatives.FirstOrDefault();
            var shiftRegime = employees.EmployeesDetailsAstu?.ShiftRegime;
            var isntAbsence = employees.Employee.AbsenceSchedule == null || !employees.Employee.AbsenceSchedule.Any();
            var employeeData = new EmployeeData
            {
                EmployeeId = employees.EmployeeId,
                Name = GenerateEmployeeFullName(employees),
                ShiftEnabled = shiftRegime != null,
                TabelNumber = employees.Employee.AccountNumber,
            };
            if (shiftRegime == null)
                return employeeData;

            employeeData.GroupId = groupKey;
            employeeData.UnFlexibleNormatives = shiftRegime?.FlexibleSchedule == 0 && isntAbsence
                ? new UnFlexibleNormatives
                {
                    IsOutlet = shiftRegime.Shifts.Exists(shifts => shifts.ShiftNumber == 0),
                }
                : null;
            //cюда кладется либо интервалы с наших таблиц, при гибком, либо null при их отсутствии
            //ибо интервалы с их таблиц при не гибком, либо null при выходных (shifts.ShiftNumber != 0)
            employeeData.Intervals =
                shiftRegime?.FlexibleSchedule == 1 && employees.Employee.Timetable.Any() && isntAbsence
                    ? employees.Employee.Timetable.Select(timeTable => new Interval
                    {
                        ShiftStart = timeTable.ShiftStart,
                        ShiftEnd = timeTable.ShiftEnd
                    }).ToList()
                    : shiftRegime?.FlexibleSchedule == 0 && shiftRegime.Shifts.Any() &&
                      shiftRegime.Shifts.All(shifts => shifts.ShiftNumber != 0)
                        ? shiftRegime.Shifts.Select(shifts => new Interval
                        {
                            ShiftStart = shifts.ShiftStart,
                            ShiftEnd = shifts.ShiftEnd
                        }).ToList()
                        : null;

                employeeData.AbsenceCause =
                !isntAbsence ? employees.Employee.AbsenceSchedule?.FirstOrDefault()?.AbsenceCause.Name : null;
            employeeData.NormOfHours = normatives?.NormHours ?? 0;
            employeeData.NormOfDays = normatives?.NormDays ?? 0;
            employeeData.NormOFHoursForDay =
                NormOfHoursForDay - (shortFriday && shiftRegime.ShiftRegimeDetails.ShortFriday.Equals(1) ||
                                     shortPreHoliday && shiftRegime.ShiftRegimeDetails.ShortPreHoliday.Equals(1)
                    ? 1
                    : 0);
            employeeData.Position = employees.Profession.Name;
            employeeData.CountHours = flexibleHoursToCurrentDate;
            employeeData.CountHoursFromStartYear = countFlexibleHoursFromYearStart;
            employeeData.CountHoursToMonthEnd = countFlexibleHoursToEndDate;
            return employeeData;
        }

        public async Task<Employees4MonthJsonData> SetDataFromEmployee(EmployeesDetails employees, int groupKey,
            double countHours, double countOvertimeHours, DateTime monthDate,
            List<int> holidaysDaysNumbers, List<Tuple<int, int>> employeeAbsenceList)
        {
            var monthEndDate = monthDate.Date.AddMonths(1).AddDays(-monthDate.Day);
            var shiftRegime = employees.EmployeesDetailsAstu?.ShiftRegime;
            var normatives = employees.WorkTimeNormatives.FirstOrDefault();
            var unFlexibleNormatives = shiftRegime?.FlexibleSchedule == 0;
            List<EmployeeShiftData> employeeUnflexibleShift = null;

            var employeeMonthAbsenceSchedule = employeeAbsenceList.Where(dick => dick.Item1 == employees.EmployeeId).Select(dictionary => dictionary.Item2).ToList();


            if (unFlexibleNormatives)
            {
                employeeUnflexibleShift =
                    await _timeTablesLogic.GetUnflexibleEmployeeShift(employees.EmployeeId, monthDate.Month);
            }



            var employeeData = new Employees4MonthJsonData()
            {
                EmployeeId = employees.EmployeeId,
                IsPreHolidayDayAvailable = shiftRegime?.ShiftRegimeDetails != null &&
                                           shiftRegime.ShiftRegimeDetails.ShortPreHoliday.Equals(1),
                IsShortFridayDayAvailable = shiftRegime?.ShiftRegimeDetails != null &&
                                            shiftRegime.ShiftRegimeDetails.ShortFriday.Equals(1),
                NormOfDays = normatives?.NormDays ?? default,
                NormOfHours = normatives?.NormHours ?? default,
                Name = GenerateEmployeeFullName(employees),
                OvertimeForCurrentYear = countOvertimeHours,
                Position = employees.Profession.Name,
                Comment = "",
                TabelNumber = employees.Employee.AccountNumber,
                CountHours = countHours,
                GroupId = groupKey,
                Days = new List<Days4MonthJsonData>()
            };
                
            for (var i = 1; i <= monthEndDate.Day; i++)
            {
                bool isOutlet = false;
                List<Interval> intervalsList = new List<Interval>();

                var isntAbsence = !employeeMonthAbsenceSchedule.Contains(i) ? true : false;

                if (!(employeeUnflexibleShift is null))
                {
                    intervalsList = employeeUnflexibleShift.Where(shift => shift.ShiftDate != null && shift.ShiftDate.Value.Day.Equals(i)).Select(shifts => new Interval { ShiftEnd = shifts.ShiftEnd, ShiftStart = shifts.ShiftStart }).ToList();
                }
                    
                var currentDate = new DateTime(monthDate.Year, monthDate.Month, i);
                var day = new Days4MonthJsonData
                {
                    DayNumber = i,
                    AbsenceCause = employees.Employee.AbsenceSchedule
                        .FirstOrDefault(abs => abs.ScheduleDate == currentDate)?
                        .AbsenceCause.Name,
                    ShiftEnabled = employees.StartDate.Date <= currentDate && currentDate <= employees.EndDate.Date && shiftRegime != null,
                    NormOFHoursForDay = NormOfHoursForDay -
                        (employeeData.IsShortFridayDayAvailable && currentDate.DayOfWeek == DayOfWeek.Friday
                        || employeeData.IsShortFridayDayAvailable && holidaysDaysNumbers.Contains(currentDate.Day)
                            ? 1 : 0),
                    UnFlexibleNormatives = unFlexibleNormatives
                        ? new UnFlexibleNormatives
                        {
                            IsOutlet = shiftRegime.Shifts.Exists(shifts => shifts.ShiftNumber == 0) 
                        }
                        : null,
                    Intervals = !unFlexibleNormatives && employees.Employee.Timetable.Any(timetable => timetable.Date.Day == i)
                            ? employees.Employee.Timetable.Where(timetable => timetable.Date.Day == i).
                                Select(timeTable => new Interval
                                {
                                    ShiftStart = timeTable.ShiftStart,
                                    ShiftEnd = timeTable.ShiftEnd
                                })
                                .ToList()
                            : unFlexibleNormatives && intervalsList.Count != 0 && isntAbsence && !isOutlet
                                ? intervalsList.Select(shifts => new Interval
                                {
                                    ShiftStart = intervalsList[0].ShiftStart,
                                    ShiftEnd = intervalsList[0].ShiftEnd
                                }).ToList()
                                : null
                };
                employeeData.Days.Add(day);
            }

            return employeeData;
        }



    }
}