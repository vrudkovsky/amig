using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ActimaDbEntities.Exceptions;
using AmigScheduleApi.Models.HelpClasses;
using AmigScheduleApi.Models.PlanFactReport;
using AmigScheduleApi.Services.IServices;
using Microsoft.EntityFrameworkCore.Internal;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.Style;

namespace AmigScheduleApi.Services.Services
{
    public class ThirdExcelReport : IThirdExcelReport
    {
        private const int MaxCell = 20;

        public string BaseGraphicName(DateTime mouthDate)
        {
            return $"Звіт про відпрацьований час за період з " +
                   $"{mouthDate:yyyy-M-d} по {mouthDate.GetDateWithMonthDate():yyyy-M-d}";
        }

        public string UnwrapExcelName(DateTime mouthDate, string employeeName)
        {
            return UnwrapGraphicName(mouthDate, employeeName).Replace(".", "");
        }
        private string UnwrapGraphicName(DateTime mouthDate, string employeeName)
        {
            return $"Розгорнутий звіт про відпрацьований час за період з " +
                   $"{mouthDate:yyyy-M-d} по {mouthDate.AddMonths(1).AddDays(-1):yyyy-M-d} для {employeeName}";
        }

        private void GenerateBaseGraphicName(ExcelWorksheet worksheet, DateTime mouth, bool isUnwrap = false,
            string employeeName = null)
        {
            if (isUnwrap)
            {
                const int unWrapOffset = 2;
                worksheet.Cells[2, 7 - unWrapOffset, 2, 13 + unWrapOffset].MergeColoredCellsAndSetValue(
                        UnwrapGraphicName(mouth, employeeName)).SetExcelDefaultBorders();
            }
            else
            {
                worksheet.Cells[2, 7, 2, 13].MergeColoredCellsAndSetValue(BaseGraphicName(mouth))
                    .SetExcelDefaultBorders();
            }
        }
        
        private int GenerateMuzzle(ExcelWorksheet worksheet)
        {
            worksheet.Row(4).Height = 19.2;
            worksheet.Row(5).Height = 19.2;
            worksheet.Row(6).Height = 30;
            worksheet.Row(7).Height = 33;
            worksheet.Column(20).Width = 13.33;
            worksheet.Column(1).Width = 8.11;
            worksheet.Column(2).Width = 22.4;
            worksheet.Column(3).Width = 34.67;
            worksheet.Column(4).Width = 16.9;
            worksheet.Cells[4, 1, 7, 1].MergeColoredCellsAndSetValue("Taб.№", isBoldFont: true);
            worksheet.Cells[4, 2, 7, 2].MergeColoredCellsAndSetValue("П.І.Б.", isBoldFont: true);
            worksheet.Cells[4, 3, 7, 3].MergeColoredCellsAndSetValue("Посада", isBoldFont: true);
            worksheet.Cells[4, 4, 7, 4].MergeColoredCellsAndSetValue("Дата", isBoldFont: true);
            worksheet.Cells[4, 5, 4, 11].MergeColoredCellsAndSetValue("Відпрацьовано часу, год", isBoldFont: true);
            worksheet.Cells[5, 5, 7, 5].MergeColoredCellsAndSetValue("Норма по графіку", textRotation: 90,
                isBoldFont: true);
            worksheet.Cells[5, 6, 7, 6].MergeColoredCellsAndSetValue("Фактично відпрацьовано по графіку",
                textRotation: 90, isBoldFont: true);
            worksheet.Cells[5, 7, 7, 7].MergeColoredCellsAndSetValue("Недопрацювання", textRotation: 90,
                isBoldFont: true);
            worksheet.Cells[5, 8, 7, 8].MergeColoredCellsAndSetValue("Перепрацювання", textRotation: 90,
                isBoldFont: true);
            worksheet.Cells[5, 9, 7, 9].MergeColoredCellsAndSetValue("Всього відпрацьовано", textRotation: 90,
                isBoldFont: true);
            worksheet.Cells[5, 10, 7, 10].MergeColoredCellsAndSetValue("В т.ч. нічні години", textRotation: 90,
                isBoldFont: true);
            worksheet.Cells[5, 11, 7, 11].MergeColoredCellsAndSetValue("В т.ч. cвяткові дні", textRotation: 90,
                isBoldFont: true);
            worksheet.Cells[4, 12, 5, 15].MergeColoredCellsAndSetValue("Узгоджена відсутність на робочому місці в " +
                                                                       "робочий час", isBoldFont: true);
            worksheet.Cells[6, 12, 6, 13].MergeColoredCellsAndSetValue("з оплатою", isBoldFont: true);
            worksheet.Cells[6, 14, 6, 15].MergeColoredCellsAndSetValue("без оплати", isBoldFont: true);
            worksheet.Cells[7, 12, 7, 12]
                .MergeColoredCellsAndSetValue("к-сть", isBoldFont: true, textRotation: 90);
            worksheet.Cells[7, 13, 7, 13]
                .MergeColoredCellsAndSetValue("час", isBoldFont: true, textRotation: 90);
            worksheet.Cells[7, 14, 7, 14]
                .MergeColoredCellsAndSetValue("к-сть", isBoldFont: true, textRotation: 90);
            worksheet.Cells[7, 15, 7, 15]
                .MergeColoredCellsAndSetValue("час", isBoldFont: true, textRotation: 90);
            worksheet.Cells[4, 16, 6, 17].MergeColoredCellsAndSetValue("Запізнення", isBoldFont: true);
            worksheet.Cells[7, 16, 7, 16]
                .MergeColoredCellsAndSetValue("к-сть", isBoldFont: true, textRotation: 90);
            worksheet.Cells[7, 17, 7, 17]
                .MergeColoredCellsAndSetValue("час", isBoldFont: true, textRotation: 90);
            worksheet.Cells[4, 18, 6, 19]
                .MergeColoredCellsAndSetValue("Дострокове закінчення роботи", isBoldFont: true);
            worksheet.Cells[7, 18, 7, 18]
                .MergeColoredCellsAndSetValue("к-сть", isBoldFont: true, textRotation: 90);
            worksheet.Cells[7, 19, 7, 19]
                .MergeColoredCellsAndSetValue("час", isBoldFont: true, textRotation: 90);
            worksheet.Cells[4, 20, 7, 20].MergeColoredCellsAndSetValue("Неявки по нез'ясованим причинам, год",
                isBoldFont: true);
            worksheet.Cells[4, 1, 7, 20].SetBorders(ExcelBorderStyle.Thin);
            return 8;// \_(`0_o)_/

        }

        private void GenerateEmployeeIdentifierData(ExcelWorksheet worksheet, int currentRow,
            InfoEmployeesinDepartment employee)
        {
            worksheet.Cells[currentRow, 1].Value = employee.TaxNumber;
            worksheet.Cells[currentRow, 2].Value = employee.Name;
            worksheet.Cells[currentRow, 3].Value = employee.Profession;
            worksheet.Cells[currentRow, 4].Value = employee.Date;
        }
        private void GenerateEmployeeBaseInformation(ExcelWorksheet worksheet, int currentRow,
            InfoEmployeesinDepartment employee, bool generateEmployeeIdentifier)
        {
            if(generateEmployeeIdentifier)
                GenerateEmployeeIdentifierData(worksheet,currentRow,employee);
            worksheet.Cells[currentRow, 5].Value = employee.GraphicNorma;
            worksheet.Cells[currentRow, 6].Value = employee.FactWork;
            worksheet.Cells[currentRow, 7].Value = employee.Flaws;
            worksheet.Cells[currentRow, 8].Value = employee.OverTime;
            worksheet.Cells[currentRow, 9].Value = employee.All;
            worksheet.Cells[currentRow, 10].Value = employee.NightTime;
            worksheet.Cells[currentRow, 11].Value = employee.HolidayTime;
            worksheet.Cells[currentRow, 12].Value = employee.Numberabspay;
            worksheet.Cells[currentRow, 13].Value = employee.Timeabspay;
            worksheet.Cells[currentRow, 14].Value = employee.Numberabsnopay;
            worksheet.Cells[currentRow, 15].Value = employee.Timeabsnopay;
            worksheet.Cells[currentRow, 16].Value = employee.Late;
            worksheet.Cells[currentRow, 17].Value = employee.TimeLate;
            worksheet.Cells[currentRow, 18].Value = employee.Early;
            worksheet.Cells[currentRow, 19].Value = employee.TimeEarly;
            worksheet.Cells[currentRow, 20].Value = employee.TimeAbsence;
        }
        private int GenerateContent(ExcelWorksheet worksheet, List<Departments> data, int startRow)
        {
            var currentRow = startRow;
            foreach (var department in data)
            {
                worksheet.Cells[currentRow, 2].Value = $"Відділ {department.code}";
                currentRow++;
                foreach (var employee in department.Employees)
                {
                    GenerateEmployeeBaseInformation(worksheet, currentRow, employee, true);
                    currentRow++;
                }

                GenerateDepartmentCountedData(worksheet, department.Employees, currentRow, department.code);
                currentRow++;
            }

            worksheet.Cells[currentRow, 1, currentRow, 4].MergeColoredCellsAndSetValue($"Всього у звіті :",
                Color.Yellow,horizontalAlignment: ExcelHorizontalAlignment.Left);
            
            worksheet.Cells[currentRow, 5].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.GraphicNorma));
            worksheet.Cells[currentRow, 6].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.FactWork));
            worksheet.Cells[currentRow, 7].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.Flaws));
            worksheet.Cells[currentRow, 8].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.OverTime));
            worksheet.Cells[currentRow, 9].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.All));
            worksheet.Cells[currentRow, 10].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.NightTime));
            worksheet.Cells[currentRow, 11].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.HolidayTime));
            worksheet.Cells[currentRow, 12].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.Numberabspay));
            worksheet.Cells[currentRow, 13].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.Timeabspay));
            worksheet.Cells[currentRow, 14].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.Numberabsnopay));
            worksheet.Cells[currentRow, 15].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.Timeabsnopay));
            worksheet.Cells[currentRow, 16].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.Late));
            worksheet.Cells[currentRow, 17].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.TimeLate));
            worksheet.Cells[currentRow, 18].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.Early));
            worksheet.Cells[currentRow, 19].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.TimeEarly));
            worksheet.Cells[currentRow, 20].Value = data.Sum(departments => departments.Employees
                .Sum(employee => employee.TimeAbsence));
            worksheet.Cells[startRow, 1, currentRow, MaxCell].SetBorders(ExcelBorderStyle.Thin);
            currentRow++;
            return currentRow;
        }
        private int GenerateSubDataPerDaysForEmp(ExcelWorksheet worksheet,InfoEmployeesinDepartment data, int currentRow,
            string departmentCode)
        {
            worksheet.Cells[currentRow, 2].Value = $"Відділ {departmentCode}";
            var startRow = currentRow;
            GenerateEmployeeBaseInformation(worksheet, currentRow, data,true);
            currentRow++;
            foreach (var employee in data.infoDays)
            {
                worksheet.Cells[currentRow, 4].Value = employee.Date.ToString("yyyy-M-d");
                worksheet.Cells[currentRow, 5].Value = employee.GraphicNorma;
                worksheet.Cells[currentRow, 6].Value = employee.FactWork;
                worksheet.Cells[currentRow, 7].Value = employee.Flaws;
                worksheet.Cells[currentRow, 8].Value = employee.OverTime;
                worksheet.Cells[currentRow, 9].Value = employee.All;
                worksheet.Cells[currentRow, 10].Value = employee.NightTime;
                worksheet.Cells[currentRow, 11].Value = employee.HolidayTime;
                worksheet.Cells[currentRow, 12].Value = employee.Numberabspay;
                worksheet.Cells[currentRow, 13].Value = employee.Timeabspay;
                worksheet.Cells[currentRow, 14].Value = employee.Numberabsnopay;
                worksheet.Cells[currentRow, 15].Value = employee.Timeabsnopay;
                worksheet.Cells[currentRow, 16].Value = employee.Late;
                worksheet.Cells[currentRow, 17].Value = employee.TimeLate;
                worksheet.Cells[currentRow, 18].Value = employee.Early;
                worksheet.Cells[currentRow, 19].Value = employee.TimeEarly;
                worksheet.Cells[currentRow, 20].Value = employee.TimeAbsence;
                currentRow++;
            }
            worksheet.Cells[startRow, 1, currentRow-1, MaxCell].SetBorders(ExcelBorderStyle.Thin);
            return currentRow;
        }

        private void GenerateDepartmentCountedData(ExcelWorksheet worksheet,List<InfoEmployeesinDepartment> employees,
            int currentRow,string departmentCode)
        {
                worksheet.Cells[currentRow, 2, currentRow, 4]
                    .MergeColoredCellsAndSetValue($"Всього по відділу {departmentCode} :", Color.Yellow,
                        horizontalAlignment: ExcelHorizontalAlignment.Left);
                worksheet.Cells[currentRow, 5].Value = employees.Sum(employee => employee.GraphicNorma);
                worksheet.Cells[currentRow, 6].Value = employees.Sum(employee => employee.FactWork);
                worksheet.Cells[currentRow, 7].Value = employees.Sum(employee => employee.Flaws);
                worksheet.Cells[currentRow, 8].Value = employees.Sum(employee => employee.OverTime);
                worksheet.Cells[currentRow, 9].Value = employees.Sum(employee => employee.All);
                worksheet.Cells[currentRow, 10].Value = employees.Sum(employee => employee.NightTime);
                worksheet.Cells[currentRow, 11].Value = employees.Sum(employee => employee.HolidayTime);
                worksheet.Cells[currentRow, 12].Value = employees.Sum(employee => employee.Numberabspay);
                worksheet.Cells[currentRow, 13].Value = employees.Sum(employee => employee.Timeabspay);
                worksheet.Cells[currentRow, 14].Value = employees.Sum(employee => employee.Numberabsnopay);
                worksheet.Cells[currentRow, 15].Value = employees.Sum(employee => employee.Timeabsnopay);
                worksheet.Cells[currentRow, 16].Value = employees.Sum(employee => employee.Late);
                worksheet.Cells[currentRow, 17].Value = employees.Sum(employee => employee.TimeLate);
                worksheet.Cells[currentRow, 18].Value = employees.Sum(employee => employee.Early);
                worksheet.Cells[currentRow, 19].Value = employees.Sum(employee => employee.TimeEarly);
                worksheet.Cells[currentRow, 20].Value = employees.Sum(employee => employee.TimeAbsence);
        }
        private void GenerateUserInfoAndWithDate(ExcelWorksheet worksheet, string userName, int row)
        {
            worksheet.Cells[row, 1, row, 4]
                .MergeColoredCellsAndSetValue($"Звіт підготував(ла): {userName}", Color.White,
                    horizontalAlignment: ExcelHorizontalAlignment.Left)
                .SetExcelDefaultBorders();

            worksheet.Cells[row, 6, row, 7]
                .MergeColoredCellsAndSetValue(DateTime.Now.ToString("yyyy-M-d hh:mm:ss"), Color.White)
                .SetExcelDefaultBorders();
        }

        public void GenerateExcel(List<Departments> data, DateTime mouthDate,ExcelPackage package, string userName)
        {
            var (worksheet,endRow)= SetWorksheetAndMuzzle(data,package,mouthDate);
            GenerateBaseGraphicName(worksheet,mouthDate);
            endRow = GenerateContent(worksheet, data, endRow);
            GeneratePostDataAndSave(package, endRow, worksheet, userName);
        }
        
        public string GenerateExcelForSingleEmpWitUnwrap(List<Departments> data, DateTime mouthDate, ExcelPackage package,
            string userName, string employeeTaxNumber)
        {
            var code = data.FirstOrDefault()?.code;
            var subData =
                data.FirstOrDefault(departments => departments.Employees
                        .Any(emp => emp.TaxNumber == employeeTaxNumber))?
                    .Employees
                    .FirstOrDefault();
            if (subData == null)
                throw new NoContentException();
            var (worksheet,endRow) = SetWorksheetAndMuzzle(data,package,mouthDate);
            GenerateBaseGraphicName(worksheet,mouthDate,true,subData.Name);
            endRow = GenerateSubDataPerDaysForEmp(worksheet, subData, endRow,code);
            endRow = GenerateAbsoluteCountedDataForEmployee(worksheet, endRow, subData, code);
            GeneratePostDataAndSave(package, endRow, worksheet, userName);
            return subData.Name;

        }

        private int GenerateAbsoluteCountedDataForEmployee(ExcelWorksheet worksheet, int currentRow, 
            InfoEmployeesinDepartment employee, string departmentCode)
        {
            var startRow = currentRow;
            worksheet.Cells[currentRow, 2, currentRow, 4]
                .MergeColoredCellsAndSetValue($"Всього по відділу {departmentCode} :", Color.Yellow,
                    horizontalAlignment: ExcelHorizontalAlignment.Left);
            GenerateEmployeeBaseInformation(worksheet,currentRow,employee,false);
            currentRow++;
            worksheet.Cells[currentRow, 1, currentRow, 4].MergeColoredCellsAndSetValue($"Всього у звіті :",
                Color.Yellow, horizontalAlignment: ExcelHorizontalAlignment.Left);
            GenerateEmployeeBaseInformation(worksheet,currentRow,employee,false);
            worksheet.Cells[startRow, 1, currentRow, MaxCell].SetBorders(ExcelBorderStyle.Thin);
            return ++currentRow;
        }
        
        
        private (ExcelWorksheet Worksheet,int EndRow) SetWorksheetAndMuzzle(List<Departments> data,ExcelPackage package, 
            DateTime mouthDate)
        {
            var worksheet = package.Workbook.Worksheets.Add("PlanFactor");
            var endRow = GenerateMuzzle(worksheet);
            return (worksheet, endRow);
        }

        private void GeneratePostDataAndSave(ExcelPackage package, int endRow, ExcelWorksheet worksheet, string userName)
        {
            endRow+=2;
            GenerateUserInfoAndWithDate(worksheet,userName,endRow);
            package.Save();
            package.Stream.Position = 0;
        } 
    }
}