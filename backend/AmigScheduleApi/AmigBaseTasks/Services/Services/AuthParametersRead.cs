using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Services.IServices;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Linq;

namespace AmigScheduleApi.Services.Services
{
    public class AuthParametersRead: IAuthParametersRead
    {
        public string BranchIdParamName =>"branchId";
        public string AllTimetableName => "allTimetableId";
        public string DepartmentIdParamName => "departmentId";
        public  string DepartmentCodeParameter => "departmentCode";
        
        private readonly ITimeTablesLogic _timeTables;
        private readonly IDepartmentLogic _departmentLogic;

        public AuthParametersRead(IDepartmentLogic departmentLogic, ITimeTablesLogic timeTables)
        {
            _departmentLogic = departmentLogic;
            _timeTables = timeTables;
        }

        private  async Task<(int? BranchID, int? DepartmentId)> 
            GetDataAboutDepartmentAndBranchFromQueryString(AuthorizationFilterContext context)
        {
            int? availableBranchIdFromRequest = null;
            int? availableDepartmentFromRequest= null;
            var request = context.HttpContext.Request;
            var queryParams = request.Query;
            if (queryParams.ContainsKey(AllTimetableName))
            {
                var alltimeTableId = int.Parse(queryParams[AllTimetableName]);
                var allTimeTable = await _timeTables.GetAllTimeTables(alltimeTableId);
                if(allTimeTable!=null)
                    availableBranchIdFromRequest = allTimeTable.BranchId;
            }
            else
            {
                var queryBranchId = int.Parse(queryParams[BranchIdParamName]);
                availableBranchIdFromRequest = queryBranchId;
            }

            if (queryParams.ContainsKey(DepartmentIdParamName))
            {
                var queryDepartmentId = int.Parse(queryParams[DepartmentIdParamName]);
                if (queryDepartmentId != 0)
                    availableDepartmentFromRequest = queryDepartmentId;
            }
            else if (queryParams.ContainsKey(DepartmentCodeParameter)) 
            {
                var queryDepartmentCode = queryParams[DepartmentCodeParameter];
                if (!queryDepartmentCode.Equals("0"))
                {
                    var departmentFromQuery = await _departmentLogic.GetDepartmentByCode(queryDepartmentCode);
                    availableDepartmentFromRequest = departmentFromQuery.DepartmentId;
                }
            }

            return (availableBranchIdFromRequest, availableDepartmentFromRequest);
        }

        private async Task<(int? BranchID, int? DepartmentId)>
            GetDataAboutDepartmentAndBranchFromBody(AuthorizationFilterContext context)
        {
            string bodyJsonSchema;
            var request = context.HttpContext.Request;
            request.EnableRewind();
            using (var reader = new StreamReader(request.Body, Encoding.UTF8,
                true, 1024, true))
            {
                bodyJsonSchema = await reader.ReadToEndAsync();
            }
            //For now if some one made mistake in parameters naming as private constant value from this. class 
            if (string.IsNullOrEmpty(bodyJsonSchema)) return (null,null);
            /*
            For further modifications
            if (string.IsNullOrEmpty(bodyJsonSchema))
            {
                SetForbiddenStatusCodeAndAuthHeaderData(context, "Request fas forbidden due json body " +
                        "doesn't have data to read and Query string haven't any available parameters ti check");
            }
            */
            int? availableBranchIdFromRequest =null;
            int? availableDepartmentFromRequest = null;

            request.Body.Position = 0;
            var bodyJObject = JObject.Parse(bodyJsonSchema);
            if (bodyJObject.ContainsKey(AllTimetableName))
            {
                var alltimeTableId = bodyJObject[AllTimetableName].ToObject<int>();
                var allTimeTable = await _timeTables.GetAllTimeTables(alltimeTableId);
                if(allTimeTable!=null)
                    availableBranchIdFromRequest = allTimeTable.BranchId;
            }
            else if (bodyJObject.ContainsKey(BranchIdParamName))
            {
                var bodyDepartmentId =  bodyJObject[BranchIdParamName].ToObject<int>();
                availableBranchIdFromRequest = bodyDepartmentId;
            }

            if (bodyJObject.ContainsKey(DepartmentIdParamName))
            {
                var departmentIdFromBody = bodyJObject[DepartmentIdParamName]?.ToObject<int>();
                if(departmentIdFromBody!=null)
                    availableDepartmentFromRequest = departmentIdFromBody;
            }
            else if (bodyJObject.ContainsKey(DepartmentCodeParameter))
            {
                var departmentStatusCodeFromBody = bodyJObject[DepartmentCodeParameter]
                    .ToObject<string>()
                    .Trim();
                if (departmentStatusCodeFromBody != null && departmentStatusCodeFromBody != "2")
                {
                    var departmentFromBody =
                        await _departmentLogic.GetDepartmentByCode(departmentStatusCodeFromBody);
                    availableDepartmentFromRequest = departmentFromBody.DepartmentId;
                }
            }

            return (availableBranchIdFromRequest, availableDepartmentFromRequest);
        }

        public async  Task<(int? BranchID, int? DepartmentId)> 
            GetDataAboutDepartmentAndBranchFromRequest(AuthorizationFilterContext context)
        {
            int? availableBranchIdFromRequest = null;
            var request = context.HttpContext.Request;
            int? availableDepartmentFromRequest = null;
            if (context.ActionDescriptor.Parameters.Any(parameter =>parameter.Name.Equals(BranchIdParamName) || 
                                                            parameter.Name.Equals(AllTimetableName) && 
                                                            parameter.ParameterType == typeof(int)))
                (availableBranchIdFromRequest,availableDepartmentFromRequest) = 
                    await GetDataAboutDepartmentAndBranchFromQueryString(context);
            else if (request.Body.CanRead)
                (availableBranchIdFromRequest,availableDepartmentFromRequest) = 
                    await GetDataAboutDepartmentAndBranchFromBody(context);
            return (availableBranchIdFromRequest, availableDepartmentFromRequest);
        }
    }
}