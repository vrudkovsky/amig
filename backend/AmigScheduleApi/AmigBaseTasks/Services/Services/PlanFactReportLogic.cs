﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities;
using AmigScheduleApi.Controllers;
using AmigScheduleApi.Filters;
using AmigScheduleApi.Models.PlanFactReport;
using AmigScheduleApi.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace AmigScheduleApi.Services.Services
{
    public class PlanFactReportLogic : IPlanFactReportLogic
    {
        private readonly ActimaDbEntities.Entities.ActimaContext actimaDB;
        public PlanFactReportLogic(ActimaDbEntities.Entities.ActimaContext context)
        {
            actimaDB = context;
        }

        public InfoEmployeesinDepartment getInfoEmployee(int EmployeeID, DateTime date)
        {
            //получение информации по сотруднику при гибком режиме
            var report = actimaDB.AllTimetables.Join(actimaDB.Timetable,
                p => p.TimetableId,
                t => t.TimetableId,
                (p, t) => new { TypeID = p.TypeId.Value, ShiftStart = t.ShiftStart, ShiftEnd = t.ShiftEnd, Date = t.Date, EmployeeID = t.EmployeeId.Value, hours = Math.Abs((t.ShiftStart - t.ShiftEnd).TotalMinutes)/60 })
                .Where(p => p.EmployeeID == EmployeeID && p.Date.Month == date.Month && p.Date.Year == date.Year && p.TypeID == 1).ToList();
            // var report;
            var shiftsflexib = actimaDB.EmployeesDetails.Join(actimaDB.EmployeesDetailsAstu,
                p => p.RecordId,
                t => t.RecordId,
                (p, t) => new { emplId = p.EmployeeId, start = p.StartDate, end = p.EndDate, shift = t.ShiftRegimeId })
                .Join(actimaDB.ShiftRegimes,
                p => p.shift,
                t => t.ShiftRegimeId,
                (p, t) => new { p.emplId, p.end, p.start, t.FlexibleSchedule }).Where(p => p.FlexibleSchedule == 1 && p.emplId == EmployeeID);

            List<FactDates> listrep1 = new List<FactDates>();
            foreach (var item in report)
            {
                listrep1.Add(new FactDates
                {
                    Date=item.Date,
                    EmployeeID=item.EmployeeID,
                    hours=item.hours,
                    ShiftEnd=item.ShiftEnd,
                    ShiftStart=item.ShiftStart,
                    TypeID=item.TypeID
                });
            }
            //проверка на корректность запланированого гибкого режима
            foreach (var item in listrep1.AsParallel())
            {
                if (shiftsflexib.Count() != 0)
                {
                    if ((item.Date < shiftsflexib.FirstOrDefault().start) && (item.Date > shiftsflexib.FirstOrDefault().end))
                        listrep1.Find(p => p == item).EmployeeID=-1;
                }
                if (shiftsflexib.Count() == 0)
                    listrep1.Find(p => p == item).EmployeeID = -1;
            }
            var shiftsunflexib = actimaDB.EmployeesDetails.Join(actimaDB.EmployeesDetailsAstu,
                p => p.RecordId,
                t => t.RecordId,
                (p, t) => new { emplId = p.EmployeeId, start = p.StartDate, end = p.EndDate, shift = t.ShiftRegimeId })
                .Join(actimaDB.ShiftRegimes,
                p => p.shift,
                t => t.ShiftRegimeId,
                (p, t) => new { p.emplId, p.end, p.start, t.FlexibleSchedule }).Where(p => p.FlexibleSchedule == 0 && p.emplId == EmployeeID);

            //полуечение информации по сотруднику при фиксированом режиме
            var report2 = actimaDB.ShiftScheduleWithTemplate.Join(actimaDB.Shifts,
                p => p.ShiftId,
                t => t.ShiftId,
                (p, t) => new { EmployeeID = p.EmployeeId, Date = p.ScheduleDate, ShiftStart = t.ShiftStart, ShiftEnd = t.ShiftEnd, hours = Math.Abs((t.ShiftStart - t.ShiftEnd).TotalMinutes/60), id = t.ShiftRegimeId })
                .Join(actimaDB.ShiftRegimes,
                p => p.id,
                t => t.ShiftRegimeId,
                (p, t) => new { TypeID = t.FlexibleSchedule.Value, p.ShiftStart, p.ShiftEnd, p.Date, p.EmployeeID, p.hours })
                .Where(p => p.EmployeeID == EmployeeID && p.Date.Month == date.Month && p.Date.Year == date.Year).ToList();
            List<FactDates> listrep2 = new List<FactDates>();
            foreach (var item in report2)
            {
                listrep2.Add(new FactDates
                {
                    Date = item.Date,
                    EmployeeID = item.EmployeeID,
                    hours = item.hours,
                    ShiftEnd = item.ShiftEnd,
                    ShiftStart = item.ShiftStart,
                    TypeID = item.TypeID
                });
            }
            //проверка на корректность запланированого гибкого режима
            foreach (var item in listrep2.AsParallel())
            {
                if (shiftsunflexib.Count() != 0)
                {
                    if ((item.Date < shiftsunflexib.FirstOrDefault().start) && (item.Date > shiftsunflexib.FirstOrDefault().end))
                        listrep2.Find(p => p == item).EmployeeID = -1;
                }
                if (shiftsunflexib.Count() == 0)
                    listrep2.Find(p => p == item).EmployeeID = -1;
            }
        

            //получение праздничных дней на месяц
            var holidaytime = actimaDB.Holidays.Where(p => p.HolidayDate.Month == date.Month && p.HolidayDate.Year == date.Year);
            //перечень запланированых дат по сотруднику независимо от того, какой у него график
            // var list = report.Union(report2);
            IEnumerable<FactDates> list = listrep1.Union(listrep2).Where(p => p.EmployeeID != -1);
            //фактически отработано сотрудником
            var factlist = actimaDB.TmpT13countData.Where(p => p.EmployeeId == EmployeeID && p.Date.Month == date.Month && p.Date.Year == date.Year).ToList().OrderByDescending(p => p.Date);
            //пропуски сотрудника в текущем месяце
            var absAll = actimaDB.AbsenceSchedule.Join(actimaDB.AbsenceCauses,
                p => p.AbsenceCauseId,
                t => t.AbsenceCauseId,
                (p, t) => new { p.EmployeeId, p.ScheduleDate, p.AbsenceCauseId, t.Coefficient }).Where(p => p.EmployeeId == EmployeeID && p.ScheduleDate.Month == date.Month && p.ScheduleDate.Year == date.Year).ToList();
            var abs = absAll.Where(p => p.AbsenceCauseId == 6);


            InfoEmployeesinDepartment infoEmpl = new InfoEmployeesinDepartment();
            List<TimesofDay> reportEmpl = new List<TimesofDay>();
            //по запланированому дню смотрим был ли он отработан
            //пока сравниваются два списка дат и учитываются смены. 
            foreach (var item in list.AsParallel())
            {
                if (item.hours != 0)
                {
                    TimesofDay time = new TimesofDay();
                    time.Date = item.Date;
                    //запланированая норма с учетом обедов
                    if (item.hours > 4)
                        infoEmpl.GraphicNorma += time.GraphicNorma = item.hours - 1;
                    else infoEmpl.GraphicNorma += time.GraphicNorma = item.hours;

                    //неявки? Непонятно критерии
                    // if (abs.Where(p => p.ScheduleDate == item.Date).Count() != 0) infoEmpl.TimeAbsence += time.TimeAbsence = item.hours;//другой алгоритм неявок
                    if (absAll.Where(p => p.ScheduleDate == item.Date).Count() != 0)
                    {
                        if (absAll.Where(p => p.ScheduleDate == item.Date).Select(p => p.Coefficient).First() == 1)
                        {
                            infoEmpl.Timeabspay += time.Timeabspay = time.GraphicNorma;
                            infoEmpl.Numberabspay += time.Numberabspay = 1;
                        }
                        else if (absAll.Where(p => p.ScheduleDate == item.Date).Select(p => p.Coefficient).First() == 0)
                        {
                            infoEmpl.Timeabsnopay += time.Timeabsnopay = time.GraphicNorma;
                            infoEmpl.Numberabsnopay += time.Numberabsnopay = 1;
                        }
                    }


                    //Работал ли в этот день
                    if (factlist.Where(p => p.Date == item.Date).Count() != 0)
                    {
                        //получаем информацию по дню. В случае 2х смен, то предоставляется инф по дню, когда смена началась раньше
                        var fact = factlist.Where(p => p.Date == item.Date).First();

                        if (fact != null)
                        {
                            //избавляемся от информации об мервой смене, если их две. Далее вторая смена будет считаться как одна. 
                            if (factlist.Where(p => p.Date == item.Date).Count() > 1)
                                factlist.Where(p => p.Date == item.Date).First().Date = new DateTime(1000, 1, 1);
                            infoEmpl.FactWork += time.FactWork = Math.Abs((fact.StartTime - fact.EndTime).Value.TotalMinutes)/60;
                            //учитываем обед
                            if (time.FactWork > 4.5)
                            {
                                time.FactWork--;
                                infoEmpl.FactWork--;
                            }
                            else if (time.FactWork == 4.5)
                            {
                                time.FactWork -= 0.5;
                                infoEmpl.FactWork -= 0.5;
                            }

                            //недоработки
                            if (time.GraphicNorma > time.FactWork) infoEmpl.Flaws += time.Flaws = time.GraphicNorma - time.FactWork;

                            //переработки
                            if (fact.OverTime.HasValue) infoEmpl.OverTime += time.OverTime = fact.OverTime.Value.Hour + fact.OverTime.Value.Minute/60;
                            //общее время
                            infoEmpl.All += time.All = time.OverTime + time.FactWork;

                            //ночное время
                            //if (fact.NightTime == 1) infoEmpl.NightTime += time.NightTime = time.FactWork;
                            infoEmpl.NightTime += time.NightTime = fact.NightTime.Value; //количество часов ночных смен

                            //праздники
                            if (holidaytime.Where(p => p.HolidayDate == item.Date.Date).Count() != 0) infoEmpl.HolidayTime += time.HolidayTime = item.hours;

                            //опоздния
                            if (item.ShiftStart.TimeOfDay.TotalMinutes < fact.StartTime.Value.TimeOfDay.TotalMinutes)
                            {
                                infoEmpl.Late += time.Late = 1;
                                infoEmpl.TimeLate += time.TimeLate = Math.Abs((fact.StartTime.Value.TimeOfDay - item.ShiftStart.TimeOfDay).TotalMinutes)/60;
                            }
                            //уход раньше
                            if (item.ShiftEnd.TimeOfDay.TotalMinutes > fact.EndTime.Value.TimeOfDay.TotalMinutes)
                            {
                                infoEmpl.Early += time.Early = 1;
                                infoEmpl.TimeEarly += time.TimeEarly = Math.Abs((item.ShiftEnd.TimeOfDay - fact.EndTime.Value.TimeOfDay).TotalMinutes)/60;
                            }

                        }
                    }
                    else
                    {
                        infoEmpl.Flaws += time.Flaws = time.GraphicNorma;
                        if ((time.Timeabsnopay == 0) && (time.Timeabspay == 0))
                        {
                            infoEmpl.TimeAbsence += time.TimeAbsence = time.GraphicNorma;
                        }
                    }
                    time.FactWork = RoundTime(time.FactWork, 3);
                    time.Flaws = RoundTime(time.Flaws, 3);
                    time.GraphicNorma = RoundTime(time.GraphicNorma, 3);
                    time.HolidayTime = RoundTime(time.HolidayTime, 3);
                    time.NightTime = RoundTime(time.NightTime, 3);
                    time.TimeAbsence = RoundTime(time.TimeAbsence, 3);
                    time.Timeabsnopay = RoundTime(time.Timeabsnopay, 3);
                    time.Timeabspay = RoundTime(time.Timeabspay, 3);
                    time.TimeEarly = RoundTime(time.TimeEarly, 3);
                    time.TimeLate = RoundTime(time.TimeLate, 3);
                    reportEmpl.Add(time);
                }

            }

            infoEmpl.FactWork = RoundTime(infoEmpl.FactWork, 3);
            infoEmpl.Flaws = RoundTime(infoEmpl.Flaws, 3);
            infoEmpl.GraphicNorma = RoundTime(infoEmpl.GraphicNorma, 3);
            infoEmpl.HolidayTime = RoundTime(infoEmpl.HolidayTime, 3);
            infoEmpl.NightTime = RoundTime(infoEmpl.NightTime, 3);
            infoEmpl.TimeAbsence = RoundTime(infoEmpl.TimeAbsence, 3);
            infoEmpl.Timeabsnopay = RoundTime(infoEmpl.Timeabsnopay, 3);
            infoEmpl.Timeabspay = RoundTime(infoEmpl.Timeabspay, 3);
            infoEmpl.TimeEarly = RoundTime(infoEmpl.TimeEarly, 3);
            infoEmpl.TimeLate = RoundTime(infoEmpl.TimeLate, 3);
            infoEmpl.TaxNumber = getTaxNumber(EmployeeID);
            infoEmpl.Profession = getProfession(EmployeeID);
            infoEmpl.Name = getName(EmployeeID);
            infoEmpl.infoDays = reportEmpl;

            return infoEmpl;
        }

        //[Authorize]
        //[TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
         static double RoundTime(double time, int t)
        {
            if (Math.Round(time, 1) - Math.Floor(time) == 0.5) return Math.Round(time, 1); 
            else return Math.Round(time, 0);
        }
        public async Task<List<Departments>> GetReport(string codeDepartment, int Group, int IdEmployee, int Month, string Year, int branchId, int user)
        {            
           // var result = new AccountController().User.Claims;
            List<Departments> departments = new List<Departments>();
            // var departments;
            List<InfoEmployeesinDepartment> employee = new List<InfoEmployeesinDepartment>();
            DateTime date = new DateTime(Convert.ToInt32(Year), Month, 1);
            //фильтр по сотруднику
            if (IdEmployee != 0)
            {
                employee.Add(getInfoEmployee(IdEmployee, date));
                string codeDep = actimaDB.Departments.FirstOrDefault(p => p.DepartmentId == getDepartmentID(IdEmployee))?.Code;
                departments.Add(new Departments()
                {
                    id = getDepartmentID(IdEmployee),
                    code = codeDep,
                    name = actimaDB.Departments.Where(p => p.Code == codeDep).Select(p => p.Name).First(),
                    Employees = employee.OrderBy(p => p.Name).ToList()
                });

            }
            // фильтр по подразделению
            else if (codeDepartment != "0") departments = getInfoFromDepartment(Group, new List<string> { codeDepartment }, date);
            //по группе
            else if (Group != 0) departments = getInfoFromGroup(Group, date, branchId,user).ToList();
            //по филиалу
            else
            {
                List<string> departmentIds = getDepart(branchId,user);
                departments = getInfoFromDepartment(Group, departmentIds, date).ToList();
            }

            return departments;
        }
        public int getDepartmentID(int id)
        {
            var info = actimaDB.EmployeesDetails.Join(actimaDB.Departments,
                p => p.DepartmentId,
                t => t.DepartmentId,
                (p, t) => new { p.DepartmentId, p.EmployeeId }).Where(p => p.EmployeeId == id).FirstOrDefault().DepartmentId.Value;
            return info;
        }

        public List<string> getDepart(int branch, int user)
        {
            var IsNormalDepartmentStructure = actimaDB.Departments.Any(departments => departments.Root.DepartmentId == branch);
            if(IsNormalDepartmentStructure)
            { 
            var depRights = actimaDB.Departments.Join(actimaDB.UserDepartmentRights,
                p => p.DepartmentId,
                t => t.DepartmentId,
                (p, t) => new { p.Code, t.Enabled, p.RootId,t.UserId }).Where(p => p.Enabled == 1 && p.RootId == branch&&p.UserId==user);
            return depRights.Select(p => p.Code).Distinct().ToList();
            }
            else
            {
                var depRights = actimaDB.Departments.Join(actimaDB.UserDepartmentRights,
                    p => p.DepartmentId,
                    t => t.DepartmentId,
                    (p, t) => new { p.Code, t.Enabled, p.RootId, t.UserId, p.DepartmentId }).Where(p => p.Enabled == 1 && p.DepartmentId == branch && p.UserId == user);
                return depRights.Select(p => p.Code).Distinct().ToList();
            }
            

        }


        public List<Departments> getInfoFromGroup(int Group, DateTime date, int Branch,int user)
        {
            List<Departments> Departments = new List<Departments>();

            //получения всех подразделений филиала для дальнейшего сравнения пренадлежания сотрудника к филиалу, а не только к группе.
            var departments = getDepart(Branch,user);


            foreach (var codes in departments.AsParallel())
            {
                List<InfoEmployeesinDepartment> employee = new List<InfoEmployeesinDepartment>();
                var id = actimaDB.EmployeesDetails.Join(actimaDB.EmployeeGroups2Employee,
                    p => p.RecordId,
                    t => t.RecordId,
                    (p, t) => new { id = p.EmployeeId, group = t.EmployeeGroupId, departmemt = p.Department.Code })
                    .Where(p => p.group == Group && p.departmemt == codes)
                    .OrderBy(p => p.departmemt)
                    .Distinct()
                    .ToList();
                foreach (var item in id.AsParallel())
                    employee.Add(getInfoEmployee(item.id, date));
                Departments.Add(new Departments()
                {
                    id = actimaDB.Departments.Where(p => p.Code == codes).FirstOrDefault().DepartmentId,
                    code = codes,
                    name = actimaDB.Departments.Where(p => p.Code == codes).Select(p => p.Name).First(),
                    Employees = employee.OrderBy(p => p.Name).ToList()
                });
            }


            return Departments;
        }

        public List<Departments> getInfoFromDepartment(int Group, List<string> codeDepartment, DateTime date)
        {
            List<Departments> departments = new List<Departments>();

            var idDepart = actimaDB.Departments.Where(p => codeDepartment.Contains(p.Code)).ToList();

            foreach (var CodeDep in codeDepartment.AsParallel())
            //Если была выбрана группа
            {
                List<InfoEmployeesinDepartment> employee = new List<InfoEmployeesinDepartment>();
                int idDep = idDepart.Where(t => t.Code == CodeDep).Select(k => k.DepartmentId).FirstOrDefault();
                var allid = actimaDB.EmployeesDetails.Where(p => p.DepartmentId == idDep);
                if (Group != 0)
                {
                    var id = allid.Join(actimaDB.EmployeeGroups2Employee,
                        p => p.RecordId,
                        t => t.RecordId,
                        (p, t) => new { id = p.EmployeeId, group = t.EmployeeGroupId }).Where(p => p.group == Group).Select(p => p.id).Distinct().ToList();


                    foreach (var item in id.AsParallel())
                        employee.Add(getInfoEmployee(item, date));
                }
                else
                {
                    var allids = allid.Select(p => p.EmployeeId)
                        .Distinct()
                        .ToList();
                    foreach (var item in allids.AsParallel())
                        employee.Add(getInfoEmployee(item, date));

                }

                string Name = actimaDB.Departments.Where(p => p.Code == CodeDep).Select(p => p.Name).FirstOrDefault();

                departments.Add(new Departments()
                {
                    id = idDep,
                    code = CodeDep,
                    name = Name,
                    Employees = employee.OrderBy(p => p.Name).ToList()
                });
            }
            return departments;
        }

        public string getTaxNumber(int id)
        {
            var number = actimaDB.Employees.Where(p => p.EmployeeId == id).FirstOrDefault().AccountNumber;
            if (number != null)
                return number.ToString();
            else return null;

        }
        public string getProfession(int id)
        {
            var profession = actimaDB.EmployeesDetails.Join(actimaDB.Professions,
                 p => p.ProfessionId,
                 t => t.ProfessionId,
                 (p, t) => new { idEmployee = p.EmployeeId, profession = t.Name }).Where(p => p.idEmployee == id).Select(p => p.profession).FirstOrDefault();
            if (profession != null)
                return profession;
            else return null;
        }

        public string getName(int id)
        {
            var employee = actimaDB.EmployeesDetails.Where(p => p.EmployeeId == id).FirstOrDefault();
            if (employee != null)
                return employee.LastName.ToString() + " " + employee.FirstName.First().ToString() + "." + employee.MiddleName.First() + ".";
            else return null;
        }

    }
}
