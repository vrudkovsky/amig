using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Claims;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.Exceptions;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData;
using AmigScheduleApi.Services.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace AmigScheduleApi.Services.Services
{
    public class EmployeeControllerCollector : IEmployeeControllerCollector
    {
        private readonly IEmployeeDetailsLogic _employeeLogic;
        private readonly IDepartmentLogic _departmentLogic;
        private readonly INormatives _normatives;
        private readonly ITimeTablesLogic _timeTables;
        private readonly IEmployeeResponseObjectLogic _responseObjectLogic;
        private readonly IAuthentication _authentication;
        private const int DinnerDurationHour = 1;
        private const int NormHourForDay = 8;

        public EmployeeControllerCollector(IEmployeeDetailsLogic employeeLogic, IDepartmentLogic departmentLogic,
            INormatives normatives, ITimeTablesLogic timeTables, IEmployeeResponseObjectLogic responseObjectLogic,
            IAuthentication authentication)
        {
            _timeTables = timeTables;
            _responseObjectLogic = responseObjectLogic;
            _authentication = authentication;
            _employeeLogic = employeeLogic;
            _departmentLogic = departmentLogic;
            _normatives = normatives;
        }

        public async Task<Branch4MonthJsonData> GetOneEmployeeInfo(DateTime dateTime, List<Claim> userClaims,
            int allTimetableId, int empId)
        {
            var monthDate = new DateTime(dateTime.Year, dateTime.Month, 1);
            var allTimetable = await _timeTables.GetAllTimetablesForMonth(allTimetableId);
            if (allTimetable?.TimetablesDate != monthDate) AlltimeTableAndDateValidation(allTimetableId, allTimetable, monthDate);
            var isOvertimeSchedule = allTimetable.TypeId == 3 || allTimetable.TypeId == 4;

            var branch = await _departmentLogic.GetDepartment(allTimetable.BranchId);
            if (branch == null || !_departmentLogic.DepartmentIsBranch(branch)) BranchValidation(branch, allTimetable);

            var branchData = GeneratePostDataForBranch4Month(branch, dateTime,
                    GenerateBaseBranchData<Branch4MonthJsonData>(allTimetable, dateTime, branch));


            var records = await _employeeLogic.GetRecordIdFromEmployeeId(empId);
            foreach (var recordId in records)
            {
                var employeeGroupIdAndDepartId = await _employeeLogic.GetDepartIdAndGroupIdFromEmployee(recordId);
                var departmentId = employeeGroupIdAndDepartId.Item1;
                var groupId = employeeGroupIdAndDepartId.Item2;
                var groupName = employeeGroupIdAndDepartId.Item3;

                var department = await _departmentLogic.GetDepartment(departmentId);


                var holidaysForMonth = await _normatives.GetHolidaysForMonth(monthDate);

                var holidayDays = holidaysForMonth.Select(holidays => holidays.HolidayDate.Day).ToList();

                var groupsCollection = new List<Groups4MonthJsonData>();
                var employeesByDepartment =
                    await _employeeLogic.GetEmployeesByDepartment4Month(departmentId, monthDate);
                var absenceForAllEmployees =
                    await _normatives.GetAll4MonthAbsenceForDepartment(monthDate, department.Code);
                var employeesByGroup = _employeeLogic.GetEmployeeByGroup(groupId, employeesByDepartment);
                if (!employeesByGroup.Exists(emp => emp.RecordId == recordId))
                    continue;

                var workNormatives = await _normatives.GetDepartmentNormativesForMonth(monthDate);
                var empDataForGroup = new List<Employees4MonthJsonData>();
                var monthAbsenceSchedule = await _employeeLogic.GetEmployeeAbsenceSchedule(monthDate.Month);
                foreach (var employee in employeesByGroup.Where(e => e.RecordId == recordId))
                {
                    if (workNormatives.ContainsKey(employee.EmployeesDetailsAstu.ShiftRegimeId))
                        employee.WorkTimeNormatives.Add(new WorkTimeNormatives
                        {
                            NormHours = Convert.ToInt32(workNormatives[employee.EmployeesDetailsAstu.ShiftRegimeId].Item1),
                            NormDays = workNormatives[employee.EmployeesDetailsAstu.ShiftRegimeId].Item2
                        });

                    if (allTimetable.Timetable.Any(timetable =>
                        timetable.EmployeeId == employee.EmployeeId))
                        employee.Employee.Timetable.AddRange(allTimetable.Timetable
                            .Where(timetable => timetable.EmployeeId == employee.EmployeeId &&
                                                timetable.Date >= employee.StartDate &&
                                                timetable.Date <= employee.EndDate));   
                    if (absenceForAllEmployees.Any(schedule =>
                        schedule.EmployeeId == employee.EmployeeId))
                        employee.Employee.AbsenceSchedule.AddRange(absenceForAllEmployees
                            .Where(schedule => schedule.EmployeeId == employee.EmployeeId));

                    var countFlexibleHoursFromYearStart = isOvertimeSchedule
                        ? GetFlexibleHoursCountToDate(allTimetable.Timetable, allTimetable.TypeId.Value,
                            employee.EmployeeId)
                        : default;
                    var countFlexibleHoursToEndDate = isOvertimeSchedule
                        ? GetFlexibleHoursCountToDate(allTimetable.Timetable
                            .Where(emp => emp.TimetableId == allTimetable.TimetableId).ToList(),
                            allTimetable.TypeId.Value, employee.EmployeeId)
                         : GetFlexibleHoursCountToDate(allTimetable.Timetable, allTimetable.TypeId.Value,
                              employee.EmployeeId) + NormHourForDay * absenceForAllEmployees
                              .Count(schedule => schedule.EmployeeId == employee.EmployeeId);
                    var data = await _responseObjectLogic.SetDataFromEmployee(employee, groupId,
                        countFlexibleHoursToEndDate, countFlexibleHoursFromYearStart, monthDate, holidayDays, monthAbsenceSchedule);
                    empDataForGroup.Add(data);
                }
                groupsCollection.Add(new Groups4MonthJsonData()
                {
                    Code = department.Name + groupId + branch.Code,
                    NameGroup = groupName,
                    EmployeesData = empDataForGroup
                });
                branchData.Departments.Add(new Departments4MonthJsonData()
                {
                    Id = department.DepartmentId,
                    Code = department.Code,
                    NameDepartment = department.Name,
                    Groups = groupsCollection
                });
            }


            return branchData;
        }


        public async Task<BranchJsonData> GetEmployeesData(DateTime dateTime, int allTimetableId,
            List<Claim> userClaims, int departmentId = 0, int groupId = -1)
        {
            var allowedDepartments = _authentication.GetDepartmentsListFromClaims(userClaims);
            var date = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
            var dateIsFriday = date.DayOfWeek == DayOfWeek.Friday;
            var nextDateIsHoliday = await _normatives.NextDayIsHoliday(date);
            var monthDate = new DateTime(date.Year, date.Month, 1);
            var allTimetable = await _timeTables.GetAllTimetableMainSchedule(allTimetableId);
            if (allTimetable == null)
                throw new UnprocessableEntityException("AllTimetables with parameters like that isn`t exist" +
                                               $"allTimeTableId : {allTimetableId}");
            if (allTimetable.TimetablesDate != monthDate)
                throw new UnprocessableEntityException($"AllTimetable has different date " +
                                                        $"{allTimetable.TimetablesDate} from {monthDate}");
            var branch = await _departmentLogic.GetDepartment(allTimetable.BranchId);
            if (branch == null)
                throw new BadRequestException($"Department with id like : {allTimetable.BranchId} doesnt exist");
            if (!_departmentLogic.DepartmentIsBranch(branch))
                throw new BadRequestException($"branch with id like {branch.DepartmentId}isn`t exist");
            var branchData = new BranchJsonData
            {
                Id = branch.DepartmentId,
                Date = date,
                IsFriday = date.DayOfWeek == DayOfWeek.Friday,
                NextDayIsHoliday = nextDateIsHoliday,
                NameBranch = branch.Name,
                Code = branch.Code,
                AllTimetables = new AllTimetableData
                {
                    Id = allTimetable.TimetableId,
                    Name = allTimetable.TimetablesName.Trim(),
                    TypeId = allTimetable.TypeId,
                    EditDate = allTimetable.EditDate
                },
                Departments = new List<DepartmentsJsonData>()
            };
            var subDepartments = await _departmentLogic.GetSubDepartments(branch.DepartmentId, departmentId);
            if (!subDepartments.Any())
                throw new BadRequestException($"Sub departments for branch with Id like {branch.DepartmentId}" +
                                               $" isn`t exist");
            if (departmentId == default)
            {
                subDepartments = subDepartments
                    .Where(departments => allowedDepartments.Contains(departments.DepartmentId))
                    .ToList();
            }

            var isOvertimeSchedule = allTimetable.TypeId == 3 || allTimetable.TypeId == 4;

            var groupDictionary = await _employeeLogic.GetDictionaryForGroupNamesWoTracking(groupId);
            List<Timetable> empTimeTablesForAllEmpToDate;
            if (!isOvertimeSchedule)
            {
                empTimeTablesForAllEmpToDate =
                    await _timeTables.GetTimetablesForMonth(allTimetable.TimetableId, date);
            }
            else
            {
                empTimeTablesForAllEmpToDate =
                    await _timeTables.GetTimetablesForOvertimeOverYear(allTimetable.TimetableId, date);
            }

            var empTimeTablesForAllEmpForDate = empTimeTablesForAllEmpToDate.Where(emp => emp.Date == date)
                .ToList();

            foreach (var department in subDepartments)
            {
                var groupsData = new List<GroupsJsonData>();
                var employeesByDepartment =
                    await _employeeLogic.GetEmployeesByDepartment(department.DepartmentId, date);
                var absenceForAllEmployees = await _normatives.GetAllAbsenceForDepartment(date, department.Code);
                var workNormativesForEmployee =
                    await _normatives.GetWorkTimeNormativesForDepartment(monthDate, department.Code);
                foreach (var group in groupDictionary)
                {
                    var employeesByGroup = _employeeLogic.GetEmployeeByGroup(group.Key, employeesByDepartment);
                    var empDataForGroup = new List<EmployeeData>();
                    if (employeesByGroup.Count == 0)
                        continue;
                    foreach (var employee in employeesByGroup)
                    {

                        if (workNormativesForEmployee.Any(normatives =>
                            normatives.RecordId == employee.RecordId))
                            employee.WorkTimeNormatives.Add(workNormativesForEmployee
                                .FirstOrDefault(normatives => normatives.RecordId == employee.RecordId));
                        if (empTimeTablesForAllEmpForDate.Any(timetable =>
                            timetable.EmployeeId == employee.EmployeeId))
                            employee.Employee.Timetable.AddRange(empTimeTablesForAllEmpForDate
                                .Where(timetable => timetable.EmployeeId == employee.EmployeeId));
                        if (absenceForAllEmployees.Any(schedule =>
                            schedule.EmployeeId == employee.EmployeeId && schedule.ScheduleDate == date))
                            employee.Employee.AbsenceSchedule.Add(absenceForAllEmployees
                                .FirstOrDefault(schedule => schedule.EmployeeId == employee.EmployeeId));

                        var countFlexibleHoursFromYearStart = isOvertimeSchedule
                            ? GetFlexibleHoursCountToDate(empTimeTablesForAllEmpToDate, allTimetable.TypeId.Value,
                                employee.EmployeeId)
                            : default;
                        var countFlexibleHoursToEndDate = isOvertimeSchedule
                            ? GetFlexibleHoursCountToDate(empTimeTablesForAllEmpToDate
                                .Where(emp => emp.TimetableId == allTimetable.TimetableId).ToList(),
                                allTimetable.TypeId.Value, employee.EmployeeId)
                             : GetFlexibleHoursCountToDate(empTimeTablesForAllEmpToDate, allTimetable.TypeId.Value,
                                  employee.EmployeeId) + NormHourForDay * absenceForAllEmployees
                                  .Count(schedule => schedule.EmployeeId == employee.EmployeeId);

                        var countFlexibleHoursToCurrentDate = isOvertimeSchedule
                            ? GetFlexibleHoursCountToDate(empTimeTablesForAllEmpToDate
                                    .Where(emp => emp.TimetableId == allTimetable.TimetableId).ToList(),
                                allTimetable.TypeId.Value, employee.EmployeeId, date)
                            : GetFlexibleHoursCountToDate(empTimeTablesForAllEmpToDate, allTimetable.TypeId.Value,
                                  employee.EmployeeId, date) + NormHourForDay * absenceForAllEmployees
                                  .Count(schedule => schedule.EmployeeId == employee.EmployeeId);
                        empDataForGroup.Add(_responseObjectLogic.SetDataFromEmployee(employee, group.Key,
                            countFlexibleHoursToCurrentDate, countFlexibleHoursFromYearStart, dateIsFriday,
                            nextDateIsHoliday, countFlexibleHoursToEndDate));
                    }
                    groupsData.Add(new GroupsJsonData
                    {
                        Code = department.Name + group.Key + branch.Code,
                        NameGroup = group.Value,
                        EmployeesData = empDataForGroup.OrderBy(data => data.TabelNumber).ToList()
                    });
                }

                branchData.Departments.Add(new DepartmentsJsonData
                {
                    Id = department.DepartmentId,
                    Code = department.Code,
                    NameDepartment = department.Name,
                    Groups = groupsData
                });
            }

            if (branchData.Departments.All(departments =>
                departments.Groups.All(groups => groups.EmployeesData.Count == 0)))
                throw new UnprocessableEntityException("No employees in that branch");
            return branchData;
        }
        private double GetIntervalDuration(Timetable timetable)
        {
            return timetable.ShiftEnd.Hour -
                   timetable.ShiftStart.Hour +
                   (timetable.ShiftEnd.Hour == 23 &&
                    timetable.ShiftEnd.Minute == 59
                       ? 1
                       : Math.Round((double)(timetable.ShiftEnd.Minute -
                                              timetable.ShiftStart.Minute) / 60, 2));
        }

        private double GetFlexibleHoursCountToDate(List<Timetable> empTimeTablesForAllEmpToDate, int allTimetableTypeId,
            int employeeId, DateTime? day = null)
        {
            var countFlexibleHoursToCurrentDate = default(double);
            const int minTimeForIntervalWithDinner = 4;
            var empTimeTablesForEmpToDate = empTimeTablesForAllEmpToDate
                .Where(timetable => timetable.EmployeeId == employeeId)
                .OrderBy(timetable => timetable.Date)
                .ToList();
            if (day != null)
            {
                empTimeTablesForEmpToDate = empTimeTablesForEmpToDate.Where(timetable => timetable.Date <= day.Value)
                    .ToList();
            }
            if (allTimetableTypeId == 3 || allTimetableTypeId == 4)
            {
                countFlexibleHoursToCurrentDate = empTimeTablesForEmpToDate
                    .Sum(GetIntervalDuration);
            }
            else
            {
                for (var i = 0; i < empTimeTablesForEmpToDate.Count; i++)
                {
                    var intervalDuration = GetIntervalDuration(empTimeTablesForEmpToDate[i]);

                    if (empTimeTablesForEmpToDate[i].ShiftEnd.Hour == 23 &&
                        empTimeTablesForEmpToDate[i].ShiftEnd.Minute == 59 &&
                        i + 1 != empTimeTablesForEmpToDate.Count &&
                        empTimeTablesForEmpToDate[i + 1].ShiftStart.Hour == 0
                        && empTimeTablesForEmpToDate[i + 1].ShiftStart.Minute == 0)
                    {
                        var crossNightIntervalDuration = intervalDuration +
                            GetIntervalDuration(empTimeTablesForEmpToDate[i + 1]);
                        if (crossNightIntervalDuration >= minTimeForIntervalWithDinner)
                            crossNightIntervalDuration--;
                        countFlexibleHoursToCurrentDate += crossNightIntervalDuration;
                        i++;
                    }
                    else
                    {
                        countFlexibleHoursToCurrentDate += intervalDuration >= minTimeForIntervalWithDinner
                            ? intervalDuration - 1
                            : intervalDuration;
                    }
                }
            }

            return countFlexibleHoursToCurrentDate;
        }

        public async Task<BranchJsonData> GetEmployeesDataBySpecificEmployee(DateTime dateTime,
            int employeeId, int allTimeTableId, List<Claim> userClaims)
        {

            var date = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
            var dateIsFriday = date.DayOfWeek == DayOfWeek.Friday;
            var nextDateIsHoliday = await _normatives.NextDayIsHoliday(date);
            var monthDate = new DateTime(date.Year, date.Month, 1);
            var allTimetable = await _timeTables.GetAllTimetableMainSchedule(allTimeTableId);
            if (allTimetable == null)
                throw new BadRequestException("AllTimetables with parameters like that isn`t exist" +
                                                $" allTimeTablesId : {allTimeTableId}");
            var employee = await _employeeLogic.GetEmployeeDetails(date, employeeId);

            if (employee?.DepartmentId == null)
                throw new BadRequestException($"There isn't data about employee with id like : {employeeId}");

            var department = await _departmentLogic.GetDepartment(employee.Department.DepartmentId);
            var branch = department.Root;
            var allowedDepartments = _authentication.GetDepartmentsListFromClaims(userClaims);
            if (branch.DepartmentId != allTimetable.BranchId || !allowedDepartments.Contains(department.DepartmentId))
                throw new BadRequestException($"Employee doesn't related to Branch with " +
                    $"branchId like {branch.DepartmentId}, or to department with id like {department.DepartmentId}");
            //because in one day employee can only belong to one empGroup or dont belong to anyone
            var group = employee.EmployeeGroups2Employee.FirstOrDefault();

            var branchData = new BranchJsonData
            {
                Id = branch.DepartmentId,
                Date = date,
                IsFriday = date.DayOfWeek == DayOfWeek.Friday,
                NextDayIsHoliday = nextDateIsHoliday,
                NameBranch = branch.Name,
                Code = branch.Code,
                AllTimetables =
                    new AllTimetableData
                    {
                        Id = allTimetable.TimetableId,
                        Name = allTimetable.TimetablesName.Trim(),
                        TypeId = allTimetable.TypeId,
                        EditDate = allTimetable.EditDate
                    },
                Departments = new List<DepartmentsJsonData>()
            };
            var departmentsJsonData = new DepartmentsJsonData
            {
                Id = department.DepartmentId,
                Code = department.Code,
                NameDepartment = department.Name,
                Groups = new List<GroupsJsonData>()
            };
            var groupsJsonData = new GroupsJsonData
            {
                //if there is no department we will send default value
                Code = department.Name + (group?.EmployeeGroupId ?? 0) + branch.Code,
                NameGroup = group?.EmployeeGroup.Name,
                EmployeesData = new List<EmployeeData>()
            };
            var isOvertimeSchedule = allTimetable.TypeId == 3 || allTimetable.TypeId == 4;
            List<Timetable> timeTablesToDate;
            if (!isOvertimeSchedule)
            {
                timeTablesToDate = await _timeTables.GetTimetablesForEmployeeToDate(
                    allTimetable.TimetableId, date, employeeId, department.Code);
            }
            else
            {
                timeTablesToDate = await _timeTables.GetTimetablesForOvertimeForEmployeeToDate(
                    allTimetable.TimetableId, date, employeeId, department.Code);
            }

            var absence = await _normatives.GetAbsenceForEmployeeFromMonthStart(date, department.Code, employeeId);
            var timeTables = timeTablesToDate
                .Where(timetable => timetable.Date == date)
                .ToList();
            if (timeTables.Any())
                foreach (var timeTable in timeTables)
                    employee.Employee.Timetable.Add(timeTable);
            if (absence.Any())
                employee.Employee.AbsenceSchedule.Add(absence.FirstOrDefault(abse => abse.ScheduleDate == date));
            var workTimeNormatives = await _normatives.GetWorkTimeNormativesForEmployee(monthDate, employee.RecordId);
            employee.WorkTimeNormatives.Add(workTimeNormatives);


            var countFlexibleHoursFromYearStart = isOvertimeSchedule
                ? GetFlexibleHoursCountToDate(timeTablesToDate, allTimetable.TypeId.Value,
                    employee.EmployeeId)
                : default;
            var countFlexibleHoursToEndDate = isOvertimeSchedule
                ? GetFlexibleHoursCountToDate(timeTablesToDate.Where(emp => emp.TimetableId == allTimetable.TimetableId)
                    .ToList(), allTimetable.TypeId.Value, employee.EmployeeId)
                : GetFlexibleHoursCountToDate(timeTablesToDate, allTimetable.TypeId.Value, employee.EmployeeId)
                  + NormHourForDay * absence.Count;

            var countFlexibleHoursToCurrentDate = isOvertimeSchedule
                ? GetFlexibleHoursCountToDate(timeTablesToDate.Where(emp => emp.TimetableId == allTimetable.TimetableId)
                    .ToList(), allTimetable.TypeId.Value, employee.EmployeeId, date)
                : GetFlexibleHoursCountToDate(timeTablesToDate, allTimetable.TypeId.Value, employee.EmployeeId, date)
                  + NormHourForDay * absence.Count;

            groupsJsonData.EmployeesData.Add(_responseObjectLogic
                .SetDataFromEmployee(employee, group?.EmployeeGroupId ?? 0, countFlexibleHoursToCurrentDate,
                    countFlexibleHoursFromYearStart, dateIsFriday, nextDateIsHoliday, countFlexibleHoursToEndDate));
            branchData.Departments.Add(departmentsJsonData);
            departmentsJsonData.Groups.Add(groupsJsonData);

            return branchData;
        }

        public async Task<Branch4MonthJsonData> GetEmployeesData4Month(DateTime dateTime, List<Claim> userClaims,
                    int allTimetableId, int departmentId = default, int groupId = -1)
        {
            var monthDate = new DateTime(dateTime.Year, dateTime.Month, 1);
            var allowedDepartments = _authentication.GetDepartmentsListFromClaims(userClaims);
            var groupDictionary = await _employeeLogic.GetDictionaryForGroupNamesWoTracking(groupId);

            //var date = new DateTime(dateTime.Year, dateTime.Month, 1);
            //var dateIsFriday = date.DayOfWeek == DayOfWeek.Friday;
            //var nextDateIsHoliday = await _normatives.NextDayIsHoliday(date);
            var allTimetable = await _timeTables.GetAllTimetablesForMonth(allTimetableId);
            if (allTimetable?.TimetablesDate != monthDate) AlltimeTableAndDateValidation(allTimetableId, allTimetable, monthDate);
            var isOvertimeSchedule = allTimetable.TypeId == 3 || allTimetable.TypeId == 4;

            var branch = await _departmentLogic.GetDepartment(allTimetable.BranchId);
            if (branch == null || !_departmentLogic.DepartmentIsBranch(branch)) BranchValidation(branch, allTimetable);

            var branchData = GeneratePostDataForBranch4Month(branch, dateTime,
                    GenerateBaseBranchData<Branch4MonthJsonData>(allTimetable, dateTime, branch));

            var subDepartments = await _departmentLogic.GetSubDepartments(branch.DepartmentId, departmentId);
            if (departmentId == default)
                subDepartments = subDepartments
                    .Where(departments => allowedDepartments.Contains(departments.DepartmentId))
                    .ToList();

            List<Timetable> empTimeTablesForAllEmpForMonth;
            if (isOvertimeSchedule)
                empTimeTablesForAllEmpForMonth =
                    await _timeTables.GetTimetablesForOvertimeOverYear(allTimetable.TimetableId, monthDate);
            var holidaysForMonth = await _normatives.GetHolidaysForMonth(monthDate);
            var holidayDays = holidaysForMonth.Select(holidays => holidays.HolidayDate.Day).ToList();

            var monthAbsenceSchedule = await _employeeLogic.GetEmployeeAbsenceSchedule(monthDate.Month);



            foreach (var department in subDepartments)
            {
                var groupsCollection = new List<Groups4MonthJsonData>();
                var employeesByDepartment =
                    await _employeeLogic.GetEmployeesByDepartment4Month(department.DepartmentId, monthDate);
                var absenceForAllEmployees =
                    await _normatives.GetAll4MonthAbsenceForDepartment(monthDate, department.Code);
                var workNormatives = await _normatives.GetDepartmentNormativesForMonth(monthDate);


                foreach (var group in groupDictionary)
                {
                    var employeesByGroup = _employeeLogic.GetEmployeeByGroup(group.Key, employeesByDepartment);
                    if (employeesByGroup.Count == 0)
                        continue;
                    var empDataForGroup = new List<Employees4MonthJsonData>();
                    foreach (var employee in employeesByGroup)
                    {
                        if (workNormatives.Count != 0 && workNormatives.ContainsKey(employee.EmployeesDetailsAstu.ShiftRegimeId))
                        employee.WorkTimeNormatives.Add(new WorkTimeNormatives {
                            NormHours = Convert.ToInt32(workNormatives[employee.EmployeesDetailsAstu.ShiftRegimeId].Item1),
                            NormDays = workNormatives[employee.EmployeesDetailsAstu.ShiftRegimeId].Item2
                        });
                        if (allTimetable.Timetable.Any(timetable =>
                            timetable.EmployeeId == employee.EmployeeId))
                            employee.Employee.Timetable.AddRange(allTimetable.Timetable
                                .Where(timetable => timetable.EmployeeId == employee.EmployeeId &&
                                                    timetable.Date >= employee.StartDate &&
                                                    timetable.Date <= employee.EndDate));
                        if (absenceForAllEmployees.Any(schedule =>
                            schedule.EmployeeId == employee.EmployeeId))
                            employee.Employee.AbsenceSchedule.AddRange(absenceForAllEmployees
                                .Where(schedule => schedule.EmployeeId == employee.EmployeeId));

                        var countFlexibleHoursFromYearStart = isOvertimeSchedule
                            ? GetFlexibleHoursCountToDate(allTimetable.Timetable, allTimetable.TypeId.Value,
                                employee.EmployeeId)
                            : default;
                        var countFlexibleHoursToEndDate = isOvertimeSchedule
                            ? GetFlexibleHoursCountToDate(allTimetable.Timetable
                                .Where(emp => emp.TimetableId == allTimetable.TimetableId).ToList(),
                                allTimetable.TypeId.Value, employee.EmployeeId)
                             : GetFlexibleHoursCountToDate(allTimetable.Timetable, allTimetable.TypeId.Value,
                                  employee.EmployeeId) + NormHourForDay * absenceForAllEmployees
                                  .Count(schedule => schedule.EmployeeId == employee.EmployeeId);
                        empDataForGroup.Add(await _responseObjectLogic.SetDataFromEmployee(employee, group.Key,
                            countFlexibleHoursToEndDate, countFlexibleHoursFromYearStart, monthDate, holidayDays, monthAbsenceSchedule));
                    }
                    groupsCollection.Add(new Groups4MonthJsonData()
                    {
                        Code = department.Name + group.Key + branch.Code,
                        NameGroup = group.Value,
                        EmployeesData = empDataForGroup
                    });
                }
                branchData.Departments.Add(new Departments4MonthJsonData()
                {
                    Id = department.DepartmentId,
                    Code = department.Code,
                    NameDepartment = department.Name,
                    Groups = groupsCollection
                });
            }

            if(!AnyEmployeesInBranch(branchData))
                throw new NoContentException("No employees in that branch"); 
            return branchData;

        }


        private void AlltimeTableAndDateValidation(int alltimetableId, AllTimetables table, DateTime date)
        {
            if (table == null)
                throw new UnprocessableEntityException("AllTimetables with parameters like that isn`t exist" +
                                                       $"allTimeTableId : {alltimetableId}");
            if (table.TimetablesDate != date)
                throw new UnprocessableEntityException($"AllTimetable has different date " +
                                                        $"{table.TimetablesDate} from {date}");
        }

        private void BranchValidation(Departments branch, AllTimetables allTimetable)
        {
            if (branch == null)
                throw new BadRequestException($"Department with id like : {allTimetable.BranchId} doesnt exist");
            if (!_departmentLogic.DepartmentIsBranch(branch))
                throw new BadRequestException($"branch with id like {branch.DepartmentId}isn`t exist");
        }

        private TBranchType GenerateBaseBranchData<TBranchType>(AllTimetables allTimetable, DateTime date, Departments branch)
            where TBranchType : BranchResponseObject, new()
        {
            return new TBranchType()
            {
                Code = branch.Code,
                NameBranch = branch.Name,
                AllTimetables = new AllTimetableData
                {
                    Id = allTimetable.TimetableId,
                    Name = allTimetable.TimetablesName.Trim(),      
                    TypeId = allTimetable.TypeId,
                    EditDate = allTimetable.EditDate
                },
            };
        }
        private Branch4MonthJsonData GeneratePostDataForBranch4Month(Departments branch, DateTime date, Branch4MonthJsonData data)
        {
            data.Id = branch.DepartmentId;
            data.Year = date.Year;
            data.Month = date.Month;
            data.Departments = new List<Departments4MonthJsonData>();
            return data;
        }

        private bool AnyEmployeesInBranch(Branch4MonthJsonData data)
        {
            return !data.Departments.All(departments => departments.Groups.
                All(groups => groups.EmployeesData.Count == 0));
        }
    }
}