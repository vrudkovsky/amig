using System;
using System.Threading.Tasks;
using AmigScheduleApi.Models.CodesResponseJsonObject;
using OfficeOpenXml;

namespace AmigScheduleApi.Services.IServices
{
    public interface ISecondExcelReport
    {
        string Header(DateTime monthDate);

        void GenerateExcelPerMouth(ExcelPackage package, EmployeesByDepartment employeesByDepartment,
            DateTime monthDate, string filterName);

        Task<string> GetFiltersDescription(string departmentCode, int groupId);
    }
}