using System;
using System.Collections.Generic;
using AmigScheduleApi.Models.PlanFactReport;
using OfficeOpenXml;

namespace AmigScheduleApi.Services.IServices
{
    public interface IThirdExcelReport
    {
        void GenerateExcel(List<Departments> data,DateTime monthDate ,ExcelPackage package,string userName);
        string BaseGraphicName(DateTime mouthDate);

        string GenerateExcelForSingleEmpWitUnwrap(List<Departments> data, DateTime mouthDate, ExcelPackage package,
            string userName, string employeeTaxNumber);

        string UnwrapExcelName(DateTime mouthDate, string employeeName);
    }
}