﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmigScheduleApi.Models.PlanFactReport;

namespace AmigScheduleApi.Services.IServices
{
    public interface IPlanFactReportLogic
    {
        InfoEmployeesinDepartment getInfoEmployee(int EmployeeID, DateTime date);
        Task<List<Departments>> GetReport(string codeDepartment, int Group, int IdEmployee, int Month, string Year, int branchId, int user);
        int getDepartmentID(int id);
        List<string> getDepart(int branch, int user);
        List<Departments> getInfoFromGroup(int Group, DateTime date, int Branch, int user);
        List<Departments> getInfoFromDepartment(int Group, List<string> codeDepartment, DateTime date);
        string getTaxNumber(int id);
        string getProfession(int id);
        string getName(int id);
    }
}
