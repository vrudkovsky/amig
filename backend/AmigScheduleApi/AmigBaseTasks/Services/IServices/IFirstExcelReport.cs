using System;
using System.IO;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using OfficeOpenXml;

namespace AmigScheduleApi.Services.IServices
{
    public interface IFirstExcelReport
    {
        void CreateExcel(BranchJsonData branchData, ExcelPackage package);
        string GetAlltimetableName(string name, int timetableType, DateTime date);
    }
}