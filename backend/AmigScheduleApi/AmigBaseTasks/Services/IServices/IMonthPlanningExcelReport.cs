﻿using AmigScheduleApi.Models.CodesResponseJsonObject;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmigScheduleApi.Services.IServices
{
    public interface IMonthPlanningExcelReport
    {

        Task GenerateMonthPlanExcel(ExcelPackage package, Branch4MonthJsonData branchData,
        DateTime monthDate, int allTimetableId, int groupId, int departmentId);
    }
}
