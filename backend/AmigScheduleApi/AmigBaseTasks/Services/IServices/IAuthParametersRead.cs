using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AmigScheduleApi.Services.IServices
{
    public interface IAuthParametersRead
    {
        string BranchIdParamName { get; }
        string AllTimetableName  { get; }
        string DepartmentIdParamName { get; }
        string DepartmentCodeParameter { get; }
        Task<(int? BranchID, int? DepartmentId)> GetDataAboutDepartmentAndBranchFromRequest(
            AuthorizationFilterContext context);
    }
}