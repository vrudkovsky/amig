using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Models.HelpClasses;

namespace AmigScheduleApi.Services.IServices
{
    public interface IEmployeeControllerCollector
    {
        Task<BranchJsonData> GetEmployeesData(DateTime dateTime, int allTimetableId, List<Claim> userClaims,
            int departmentId = 0, int groupId = -1);

         Task<BranchJsonData> GetEmployeesDataBySpecificEmployee(DateTime dateTime,
             int employeeId, int allTimeTableId, List<Claim> userClaims);

         Task<Branch4MonthJsonData> GetEmployeesData4Month(DateTime dateTime, List<Claim> userClaims,
             int allTimetableId, int departmentId = default, int groupId = -1);

        Task<Branch4MonthJsonData> GetOneEmployeeInfo(DateTime dateTime, List<Claim> userClaims,
                    int allTimetableId, int empId);
    }
}