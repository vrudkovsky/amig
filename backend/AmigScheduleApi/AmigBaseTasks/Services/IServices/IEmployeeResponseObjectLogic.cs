using ActimaDbEntities.Entities;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmigScheduleApi.Services.IServices
{
    public interface IEmployeeResponseObjectLogic
    {
        EmployeeData SetDataFromEmployee(EmployeesDetails employees, int groupKey,
            double countFlexibleHoursToCurrentDate, double countFlexibleHoursFromYearStart,
            bool shortDay, bool shortPreHoliday, double countFlexibleHoursToEndDate);

        Task<Employees4MonthJsonData> SetDataFromEmployee(EmployeesDetails employees, int groupKey,
            double countHours, double countOvertimeHours, DateTime monthDate,
            List<int> holidaysDaysNumbers, List<Tuple<int, int>> employeeAbsenceList);
        
    }
}