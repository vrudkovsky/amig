using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using ActimaDbEntities.IServices;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;

namespace AmigScheduleApi.Services
{
    public class LogCompressorBackgroundService:BackgroundService
    {        
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Log.Information("Timed Background Service is starting.");

            var timer = new Timer(CompressLogFilesForPastWeekend, null, TimeSpan.Zero, 
                TimeSpan.FromDays(7));

            return Task.CompletedTask;
            
        }

        private string GetDayMonthSerilogRepresentation(int number)=>
            number >= 10
                ? number.ToString()
                : $"0{number}";
        
        private  void CompressLogFilesForPastWeekend(object state)
        {


            var allFiles = new DirectoryInfo("./Logs");
            var dateString = $"{DateTime.Today.Year}{GetDayMonthSerilogRepresentation(DateTime.Today.Month)}" +
                             $"{GetDayMonthSerilogRepresentation(DateTime.Today.Day)}";
            var regexForFilesFromPreviousDays = new Regex($"^{dateString}\\w*");
            var logFiles = allFiles.EnumerateFiles("*.log")
                .Where(info =>!regexForFilesFromPreviousDays.IsMatch(info.Name))
                .ToList();
            
            if (!logFiles.Any()) return;
            
            DirectoryInfo directory = null;
            var directoryPath = $"./Logs/{dateString}"; 
            directory = !Directory.Exists(directoryPath) 
                ? Directory.CreateDirectory(directoryPath) 
                : Directory.GetParent(directoryPath);

            foreach (var log in logFiles)
            {
                try
                {
                    log.MoveTo($"{directoryPath}/{log.Name}");
                }
                catch (Exception e)
                {
                    Log.Error(e,"File from ./Logs/{0}.{1} was opened by logger" +
                                "so we not enabled to move that file in  zip archive we will do it automatically " +
                                "next time, after 7days",log.Name,log.Extension);
                }                    
            }
                
            var zipDirectory = $"./Logs/{dateString}ForWeekend.zip";
            Log.Information($"Starting compressing log files into {zipDirectory}");
            ZipFile.CreateFromDirectory(directoryPath, zipDirectory);
            directory.Delete(true);
            Log.Information("Log files for at least seven days was compressed into \"" +
                            "./Logs/{0}ForWeekend.zip ",dateString);
        }        
    }
}