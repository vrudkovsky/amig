using System;
using System.Collections.Generic;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace AmigScheduleApi.MQ.Listener
{
    public interface IMqListener
    {
        List<QueueDeclareOk> QueuesInformation { get; set; }

        void BindRandomGenerateQueueToExchangeWithLogDebugMessagesOutput(string exchangeName,
            params string[] routingKeys);

        void BindQueueToExchangeWithCustomEventHandler(string queueName,
            string exchangeName,
            EventHandler<BasicDeliverEventArgs> onReceivedAction,
            params string[] routingKeys);

        void BindQueueToExchangeWithReceiveObjectFromJson<T>(string queueName,
            string exchangeName,            
            params string[] routingKeys);

        void Deregister();
        string CreateQueueWithRandomName();
    }
}