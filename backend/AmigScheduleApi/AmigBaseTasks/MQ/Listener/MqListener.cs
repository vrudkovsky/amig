using System;
using System.Collections.Generic;
using System.Text;
using AmigScheduleApi.Models.HelpClasses;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Serilog;

namespace AmigScheduleApi.MQ.Listener
{
    public sealed class MqListener:RabbitMqConnection,IMqListener
    {
        public List<QueueDeclareOk> QueuesInformation { get; set; } = new List<QueueDeclareOk>();  
        
        public void BindRandomGenerateQueueToExchangeWithLogDebugMessagesOutput(string exchangeName,
            params string[] routingKeys)
        {
            var queue = _channel.QueueDeclare();
            QueuesInformation.Add(queue);
            var queueName = queue.QueueName;

            void ReceivedEventHandler(object sender, BasicDeliverEventArgs args)
            {
                var body = args.Body;
                var massage = Encoding.UTF8.GetString(body);
                Log.Debug($"Queue {queue.QueueName} received a message {massage} from exchange "
                 + $"{exchangeName}\n{massage}\n at {DateTime.Now}");
            }

            BindQueueToExchangeWithCustomEventHandler(queueName:queueName,
                                                      onReceivedAction: ReceivedEventHandler,
                                                       exchangeName: exchangeName,
                                                       routingKeys: routingKeys);
        }

        public void BindQueueToExchangeWithCustomEventHandler(string queueName,
            string exchangeName, 
            EventHandler<BasicDeliverEventArgs>onReceivedAction,
            params string[] routingKeys)
        {
            _channel.ExchangeDeclare(exchange:exchangeName,type: "topic");            
            var routingKeyForPattern = routingKeys.Join(".");
                     
            _channel.QueueBind(queue:queueName, 
                exchange: exchangeName, 
                routingKey: routingKeyForPattern);
            
            var consumer = new EventingBasicConsumer(_channel);            
            consumer.Received += onReceivedAction;
            _channel.BasicConsume(queue: queueName, consumer: consumer, autoAck: true);
            
            Log.Information($"Queue {queueName} was bound to Exchange: {exchangeName} with" +
                                   $"routing keys {routingKeyForPattern} ");            
        }

        public void BindQueueToExchangeWithReceiveObjectFromJson<T>(string queueName,
            string exchangeName,
            params string[] routingKeys)
        
        {       
            
            void ReceiveObject(object sender, BasicDeliverEventArgs args)
            {
                if (!args.BasicProperties.ContentType.Contains("application/json"))
                {                    
                    Log.Debug($"Queue {queueName} received a message with incorrect Content-Type" +
                              $" property {args.BasicProperties.ContentType} from exchange {exchangeName} at " +
                              $"{DateTime.Now}");
                    return;
                }
                    var body = args.Body;
                    var jsonSchema = Encoding.UTF8.GetString(body);
                    var receiveObject = JsonConvert.DeserializeObject<T>(jsonSchema);
                    if (receiveObject.IsObjectClear())
                    {
                        Log.Debug($"Queue {queueName} received an incorrect message {jsonSchema}\n " +
                                  $"from exchange {exchangeName}\n{jsonSchema}\n at {DateTime.Now}");                        
                    }
                    else
                    {
                        Log.Debug($"Queue {queueName} received a message {jsonSchema}\n" +
                                               $"from exchange {exchangeName}\n{jsonSchema}\n at {DateTime.Now}");
                        //TODO:implement functions to save object in collection
                    }
                
            }
            
            BindQueueToExchangeWithCustomEventHandler(queueName:queueName,
                onReceivedAction: ReceiveObject,
                exchangeName: exchangeName,
                routingKeys: routingKeys);            
        }

        public string CreateQueueWithRandomName()
        {
            var queueDeclare = _channel.QueueDeclare();
            QueuesInformation.Add(queueDeclare);
            return queueDeclare.QueueName;
        }        
        
        public override void Deregister()
        {
            base.Deregister();
            Log.Information("Mq listener was de registered");
        }
    }
}
