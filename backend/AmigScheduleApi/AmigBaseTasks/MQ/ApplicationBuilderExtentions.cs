using AmigScheduleApi.Models.HelpClasses;
using AmigScheduleApi.MQ.Listener;
using AmigScheduleApi.MQ.Sender;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AmigScheduleApi.MQ
{
    public static class ApplicationBuilderExtentions
    {
        private static  IMqListener _listener;
        private static IMqSender _sender;
        public static IApplicationBuilder UseRabbitListener(this IApplicationBuilder app)
        {
            _listener = app.ApplicationServices.GetService<IMqListener>();
            _sender = app.ApplicationServices.GetService<IMqSender>();
            var lifetime = app.ApplicationServices.GetService<IApplicationLifetime>();

            lifetime.ApplicationStarted.Register(OnStarted);

            //press Ctrl+C to reproduce if your app runs in Kestrel as a console app
            lifetime.ApplicationStopping.Register(OnStopping);

            return app;
        }

        private static void OnStarted()
        {
            _listener.BindRandomGenerateQueueToExchangeWithLogDebugMessagesOutput("test_exchange",
                                                                                 "#");
            _listener.BindRandomGenerateQueueToExchangeWithLogDebugMessagesOutput("test_exchange",
                                                                                 "#", "info","#");
            var queueName = _listener.CreateQueueWithRandomName();
            _listener.BindQueueToExchangeWithReceiveObjectFromJson<TestClass>( queueName,
                                                                    "test_exchange",
                                                                "123");
            _sender.SendSerializedObjectInJsonToExchange("test_exchange", 
                new TestClass(){ Age= 23, Name= "Marry"}, "user", "info");
        }
        private static void OnStopping()
        {
            _sender.Deregister();
            _listener.Deregister();
        }
    }

}