using RabbitMQ.Client;

namespace AmigScheduleApi.MQ
{
    public abstract class RabbitMqConnection
    {
        protected readonly ConnectionFactory _factory;
        protected readonly IConnection _connection;
        protected readonly IModel _channel;               

        protected RabbitMqConnection()
        {
            _factory = new ConnectionFactory {HostName = "localhost", Port = 5672};
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
        }
        public virtual void Deregister()
        {                
            _connection.Close();
        }
    }
}