using RabbitMQ.Client;

namespace AmigScheduleApi.MQ.Sender
{
    public interface IMqSender
    {
        void SendSerializedObjectInJsonToExchange<T>(string exchangeName, T objectToSent, params string[] routingKeys);

        void SendObjectToExchange<T>(string exchangeName, T objectToSent, IBasicProperties properties,
            params string[] routingKeys);

        void Deregister();
    }
}