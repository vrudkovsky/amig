using System.Text;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using RabbitMQ.Client;
using Serilog;

namespace AmigScheduleApi.MQ.Sender
{
    public class MqSender: RabbitMqConnection, IMqSender
    {
        public void SendObjectToExchange<T>(string exchangeName, T objectToSent, IBasicProperties properties, 
            params string [] routingKeys)
        {
            _channel.ExchangeDeclare(exchange:exchangeName, 
                                     type:"topic");
            byte[] body;
            string messageToSent;
            var routingKey = routingKeys.Join(".");
            if (objectToSent is string message)
            {
                messageToSent = message;
                
            }
            else
            {
                messageToSent = JsonConvert.SerializeObject(objectToSent);                
            }
            body = Encoding.UTF8.GetBytes(messageToSent);
            _channel.BasicPublish(exchange:exchangeName, 
                                  basicProperties: properties, 
                                  body:body, 
                                  routingKey:routingKey);            
            Log.Debug($"Message {messageToSent} was sent to exchange {exchangeName} " +
                      $"with routing key {routingKey}");
        }

        public void SendSerializedObjectInJsonToExchange<T>(string exchangeName, T objectToSent,
            params string[] routingKeys)
        {
            var properties = _channel.CreateBasicProperties();
            properties.ContentType = "application/json;";
            SendObjectToExchange(exchangeName,objectToSent,properties,routingKeys);
        }        
        public override void Deregister()
        {
            base.Deregister();
            Log.Information("Mq sender was de registered");
        }
    }
}