using System.Collections.Generic;
using OfficeOpenXml;

namespace AmigScheduleApi.Models.CodesResponseJsonObject
{
    public class BranchCodes
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool FullRights { get; set; }
        public List<DepartmentCodes> DepartmentCodes { get; set; }
    }
}