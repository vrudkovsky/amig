namespace AmigScheduleApi.Models.CodesResponseJsonObject
{
    public class DepartmentCodes
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}