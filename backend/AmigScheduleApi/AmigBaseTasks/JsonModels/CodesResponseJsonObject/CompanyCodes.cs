using System.Collections.Generic;

namespace AmigScheduleApi.Models.CodesResponseJsonObject
{
    public class CompanyCodes
    {
        public List<BranchCodes> BranchCodes { get; set; }
        public List<GroupsInfo> GroupsIds { get; set; }
    }
}