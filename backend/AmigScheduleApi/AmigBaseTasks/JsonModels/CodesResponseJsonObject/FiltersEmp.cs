﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;

namespace AmigScheduleApi.Models.CodesResponseJsonObject
{
    public class FiltersEmp
    {
            public string Year { get; set; }
            public string Month { get; set; }
            public IQueryable DepartmentIds { get; set; }
            public List<EmployeeGroups> GroupsIds { get; set; }

            public IQueryable TimeTables { get; set; }

            public IQueryable Employee { get; set; }

    }

}

