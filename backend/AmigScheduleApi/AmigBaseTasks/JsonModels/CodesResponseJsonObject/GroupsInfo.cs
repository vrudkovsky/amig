namespace AmigScheduleApi.Models.CodesResponseJsonObject
{
    public class GroupsInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}