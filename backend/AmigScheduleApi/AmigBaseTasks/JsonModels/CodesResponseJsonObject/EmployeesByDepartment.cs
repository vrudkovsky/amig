﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmigScheduleApi.Models.GraphicPerMonth;

namespace AmigScheduleApi.Models.CodesResponseJsonObject
{
    public class EmployeesByDepartment
    {
        public List<EmpDep> Departments { get; set; }
    }
}
