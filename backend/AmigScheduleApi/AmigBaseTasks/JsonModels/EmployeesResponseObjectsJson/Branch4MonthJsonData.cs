using System.Collections.Generic;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData;

namespace AmigScheduleApi.Models.EmployeesResponseObjectsJson
{
    public class Branch4MonthJsonData:BranchResponseObject
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public List<Departments4MonthJsonData> Departments { get; set; }
    }

    public class Departments4MonthJsonData : DepartmentsResponseObject
    {
        public List<Groups4MonthJsonData> Groups { get; set; }
    }

    public class Groups4MonthJsonData : GroupsResponseObject
    {
        public List<Employees4MonthJsonData > EmployeesData { get; set; }
    }

    public class Employees4MonthJsonData : EmployeesResponseObject
    {
        public bool IsShortFridayDayAvailable { get; set; }
        public bool IsPreHolidayDayAvailable { get; set; }  
        public string Comment { get; set; }
        public double OvertimeForCurrentYear { get; set; }
        public List<Days4MonthJsonData> Days { get; set; }
    }

    public class Days4MonthJsonData: DaysResponseObject
    {
        public int DayNumber { get; set; }
    }
}