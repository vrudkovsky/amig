using System;
using System.Collections.Generic;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData;

namespace AmigScheduleApi.Models.EmployeesResponseObjectsJson
{
    public sealed class BranchJsonData:BranchResponseObject
    {
        public DateTime Date { get; set; }
        public bool IsFriday { get; set; }
        public bool NextDayIsHoliday { get; set; }
        public int Id { get; set; }
        public List<DepartmentsJsonData> Departments { get; set; }
    }
    public sealed class DepartmentsJsonData:DepartmentsResponseObject
    {
        public List<GroupsJsonData> Groups { get; set; }
    }
    public sealed class GroupsJsonData:GroupsResponseObject
    {
        public int GroupId { get; set; }
        public  List<EmployeeData> EmployeesData { get; set; }
    }
    public sealed class EmployeeData:EmployeesResponseObject,IDaysResponseObject
    {
        public double CountHoursToMonthEnd { get; set; }
        public double CountHoursFromStartYear { get; set; }
        public int NormOFHoursForDay { get; set; }
        public bool ShiftEnabled { get; set; }
        public UnFlexibleNormatives UnFlexibleNormatives { get; set; }
        public string AbsenceCause { get; set; }
        public List<Interval> Intervals { get; set; }
    }
}