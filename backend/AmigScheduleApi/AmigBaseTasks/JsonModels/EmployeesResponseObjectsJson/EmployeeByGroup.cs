﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;

namespace AmigScheduleApi.Models.EmployeesResponseObjectsJson
{
    public class EmployeeByGroup
    {
        public int employeeid { get; set; }
        public int tabelNumber {get;set;}
        public string Name { get; set; }
        public int totalWorksDay { get; set; }
        public int normOfHours { get; set; }
        public IQueryable<EmployeeByGroup> Employee { get; set; }
    }
}
