using System.Collections.Generic;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData;

namespace AmigScheduleApi.Models.EmployeesResponseObjectsJson
{
    public abstract class BranchResponseObject
    {
        public string NameBranch { get; set; }
        public string Code { get; set; }
        public AllTimetableData AllTimetables { get; set; }
    }

    public abstract class DepartmentsResponseObject
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string NameDepartment { get; set; }
    }

    public abstract class GroupsResponseObject
    {
        public string Code { get; set; } // 091115 = 09 department 1 - group 115 - branch  ┐(͡`ಠ ʖ̯ ͡ಠ)┌
        public string NameGroup { get; set; } 
    }

    public abstract class EmployeesResponseObject
    {
        public int EmployeeId { get; set; }
        public decimal TabelNumber { get; set; }
        public string Name { get; set; }
        public double CountHours { get; set; }
        public string Position { get; set; }
        public int NormOfHours { get; set; }
        public int NormOfDays { get; set; }
        public int GroupId { get; set; }
    }

    public interface IDaysResponseObject
    {
        int NormOFHoursForDay { get; set; }
        bool ShiftEnabled { get; set; }
        UnFlexibleNormatives UnFlexibleNormatives { get; set; }
        string AbsenceCause { get; set; }
        List<Interval> Intervals { get; set; }
    }

    public abstract class DaysResponseObject : IDaysResponseObject
    {
        public int NormOFHoursForDay { get; set; }
        public bool ShiftEnabled { get; set; }
        public UnFlexibleNormatives UnFlexibleNormatives { get; set; }
        public string AbsenceCause { get; set; }
        public List<Interval> Intervals { get; set; }
    }
}