namespace AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData
{
    public sealed class UnFlexibleNormatives
    {
        public bool IsOutlet { get; set; }
    }
}