using System;

namespace AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData
{
    public sealed class Interval
    {
        public DateTime? ShiftStart { get; set; }
        public DateTime? ShiftEnd { get; set; }
    }
}