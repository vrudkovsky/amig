using System;

namespace AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData
{
    public class AllTimetableData
    {
        public int Id { get; set; }
        public int? TypeId { get; set; }
        public string Name { get; set; }        
        public DateTime? EditDate { get; set; }
    }
}