namespace AmigScheduleApi.Models.GraphicPerMonth
{
    public class InformationForDay
    {
        public int Number { get; set; }
        public string TypeDay { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public double Plan { get; set; }
    }
}