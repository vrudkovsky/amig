using System.Collections.Generic;

namespace AmigScheduleApi.Models.GraphicPerMonth
{
    public class EmployeeAppearanceData
    {
        public int EmployeeId { get; set; }
        public decimal TabelNumber { get; set; }
        public string Name { get; set; }
        public int TotalWorksDay { get; set; }
        public int NormOfHours { get; set; }
        public List<InformationForDay> Days { get; set; }
    }
}