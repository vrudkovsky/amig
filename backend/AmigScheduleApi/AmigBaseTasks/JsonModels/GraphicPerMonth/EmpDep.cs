﻿using System.Collections.Generic;

namespace AmigScheduleApi.Models.GraphicPerMonth
{
    public class EmpDep
    {
        public string DepartmentId { get; set; }
        public List<EmployeeAppearanceData> Employees { get; set; } 
    }
}