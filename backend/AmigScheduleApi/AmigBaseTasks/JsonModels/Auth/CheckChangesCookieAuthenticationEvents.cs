using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ActimaDbEntities.IServices;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Newtonsoft.Json;
using Serilog;

namespace AmigScheduleApi.Models.Auth
{
    public class CheckChangesCookieAuthenticationEvents:CookieAuthenticationEvents
    {
        private readonly IAuthentication _authenticationLogic;

        public CheckChangesCookieAuthenticationEvents(IAuthentication authenticationLogic)
        {
            _authenticationLogic = authenticationLogic;
        }
        public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
        {
            var idAsString = context.Principal.Claims.FirstOrDefault(claim => claim.Type == "Id")?.Value;
            var departmentIdsAsString = context.Principal.Claims
                .FirstOrDefault(claim => claim.Type == "DepartmentId")
                ?.Value;
            if (string.IsNullOrWhiteSpace(idAsString) || string.IsNullOrWhiteSpace(departmentIdsAsString) ||
                !int.TryParse(idAsString, out var id))
            {
                var cookieTroubleMessage = "There isn`t any available data for user, try re-login";
                Log.Warning(cookieTroubleMessage);
                await SignOutAndDeletePrincipals(context,cookieTroubleMessage);
            }
            else
            {
                var departmentIds = JsonConvert.DeserializeObject<int[]>(departmentIdsAsString);
                var currentUser = await _authenticationLogic.GetUserById(id);
                var claimPassword = context.Principal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Hash)
                    ?.Value;
                if (currentUser == null || !currentUser.UserPassword.Equals(claimPassword))
                {
                    var userIdFromCookie = context.Principal.Claims.FirstOrDefault(claim => claim.Type == "Id")?.Value;
                    var userNullOrPasswordChangeMessage =
                        "There isn`t more any available users in " +
                        $"system with Id Like:{userIdFromCookie}, or password was changed";
                    Log.Warning(userNullOrPasswordChangeMessage);
                    await SignOutAndDeletePrincipals(context,userNullOrPasswordChangeMessage);
                    return;
                }
                var userDepartmentsIdsRights = currentUser.UserDepartmentRights.Select(rights => rights.DepartmentId);
                if (userDepartmentsIdsRights.SequenceEqual(departmentIds)) return;
                var departmentsRightsNotEqualsMessage =
                    $"User {currentUser.UserLogin} was rejected, user will be SignOut " +
                    "from system due DB changes in tables UserDepartmentRights or Users";
                Log.Warning(departmentsRightsNotEqualsMessage);
                await SignOutAndDeletePrincipals(context, departmentsRightsNotEqualsMessage);
            }
        }

        public override Task RedirectToLogin(RedirectContext<CookieAuthenticationOptions> context)
        {
            context.Response.StatusCode = 401;
            return Task.CompletedTask;
        }

        public override Task RedirectToAccessDenied(RedirectContext<CookieAuthenticationOptions> context)
        {
            context.Response.StatusCode = 403;
            return Task.CompletedTask;
        }

        private async Task SignOutAndDeletePrincipals(CookieValidatePrincipalContext context, string message)
        {
            context.RejectPrincipal();
            await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            context.HttpContext.Response.Headers.Add("WWW-Authenticate", message);
            context.Response.StatusCode = 401;
        }
    }
}