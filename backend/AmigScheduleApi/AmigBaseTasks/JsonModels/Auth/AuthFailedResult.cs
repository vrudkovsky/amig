using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace AmigScheduleApi.Models.Auth
{
    public static class AuthFailedResult
    {
        public static void SetForbiddenStatusCodeResultAndAuthHeaderData(AuthorizationFilterContext context, 
            string message)
        {
            SetStatusCodeResultAndMetaData(context,message,new StatusCodeResult(403));
        }
        public  static void SetNoContentStatusCodeResultAndMetaData(AuthorizationFilterContext context, string message)
        {
            SetStatusCodeResultAndMetaData(context,message,new StatusCodeResult(204));
        }
        private static void SetStatusCodeResultAndMetaData(AuthorizationFilterContext context, string message, 
            StatusCodeResult codeResult)
        {
            context.HttpContext.Response.Headers.Add("XXX-Metadata",message);
            context.Result = codeResult;
            Log.Warning(message);
        }
    }
}