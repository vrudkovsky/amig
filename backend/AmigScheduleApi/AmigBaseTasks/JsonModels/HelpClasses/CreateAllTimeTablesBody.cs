using System;

namespace AmigScheduleApi.Models.HelpClasses
{
    public class CreateAllTimetablesBody
    {
        public int BranchId { get; set; }
        public DateTime MonthDate { get; set; }
        public string Name { get; set; } 
        public int TableTypeId { get; set; }
        public DateTime? EditDate { get; set; } = null;
    }
}