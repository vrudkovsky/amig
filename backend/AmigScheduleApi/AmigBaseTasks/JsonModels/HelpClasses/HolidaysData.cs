using System;

namespace AmigScheduleApi.Models.HelpClasses
{
    public class HolidaysData
    {
        public string HolidayName { get; set; }
        public DateTime HolidayDate { get; set; }
    }
}