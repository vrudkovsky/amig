namespace AmigScheduleApi.Models.HelpClasses
{
    public class UserLoginFormData
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Persistent { get; set; }
    }
}