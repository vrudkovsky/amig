using System;

namespace AmigScheduleApi.Models.HelpClasses
{
    public static class DateTimeExtensions
    {
        public static DateTime GetDateWithMonthDate(this DateTime dateTime)
        {
            return dateTime.AddMonths(1).AddDays(-1);
        }
    }
}