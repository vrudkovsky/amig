namespace AmigScheduleApi.Models.HelpClasses
{
    public class AlltimetablesCopyRequestBody
    {
        public int AllTimetableId  { get; set; }
        public string Name { get; set; }
    }
}