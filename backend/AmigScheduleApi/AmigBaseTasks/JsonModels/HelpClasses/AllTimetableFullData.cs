using System;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData;

namespace AmigScheduleApi.Models.HelpClasses
{
    public class AllTimetableFullData : AllTimetableData
    {
        public DateTime? TimetableDate { get; set; }
        public int? BranchId { get; set; }
    }
}