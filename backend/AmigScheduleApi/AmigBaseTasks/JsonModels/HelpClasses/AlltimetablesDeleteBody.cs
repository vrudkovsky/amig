namespace AmigScheduleApi.Models.HelpClasses
{
    public class AlltimetablesDeleteBody
    {
        public int AllTimetableId { get; set; }
    }
}