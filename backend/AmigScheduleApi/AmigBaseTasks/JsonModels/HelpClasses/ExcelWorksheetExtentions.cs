using System;
using System.ComponentModel;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing;
using OfficeOpenXml.Style;

namespace AmigScheduleApi.Models.HelpClasses
{
    public static  class ExcelWorksheetExtentions
    {
        public static ExcelWorksheet MergeCellsAndSetValue(this ExcelWorksheet worksheet, string firstR1C1, string secondR1C1,
            string value, 
            ExcelVerticalAlignment verticalAlignment = ExcelVerticalAlignment.Center,
            ExcelHorizontalAlignment horizontalAlignment = ExcelHorizontalAlignment.Center, bool isBoldFont = false)
        {
            worksheet.Cells[string.Join(':', firstR1C1, secondR1C1)].Merge = true;

            var cell = worksheet.Cells[firstR1C1];            
            cell.Value = value;
            cell.Style.VerticalAlignment = verticalAlignment;
            cell.Style.HorizontalAlignment = horizontalAlignment;
            cell.Style.WrapText = true;
            cell.Style.Font.Bold = isBoldFont;

            return worksheet;
        }

        public static ExcelRange SetBorders(this ExcelRange range,ExcelBorderStyle borderStyle )
        {
            range.Style.Border.Top.Style = borderStyle;
            range.Style.Border.Left.Style = borderStyle;
            range.Style.Border.Right.Style = borderStyle;
            range.Style.Border.Bottom.Style = borderStyle;
            return range;
        }
        public static ExcelRange NormalizeCell(this ExcelRange range)
        {

            range.Style.WrapText = true;
            range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            return range;
        }
        public static ExcelRange MergeColoredCellsAndSetValue(this ExcelRange range, object value, 
            Color? color = null, //¯\_(ツ)_/¯
            ExcelFillStyle style = ExcelFillStyle.Solid, 
            ExcelVerticalAlignment verticalAlignment = ExcelVerticalAlignment.Center,
            ExcelHorizontalAlignment horizontalAlignment = ExcelHorizontalAlignment.Center, bool isBoldFont = false,
            int textRotation =0)
        {
            var realColor = color ?? Color.White; //¯\_(ツ)_/¯
            range.Merge = true;
            range.Style.Fill.PatternType = style;
            range.Style.Fill.BackgroundColor.SetColor(realColor);
            range.Style.VerticalAlignment = verticalAlignment;
            range.Style.HorizontalAlignment = horizontalAlignment;
            range.Style.WrapText = true;
            range.Style.Font.Bold = isBoldFont;
            range.Style.TextRotation = textRotation;
            range.Value = value;
            return range;
        }

        public static ExcelRange SetExcelDefaultBorders(this ExcelRange range)
        {
            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(217, 217, 217));
            return range;
        }

        public static ExcelRange SetBoldStyle(this ExcelRange range)
        {
            range.Style.Font.Bold = true;
            return range;
        }
        public static ExcelRange SetWrapText(this ExcelRange range)
        {
            range.Style.WrapText = true;
            return range;
        }
    }
}