using System.Collections.Generic;
using AmigScheduleApi.Models.CodesResponseJsonObject;

namespace AmigScheduleApi.Models.HelpClasses
{
    public class ShortBranchData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Login { get; set; }
        public List<BranchCodes> BranchCodes { get; set; }
    }
}