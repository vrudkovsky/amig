using System.Linq;

namespace AmigScheduleApi.Models.HelpClasses
{
    public  static class TObjectExtentions
    {
        public static bool IsObjectClear<T>(this T obj)
        {
            return obj.GetType().GetProperties()
                .Where(pi => pi.GetValue(obj) is string)
                .Select(pi => (string) pi.GetValue(obj))
                .All(string.IsNullOrEmpty);
        }
    }
}