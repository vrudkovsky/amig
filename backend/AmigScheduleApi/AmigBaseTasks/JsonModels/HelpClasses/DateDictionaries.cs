using System.Collections.Generic;

namespace AmigScheduleApi.Models.HelpClasses
{
    public static class DateDictionaries
    {
        public static Dictionary<int,string> Months => new Dictionary<int, string>
        {
            {1,"Січень"},
            {2,"Лютий"},
            {3,"Березень"},
            {4,"Квітень"},
            {5,"Травень"},
            {6,"Червень"},
            {7,"Липень"},
            {8,"Серпень"},
            {9,"Вересень"},
            {10,"Жовтень"},
            {11,"Листопад"},
            {12,"Грудень"}
        };
        public static Dictionary<int,string> Days => new Dictionary<int, string>
        {
            {1,"Понеділок"},
            {2,"Вівторок"},
            {3,"Середа"},
            {4,"Четверг"},
            {5,"П'ятниця"},
            {6,"Субота"},
            {7,"Неділя"}
        };
    }
}