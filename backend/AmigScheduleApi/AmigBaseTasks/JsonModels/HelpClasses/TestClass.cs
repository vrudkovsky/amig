namespace AmigScheduleApi.Models.HelpClasses
{
    public sealed class TestClass
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }
}