﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmigScheduleApi.Models.PlanFactReport;
namespace AmigScheduleApi.Models.PlanFactReport
{
    public class Departments
    {
        public string code { get; set; }
        public string name { get; set; }
        public int id { get; set; }
        public List<InfoEmployeesinDepartment> Employees { get; set; }
    }
}
