﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmigScheduleApi.Models.PlanFactReport
{
    public class FactDates
    {
        public int TypeID { get; set; }
        public DateTime ShiftStart { get; set; }
        public DateTime ShiftEnd { get; set; }
        public DateTime Date { get; set; }
        public int EmployeeID { get; set; }
        public double hours { get; set; }
    }
}
