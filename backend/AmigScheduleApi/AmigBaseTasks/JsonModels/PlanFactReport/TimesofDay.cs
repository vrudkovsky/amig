﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmigScheduleApi.Models.PlanFactReport
{
    public class TimesofDay
    {
            public DateTime Date { get; set; }
            public double GraphicNorma { get; set; }
            public double FactWork { get; set; }
            public double Flaws { get; set; }
            public double OverTime { get; set; }
            public double All { get; set; }
            public double NightTime { get; set; }
            public double HolidayTime { get; set; }
            public double Timeabspay { get; set; }
            public int Numberabspay { get; set; }
            public double Timeabsnopay { get; set; }

            public int Numberabsnopay { get; set; }
            public int Late { get; set; }
            public double TimeLate { get; set; }
            public int Early { get; set; }
            public double TimeEarly { get; set; }
            public double TimeAbsence { get; set; }
        

    }
}

