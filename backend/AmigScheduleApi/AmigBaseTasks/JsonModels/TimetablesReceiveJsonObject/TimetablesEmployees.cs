using System.Collections.Generic;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson.EmployeeSubData;

namespace AmigScheduleApi.Models.TimetablesReceiveJsonObject
{
    public class TimetablesEmployees
    {
        public int EmployeeId { get; set; }
        public List<Interval> Shifts { get; set; }
    }
}