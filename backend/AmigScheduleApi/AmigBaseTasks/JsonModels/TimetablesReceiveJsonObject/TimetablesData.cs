using System;
using System.Collections.Generic;

namespace AmigScheduleApi.Models.TimetablesReceiveJsonObject
{
    public class TimetablesData
    {
        //TODO Add parameter with departmentIdName
        public int AllTimetableId{ get; set; }
        public DateTime? EditDate { get; set; }
        public DateTime  Date { get; set; }
        public IEnumerable<TimetablesEmployees> Employees { get; set; }
    }
}