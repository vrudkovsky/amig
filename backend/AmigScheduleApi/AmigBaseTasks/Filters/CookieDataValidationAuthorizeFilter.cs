using System;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Models.Auth;
using AmigScheduleApi.Services.IServices;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace AmigScheduleApi.Filters
{
    public class CookieDataValidationAuthorizeFilter:Attribute,IAsyncAuthorizationFilter
    {
        private readonly IAuthParametersRead _authParametersRead;
        private readonly IDepartmentLogic _departmentLogic;
        private readonly IAuthentication _authentication;
        
        public CookieDataValidationAuthorizeFilter(IAuthParametersRead authParametersRead, 
            IDepartmentLogic departmentLogic, IAuthentication authentication)
        {
            _authParametersRead = authParametersRead;
            _departmentLogic = departmentLogic;
            _authentication = authentication;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var claims = context.HttpContext.User.Claims;
            var departmentsIds = _authentication.GetDepartmentsListFromClaims(claims.ToList())
                .ToList();
            
            var (availableBranchIdFromRequest, availableDepartmentFromRequest) =
                await _authParametersRead.GetDataAboutDepartmentAndBranchFromRequest(context); 
            
            if (availableBranchIdFromRequest == default || departmentsIds.All(id => id != availableBranchIdFromRequest))
            {
                AuthFailedResult.SetForbiddenStatusCodeResultAndAuthHeaderData(context,
            "Request was Forbidden due request metadata doesn't contains any information about department");
                return;
            }
                
            var branch = await _departmentLogic.GetDepartment(availableBranchIdFromRequest.Value);
            if (!_departmentLogic.DepartmentIsBranch(branch))
            {
                AuthFailedResult.SetNoContentStatusCodeResultAndMetaData(context,
                    $"Department with branchId {availableBranchIdFromRequest} " + "from request isn't branch");
                return;
            }

            var subDepartmentsIds = branch.InverseRoot
                .Select(departments =>departments.DepartmentId)
                .ToList();
            if (availableDepartmentFromRequest.HasValue && availableDepartmentFromRequest.Value!=0 )
            {

                if (!departmentsIds.Contains(availableDepartmentFromRequest.Value) || subDepartmentsIds.Count != 0 &&
                    !subDepartmentsIds.Contains(availableDepartmentFromRequest.Value))
                {
                    AuthFailedResult.SetForbiddenStatusCodeResultAndAuthHeaderData(context, 
                        "User haven't rights for access to the departments");
                    return;
                }
            }
            else 
            {
                if (subDepartmentsIds.Count != 0 && !subDepartmentsIds.Intersect(departmentsIds).Any())
                {
                    AuthFailedResult.SetForbiddenStatusCodeResultAndAuthHeaderData(context, 
                        "User haven't rights for access to the departments in this branch");
                    return;
                }
            }
            Log.Information("Auth was successfully");
            //If we go to here, everything is ok
        }
    }
}