using System;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Models.Auth;
using AmigScheduleApi.Services.IServices;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace AmigScheduleApi.Filters
{
    /// <summary>
    /// Validate user full rights for branch, read only from body
    /// </summary>
    public class UserFullBranchRightsValidationAuthorizeFilter : Attribute, IAsyncAuthorizationFilter
    {
        private readonly ITimeTablesLogic _timeTables;
        private readonly IDepartmentLogic _departmentLogic;
        private readonly IAuthParametersRead _authParametersRead;
        private readonly IAuthentication _authentication;
        
        public UserFullBranchRightsValidationAuthorizeFilter(ITimeTablesLogic timeTables,
            IDepartmentLogic departmentLogic, IAuthParametersRead authParametersRead, IAuthentication authentication)
        {
            _timeTables = timeTables;
            _departmentLogic = departmentLogic;
            _authParametersRead = authParametersRead;
            _authentication = authentication;
        }

        private const string BranchIdParamName = "branchId";
        private const string AllTimetableName = "allTimetableId";

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var claims = context.HttpContext.User.Claims;
            var departmentsIds = _authentication.GetDepartmentsListFromClaims(claims.ToList())
                .ToList();
            
            var (availableBranchIdFromRequest, _ ) =
                await _authParametersRead.GetDataAboutDepartmentAndBranchFromRequest(context);
            if (!availableBranchIdFromRequest.HasValue)
            {
                AuthFailedResult.SetForbiddenStatusCodeResultAndAuthHeaderData(context,
            "Request was Forbidden due request metadata doesn't contains any information about department");
                return;
            }
                
            var branch = await _departmentLogic.GetDepartment(availableBranchIdFromRequest.Value);
            var subDepartmentsIds = branch.InverseRoot.Select(departments => departments.DepartmentId).ToList();
            var forAllDepartmentsFromBranchUserHaveRights = subDepartmentsIds.Intersect(departmentsIds).Count() ==
                                                            subDepartmentsIds.Count;
            var userHaveRightsForBranch = departmentsIds.Contains(branch.DepartmentId);
            if (!userHaveRightsForBranch || !forAllDepartmentsFromBranchUserHaveRights)
            {
                AuthFailedResult.SetForbiddenStatusCodeResultAndAuthHeaderData(context,
                    $"User haven't rights for branch with id like {branch.DepartmentId}, or haven't rights " +
                    "for all departments in branch");
                return;
            }
            Log.Information("Auth was successfully");
        }
    }
}