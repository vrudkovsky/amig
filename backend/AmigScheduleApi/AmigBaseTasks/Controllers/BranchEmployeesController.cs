﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ActimaDbEntities;
using ActimaDbEntities.Entities;
using AmigScheduleApi.Models.HelpClasses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AmigScheduleApi.Controllers
{
    [Route("api/[controller]")]
    public class BranchEmployeesController : Controller
    {
        ActimaContext actimaDB;
        public BranchEmployeesController(ActimaContext db)
        {
            actimaDB = db;
        }

        //получение списка сотрудников во всех филиалах независимо от компании
        //https://localhost:5001/api/branchemployees?IDbranch=2
        public List<int> GetEmployees([RequiredFromQuery]int IDbranch)
        {
            if (isbranch(IDbranch)) return getlistemployees(IDbranch);
            else return new List<int>(IDbranch);
        }
        //проверка на существование филиала
        private bool isbranch(int IDbranch)
        {
            if (GetBranches().FindIndex(x => x == IDbranch) != -1) return true;
            else return false;            
        }
        //получение списка сотрудников филиала
        private List<int> getlistemployees(int IDbranch)
        {
            var allepmployees = actimaDB.EmployeesDetails.Join(actimaDB.Departments,
                   p => p.DepartmentId,
                   t => t.DepartmentId,
                   (p, t) => new { employeeID = p.EmployeeId, departmentID = t.RootId, groupID=t.DepartmentId });            
            //сотрудники филиала
            var IDbranchemployees = allepmployees
                .Where(p=>p.departmentID == IDbranch||p.groupID==IDbranch)
                .Select(p=>p.employeeID)
                .Distinct()
                .ToList();
            
            return IDbranchemployees;
        }               
        //получение списка филиалов в компании
        private List<int> GetBranches()
        {
            //все подразделения: группы, филиалы, компании
            var alldepartments = actimaDB.Departments.Join(actimaDB.Departments,
               p => p.RootId,
               t => t.DepartmentId,
               (p, t) => new { IDdepartment = p.DepartmentId, IDroot = t.RootId });
            //все филиалы компании
            var IDbranches = alldepartments
                .Where(p => p.IDroot == null)
                .Select(p=>p.IDdepartment).ToList();
            return IDbranches;
        }
    }
}
