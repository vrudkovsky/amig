﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using ActimaDbEntities.Entities;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Filters;
using AmigScheduleApi.Models.HelpClasses;
using Microsoft.EntityFrameworkCore;
using AmigScheduleApi.Models.CodesResponseJsonObject;
using AmigScheduleApi.Models.GraphicPerMonth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace AmigScheduleApi.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowSpecificOrigins")]
    [ApiController]

    public class EmployeeAppearanceController : Controller
    {
        // GET: /<controller>/
        private readonly ActimaContext actima;
        private readonly IAuthentication _authentication;
        /// <param name="_actima"></param>
        public EmployeeAppearanceController(ActimaContext _actima, IAuthentication authentication)
        {
            actima = _actima;
            _authentication = authentication;
        }

        [HttpGet]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        //https://localhost:5001/api/employeeappearance?branchId=2&month=4&year=2019
        public async Task<IActionResult> Get([RequiredFromQuery] int branchId, [RequiredFromQuery] int month,
            [RequiredFromQuery] int year)
        {
            try
            {
                var filters = new FiltersEmp()
                {
                    Month = DateTime.Now.Month.ToString(),
                    Year = DateTime.Now.Year.ToString(),
                    GroupsIds = await getGroups(),
                    DepartmentIds = getDepart(branchId),
                    TimeTables = getTimeTableNames(month, year, branchId),
                    Employee = getAllEmployees(month, year, branchId)

                };
                return Ok(filters);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("bydep")]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        //https://localhost:5001/api/EmployeeAppearance/bydep?branchId=2&month=4&year=2019&departmentId=11500
        public async Task<IActionResult> Get([RequiredFromQuery] int branchId, [RequiredFromQuery] int month,
            [RequiredFromQuery] int year, [RequiredFromQuery] string departmentCode)
        {
            try
            {
                return Ok(new
                {
                    Employee = getAllEmployeesByDepdepartmentId(month, year, branchId, departmentCode)
                });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("bygroup")]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        //https://localhost:5001/api/EmployeeAppearance/bygroup?branchId=2&month=4&year=2019&groupid=5
        public async Task<IActionResult> GetEmpByGroup([RequiredFromQuery] int branchId, [RequiredFromQuery] int month,
            [RequiredFromQuery] int year, [RequiredFromQuery] int groupid)
        {
            try
            {
                return Ok(new
                {
                    Employee = getAllEmployeesByGroupId(month, year, branchId, groupid)
                });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("bygroupanddep")]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        //https://localhost:5001/api/EmployeeAppearance/bygroupanddep?branchId=2&month=4&year=2019&departmentId=11500&groupid=5
        public async Task<IActionResult> GetEmpByGroupAndDep([RequiredFromQuery] int branchId,
            [RequiredFromQuery] int month, [RequiredFromQuery] int year, [RequiredFromQuery] string departmentCode,
            [RequiredFromQuery] int groupid)
        {
            try
            {
                if (departmentCode != "0" && groupid != 0)
                    return Ok(new
                    {
                        Employee = getAllEmployeesByGroupAndDep(month, year, branchId, departmentCode, groupid)
                    });
                else if (departmentCode == "0" && groupid != 0)
                    return Ok(new
                    {
                        Employee = getAllEmployeesByGroupId(month, year, branchId, groupid)
                    });
                else
                    return Ok(new
                    {
                        Employee = getAllEmployeesByDepdepartmentId(month, year, branchId, departmentCode)
                    });
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }

        }


        [HttpGet("timetable")]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        //https://localhost:5001/api/EmployeeAppearance/timetable?branchId=2&month=4&year=2019&departmentId=11500&groupid=5&allTimetableId=2&empid=3
        public async Task<IActionResult> GetEmployee([RequiredFromQuery] int branchId, [RequiredFromQuery]int groupid,
            [RequiredFromQuery] int month, [RequiredFromQuery] int year, [RequiredFromQuery] int allTimetableId,
            [RequiredFromQuery] string departmentCode, [RequiredFromQuery] int empid)
        {
            try
            {
                var c = await GetEmployeeCollector(branchId, groupid, month, year, allTimetableId, departmentCode, empid);
                return Ok(c);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [NonAction]
        public async Task<EmployeesByDepartment> GetEmployeeCollector(int branchId, int groupid, int month, int year,
            int allTimetableId, string departmentCode, int empid, List<Claim> userClaim = null)
        {
            if ((departmentCode == null || departmentCode == "0") && empid == 0)
            {
                List<Departments> subDepartments;
                var IsNormalDepartmentsStructure =  actima.Departments.Any(departments => departments.Root.DepartmentId == branchId);
                var userClaims = userClaim ?? HttpContext.User.Claims.ToList();
                var allowedDepartments = _authentication.GetDepartmentsListFromClaims(userClaims);
                if(IsNormalDepartmentsStructure)
                {
                     subDepartments = await actima.Departments
                        .Where(departments => departments.Root.DepartmentId == branchId)
                        .AsNoTracking()
                        .ToListAsync();
                }
                else
                {
                     subDepartments = await actima.Departments
                        .Where(departments => departments.DepartmentId == branchId)
                        .AsNoTracking()
                        .ToListAsync();
                }

                var allowedSubDepartments = subDepartments
                    .Where(departments => allowedDepartments.Contains(departments.DepartmentId))
                    .ToList();
                if (allowedSubDepartments.Count != subDepartments.Count)
                {
                    departmentCode = allowedSubDepartments.FirstOrDefault().Code;
                }
            }

            var emp = new EmployeesByDepartment();

            switch (groupid)
            {
                case 0 when departmentCode != "0" && empid != 0:
                    emp.Departments = getOneEmployeeWoGroup(branchId, month, year, allTimetableId, departmentCode.ToString(), empid);
                    break;
                case 0 when departmentCode == "0" && empid != 0:
                    emp.Departments = getOneEmployeeWoGroupDep(branchId, month, year, allTimetableId, empid);
                    break;
                case 0 when departmentCode == "0" && empid == 0:

                    emp.Departments = getOneEmployeeWoAll(branchId, month, year, allTimetableId);
                    break;
                case 0 when departmentCode != "0" && empid == 0:
                    emp.Departments = getOneEmployeeWoGroupEmp(branchId, month, year, allTimetableId, departmentCode.ToString());
                    break;
                default:
                    {
                        if (departmentCode != "0" && empid != 0)
                            emp.Departments = GetOneEmployee(branchId, groupid, month, year, allTimetableId, departmentCode.ToString(),
                            empid);
                        else if (departmentCode != "0" && empid == 0)
                            emp.Departments =
                            getOneEmployeeWoEmp(branchId, groupid, month, year, allTimetableId, departmentCode.ToString());
                        else switch (departmentCode)
                            {
                                case "0" when empid != 0:
                                    emp.Departments = getOneEmployeeWoDep(branchId, groupid, month, year, allTimetableId, empid);
                                    break;
                                case "0" when empid == 0:
                                    emp.Departments = getOneEmployeeWoDepEmp(branchId, groupid, month, year, allTimetableId);
                                    break;
                                default:
                                    return null;
                            }
                        break;
                    }
            }
            return emp;
        }
        //done
        private List<EmpDep> GetOneEmployee(int branchId, int groupid, int month, int year, int allTimetableId, string departmentId, int empid)
        {
            var b = new BranchEmployeesController(actima);
            if (groupid == -1)
            {
                var a = (from deps in actima.Departments
                         where deps.Code == departmentId
                         select new EmpDep
                         {
                             DepartmentId = departmentId,
                             Employees = (from employee in actima.Employees
                                          join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                          join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                          join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                          where (worknorm.Date.Month == month &&
                                                worknorm.Date.Year == year &&
                                                !(from eg in actima.EmployeeGroups2Employee
                                                  select eg.RecordId).Contains((int)empdet.RecordId) &&
                                                employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                                dep.Code == departmentId &&
                                                empdet.EmployeeId == empid &&
                                                employee.Fired > DateTime.Now)
                                          select new EmployeeAppearanceData
                                          {
                                              EmployeeId = empdet.EmployeeId,
                                              TabelNumber = employee.AccountNumber,
                                              Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                              TotalWorksDay = worknorm.NormDays,
                                              NormOfHours = worknorm.NormHours,
                                              Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                              ? ((from emp in actima.Employees
                                                  join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                                  join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                                  join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                                  where (
                                                  emp.EmployeeId == empid &&
                                                  sswt.ScheduleDate.Date.Month == month &&
                                                  sswt.ScheduleDate.Date.Year == year &&
                                                  Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                                  orderby sswt.ScheduleDate.Date.Day
                                                  select new InformationForDay
                                                  {
                                                      Number = sswt.ScheduleDate.Date.Day,
                                                      TypeDay = "work",
                                                      Start = shifts.ShiftStart.ToString("HH:mm"),
                                                      End = shifts.ShiftEnd.ToString("HH:mm"),
                                                      Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                             (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                             (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                                  }).AsQueryable().Distinct().ToList()) :
                                                  (from emp in actima.Employees
                                                   join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                   join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                   join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                   where (
                                                       emp.EmployeeId == empid &&
                                                   timetable.Date.Month == month &&
                                                   timetable.Date.Year == year &&
                                                   timetable.TimetableId == allTimetableId &&
                                                   Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                   orderby timetable.Date.Day
                                                   select new InformationForDay()
                                                   {
                                                       Number = timetable.Date.Day,
                                                       TypeDay = "work",
                                                       Start = timetable.ShiftStart.ToString("HH:mm"),
                                                       End = timetable.ShiftEnd.ToString("HH:mm"),
                                                       Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                              (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                              (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                   })
                                                      .AsQueryable().ToList()

                                          }).ToList()
                         }).ToList();
                return a;
            }
            else
            {
                var a = (from deps in actima.Departments
                         where deps.Code == departmentId
                         select new EmpDep
                         {
                             DepartmentId = departmentId,
                             Employees = (from employee in actima.Employees
                                          join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                          join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                          join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                          join empgr2emp in actima.EmployeeGroups2Employee on empdet.RecordId equals empgr2emp.RecordId
                                          where (worknorm.Date.Month == month &&
                                                worknorm.Date.Year == year &&
                                                empgr2emp.EmployeeGroupId == groupid &&
                                                employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                                dep.Code == departmentId &&
                                                empdet.EmployeeId == empid &&
                                                employee.Fired > DateTime.Now)
                                          select new EmployeeAppearanceData
                                          {
                                              EmployeeId = empdet.EmployeeId,
                                              TabelNumber = employee.AccountNumber,
                                              Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                              TotalWorksDay = worknorm.NormDays,
                                              NormOfHours = worknorm.NormHours,
                                              Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                              ? ((from emp in actima.Employees
                                                  join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                                  join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                                  join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                                  where (
                                                  emp.EmployeeId == empid &&
                                                  sswt.ScheduleDate.Date.Month == month &&
                                                  sswt.ScheduleDate.Date.Year == year &&
                                                  Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                                  orderby sswt.ScheduleDate.Date.Day
                                                  select new InformationForDay
                                                  {
                                                      Number = sswt.ScheduleDate.Date.Day,
                                                      TypeDay = "work",
                                                      Start = shifts.ShiftStart.ToString("HH:mm"),
                                                      End = shifts.ShiftEnd.ToString("HH:mm"),
                                                      Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                             (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                             (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                                  }).AsQueryable().Distinct().ToList()) :
                                                  (from emp in actima.Employees
                                                   join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                   join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                   join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                   where (
                                                       emp.EmployeeId == empid &&
                                                   timetable.Date.Month == month &&
                                                   timetable.Date.Year == year &&
                                                   timetable.TimetableId == allTimetableId &&
                                                   Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                   orderby timetable.Date.Day
                                                   select new InformationForDay()
                                                   {
                                                       Number = timetable.Date.Day,
                                                       TypeDay = "work",
                                                       Start = timetable.ShiftStart.ToString("HH:mm"),
                                                       End = timetable.ShiftEnd.ToString("HH:mm"),
                                                       Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                              (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                              (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                   })
                                                      .AsQueryable()
                                                      .ToList()
                                          }).ToList()

                         }).ToList();
                return a;
            }
        }
        //done
        private List<EmpDep> getOneEmployeeWoGroup(int branchId, int month, int year, int allTimetableId, string departmentId, int empid)
        {
            var b = new BranchEmployeesController(actima);
            var a = (from deps in actima.Departments
                     where deps.Code == departmentId
                     select new EmpDep
                     {
                         DepartmentId = departmentId,
                         Employees = (from employee in actima.Employees
                                      join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                      join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                      join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                      join empgr2emp in actima.EmployeeGroups2Employee on empdet.RecordId equals empgr2emp.RecordId
                                      join empgr in actima.EmployeeGroups on empgr2emp.EmployeeGroupId equals empgr.EmployeeGroupId
                                      where (worknorm.Date.Month == month &&
                                            worknorm.Date.Year == year &&
                                            employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                            dep.Code == departmentId &&
                                            empdet.EmployeeId == empid &&
                                            empdet.EndDate > DateTime.Now)

                                      select new EmployeeAppearanceData
                                      {
                                          EmployeeId = empdet.EmployeeId,
                                          TabelNumber = employee.AccountNumber,
                                          Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                          TotalWorksDay = worknorm.NormDays,
                                          NormOfHours = worknorm.NormHours,
                                          Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == empid &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                   emp.EmployeeId == empid &&
                                                   timetable.Date.Month == month &&
                                                   timetable.Date.Year == year &&
                                                   timetable.TimetableId == allTimetableId &&
                                                   Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                  (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                  (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                              .AsQueryable()
                                              .ToList()
                                      }).ToList()
                     }).ToList();
            return a;
        }
        //done
        private List<EmpDep> getOneEmployeeWoDep(int branchId, int groupid, int month, int year, int allTimetableId, int empid)
        {
            var b = new BranchEmployeesController(actima);
            if (groupid == -1)
            {
                var a = (from deps in actima.Departments
                         where deps.Code == getDepByEmp(empid)
                         select new EmpDep
                         {
                             DepartmentId = getDepByEmp(empid),
                             Employees = (from employee in actima.Employees
                                          join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                          join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                          join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                          where (worknorm.Date.Month == month &&
                                                worknorm.Date.Year == year &&
                                                !(from eg in actima.EmployeeGroups2Employee
                                                  select eg.RecordId).Contains((int)empdet.RecordId) &&
                                                employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                                empdet.EmployeeId == empid &&
                                                dep.Code == getDepByEmp(empid))
                                          select new EmployeeAppearanceData
                                          {
                                              EmployeeId = empdet.EmployeeId,
                                              TabelNumber = employee.AccountNumber,
                                              Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                              TotalWorksDay = worknorm.NormDays,
                                              NormOfHours = worknorm.NormHours,
                                              Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == empid &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                    emp.EmployeeId == empid &&
                                                timetable.Date.Month == month &&
                                                timetable.Date.Year == year &&
                                                timetable.TimetableId == allTimetableId &&
                                                Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                                  .AsQueryable().ToList()

                                          }).ToList()
                         }).ToList();
                return a;
            }
            else
            {
                var a = (from deps in actima.Departments
                         where deps.Code == getDepByEmp(empid)
                         select new EmpDep
                         {
                             DepartmentId = getDepByEmp(empid),
                             Employees = (from employee in actima.Employees
                                          join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                          join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                          join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                          join empgr2emp in actima.EmployeeGroups2Employee on empdet.RecordId equals empgr2emp.RecordId
                                          join empgr in actima.EmployeeGroups on empgr2emp.EmployeeGroupId equals empgr.EmployeeGroupId
                                          where (worknorm.Date.Month == month &&
                                                worknorm.Date.Year == year &&
                                                empgr2emp.EmployeeGroupId == groupid &&
                                                employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                                empdet.EmployeeId == empid &&
                                                dep.Code == getDepByEmp(empid))
                                          select new EmployeeAppearanceData
                                          {
                                              EmployeeId = empdet.EmployeeId,
                                              TabelNumber = employee.AccountNumber,
                                              Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                              TotalWorksDay = worknorm.NormDays,
                                              NormOfHours = worknorm.NormHours,
                                              Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == empid &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                    emp.EmployeeId == empid &&
                                                timetable.Date.Month == month &&
                                                timetable.Date.Year == year &&
                                                timetable.TimetableId == allTimetableId &&
                                                Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                                  .AsQueryable()
                                                  .ToList()
                                          }).ToList()

                         }).ToList();
                return a;
            }
        }
        //done
        private List<EmpDep> getOneEmployeeWoGroupDep(int branchId, int month, int year, int allTimetableId, int empid)
        {
            var b = new BranchEmployeesController(actima);
            var a = (from deps in actima.Departments
                     where deps.Code == getDepByEmp(empid)
                     select new EmpDep
                     {
                         DepartmentId = getDepByEmp(empid),
                         Employees = (from employee in actima.Employees
                                      join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                      join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                      join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                      where (worknorm.Date.Month == month &&
                                            worknorm.Date.Year == year &&
                                            employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == employee.EmployeeId) &&
                                            empdet.EmployeeId == empid &&
                                            employee.Fired > DateTime.Now &&
                                            dep.DepartmentId > 3 &&
                                            dep.Code == getDepByEmp(empid)
                                            )
                                      select new EmployeeAppearanceData
                                      {
                                          EmployeeId = empdet.EmployeeId,
                                          TabelNumber = employee.AccountNumber,
                                          Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                          TotalWorksDay = worknorm.NormDays,
                                          NormOfHours = worknorm.NormHours,
                                          Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == empid &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                           emp.EmployeeId == empid &&
                                       timetable.Date.Month == month &&
                                       timetable.Date.Year == year &&
                                       timetable.TimetableId == allTimetableId &&
                                       Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                  (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                  (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                              .AsQueryable()
                                              .ToList()
                                      }).ToList()

                     }).ToList();
            return a;
        }

        private List<EmpDep> getOneEmployeeWoAll(int branchId, int month, int year, int allTimetableId)
        {
            var b = new BranchEmployeesController(actima);
            var a = (from deps in actima.Departments
                     where deps.RootId == branchId
                     select new EmpDep
                     {
                         DepartmentId = deps.Code,
                         Employees = (from employee in actima.Employees
                                      join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                      join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                      join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                      where (worknorm.Date.Month == month &&
                                            worknorm.Date.Year == year &&
                                            employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                            empdet.EndDate > DateTime.Now &&
                                            dep.Code == deps.Code
                                            )
                                      orderby empdet.LastName
                                      select new EmployeeAppearanceData
                                      {
                                          EmployeeId = empdet.EmployeeId,
                                          TabelNumber = employee.AccountNumber,
                                          Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                          TotalWorksDay = worknorm.NormDays,
                                          NormOfHours = worknorm.NormHours,
                                          Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == emp.EmployeeId) &&
                                              emp.EmployeeId == empdet.EmployeeId &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == timetable.EmployeeId) &&
                                                emp.EmployeeId == empdet.EmployeeId &&
                                                timetable.Date.Month == month &&
                                                timetable.Date.Year == year &&
                                                timetable.TimetableId == allTimetableId &&
                                                Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                              .AsQueryable()
                                              .ToList()
                                      }).ToList()
                     }).ToList();
            return a;
        }

        private List<EmpDep> getOneEmployeeWoGroupEmp(int branchId, int month, int year, int allTimetableId, string departmentId)
        {
            var b = new BranchEmployeesController(actima);
            var a = (from deps in actima.Departments
                     where deps.Code == departmentId
                     select new EmpDep
                     {
                         DepartmentId = departmentId,
                         Employees = (from employee in actima.Employees
                                      join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                      join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                      join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                      where (worknorm.Date.Month == month &&
                                            worknorm.Date.Year == year &&
                                            employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                            empdet.EndDate > DateTime.Now &&
                                            dep.Code == departmentId
                                            )
                                      orderby empdet.LastName
                                      select new EmployeeAppearanceData
                                      {
                                          EmployeeId = empdet.EmployeeId,
                                          TabelNumber = employee.AccountNumber,
                                          Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                          TotalWorksDay = worknorm.NormDays,
                                          NormOfHours = worknorm.NormHours,
                                          Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == emp.EmployeeId) &&
                                              emp.EmployeeId == empdet.EmployeeId &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0
                                              )
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == timetable.EmployeeId) &&
                                                emp.EmployeeId == empdet.EmployeeId &&
                                                timetable.Date.Month == month &&
                                                timetable.Date.Year == year &&
                                                timetable.TimetableId == allTimetableId &&
                                                Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                              .AsQueryable()
                                              .ToList()
                                      }).ToList()

                     }).ToList();
            return a;
        }

        private List<EmpDep> getOneEmployeeWoDepEmp(int branchId, int groupid, int month, int year, int allTimetableId)
        {
            var b = new BranchEmployeesController(actima);
            if (groupid == -1)
            {
                var a = (from deps in actima.Departments
                         where deps.RootId == branchId
                         select new EmpDep
                         {
                             DepartmentId = deps.Code,
                             Employees = (from employee in actima.Employees
                                          join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                          join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                          join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                          where (worknorm.Date.Month == month &&
                                                worknorm.Date.Year == year &&
                                                !(from eg in actima.EmployeeGroups2Employee
                                                  select eg.RecordId).Contains((int)empdet.RecordId) &&
                                                employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                                Convert.ToDateTime(employee.Fired) > DateTime.Now &&
                                                Convert.ToDateTime(empdet.EndDate) > DateTime.Now &&
                                                dep.Code == deps.Code)
                                          orderby empdet.LastName
                                          select new EmployeeAppearanceData
                                          {
                                              EmployeeId = empdet.EmployeeId,
                                              TabelNumber = employee.AccountNumber,
                                              Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                              TotalWorksDay = worknorm.NormDays,
                                              NormOfHours = worknorm.NormHours,
                                              Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == empdet.EmployeeId &&
                                              emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == emp.EmployeeId) &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                emp.EmployeeId == empdet.EmployeeId &&
                                                emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == timetable.EmployeeId) &&
                                                timetable.Date.Month == month &&
                                                timetable.Date.Year == year &&
                                                timetable.TimetableId == allTimetableId &&
                                                Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                                  .AsQueryable().ToList()

                                          }).ToList()
                         }).ToList();
                return a;
            }
            else
            {
                var a = (from deps in actima.Departments
                         where deps.RootId == branchId
                         select new EmpDep
                         {
                             DepartmentId = deps.Code,
                             Employees = (from employee in actima.Employees
                                          join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                          join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                          join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                          join empgr2emp in actima.EmployeeGroups2Employee on empdet.RecordId equals empgr2emp.RecordId
                                          join empgr in actima.EmployeeGroups on empgr2emp.EmployeeGroupId equals empgr.EmployeeGroupId
                                          where (worknorm.Date.Month == month &&
                                                worknorm.Date.Year == year &&
                                                empgr2emp.EmployeeGroupId == groupid &&
                                                employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                                dep.Code == deps.Code &&
                                                Convert.ToDateTime(employee.Fired) > DateTime.Now &&
                                                empdet.EndDate > DateTime.Now)
                                          orderby empdet.LastName
                                          select new EmployeeAppearanceData
                                          {
                                              EmployeeId = empdet.EmployeeId,
                                              TabelNumber = employee.AccountNumber,
                                              Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                              TotalWorksDay = worknorm.NormDays,
                                              NormOfHours = worknorm.NormHours,
                                              Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == empdet.EmployeeId &&
                                              emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == emp.EmployeeId) &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                emp.EmployeeId == empdet.EmployeeId &&
                                                emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == timetable.EmployeeId) &&
                                                timetable.Date.Month == month &&
                                                timetable.Date.Year == year &&
                                                timetable.TimetableId == allTimetableId &&
                                                Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                                  .AsQueryable()
                                                  .ToList()
                                          }).ToList()

                         }).ToList();
                return a;
            }
        }

        private List<EmpDep> getOneEmployeeWoEmp(int branchId, int groupid, int month, int year, int allTimetableId, string departmentId)
        {
            var b = new BranchEmployeesController(actima);
            if (groupid == -1)
            {
                var a = (from deps in actima.Departments
                         where deps.Code == departmentId
                         select new EmpDep
                         {
                             DepartmentId = departmentId,
                             Employees = (from employee in actima.Employees
                                          join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                          join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                          join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                          where (worknorm.Date.Month == month &&
                                                worknorm.Date.Year == year &&
                                                !(from eg in actima.EmployeeGroups2Employee
                                                  select eg.RecordId).Contains((int)empdet.RecordId) &&
                                                employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == empdet.EmployeeId) &&
                                                dep.Code == departmentId &&
                                                dep.Code == deps.Code)
                                          orderby empdet.LastName
                                          select new EmployeeAppearanceData
                                          {
                                              EmployeeId = empdet.EmployeeId,
                                              TabelNumber = employee.AccountNumber,
                                              Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                              TotalWorksDay = worknorm.NormDays,
                                              NormOfHours = worknorm.NormHours,
                                              Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == empdet.EmployeeId &&
                                              emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == emp.EmployeeId) &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                emp.EmployeeId == empdet.EmployeeId &&
                                                emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == timetable.EmployeeId) &&
                                                timetable.Date.Month == month &&
                                                timetable.Date.Year == year &&
                                                timetable.TimetableId == allTimetableId &&
                                                Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                                  .AsQueryable().ToList()

                                          }).ToList()
                         }).ToList();
                return a;
            }
            else
            {
                var a = (from deps in actima.Departments
                         where deps.Code == departmentId
                         select new EmpDep
                         {
                             DepartmentId = departmentId,
                             Employees = (from employee in actima.Employees
                                          join empdet in actima.EmployeesDetails.Distinct() on employee.EmployeeId equals empdet.EmployeeId
                                          join dep in actima.Departments on empdet.DepartmentId equals dep.DepartmentId
                                          join worknorm in actima.WorkTimeNormatives on empdet.RecordId equals worknorm.RecordId
                                          join empgr2emp in actima.EmployeeGroups2Employee on empdet.RecordId equals empgr2emp.RecordId
                                          join empgr in actima.EmployeeGroups on empgr2emp.EmployeeGroupId equals empgr.EmployeeGroupId
                                          where (worknorm.Date.Month == month &&
                                                worknorm.Date.Year == year &&
                                                empgr2emp.EmployeeGroupId == groupid &&
                                                employee.EmployeeId == b.GetEmployees(branchId).Find(x => x == employee.EmployeeId) &&
                                                dep.Code == deps.Code &&
                                                empdet.EndDate > DateTime.Now)

                                          orderby empdet.LastName
                                          select new EmployeeAppearanceData
                                          {
                                              EmployeeId = empdet.EmployeeId,
                                              TabelNumber = employee.AccountNumber,
                                              Name = empdet.LastName + " " + empdet.FirstName.Substring(0, 1).ToUpper() + ". " + empdet.MiddleName.Substring(0, 1).ToUpper() + ".",
                                              TotalWorksDay = worknorm.NormDays,
                                              NormOfHours = worknorm.NormHours,
                                              Days = (getEmpWithFixedSchedule(month, year).Cast<int>().Contains(employee.EmployeeId))
                                          ? ((from emp in actima.Employees
                                              join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                                              join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                                              join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                                              where (
                                              emp.EmployeeId == empdet.EmployeeId &&
                                              emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == emp.EmployeeId) &&
                                              sswt.ScheduleDate.Date.Month == month &&
                                              sswt.ScheduleDate.Date.Year == year &&
                                              Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2) != 0)
                                              orderby sswt.ScheduleDate.Date.Day
                                              select new InformationForDay
                                              {
                                                  Number = sswt.ScheduleDate.Date.Day,
                                                  TypeDay = "work",
                                                  Start = shifts.ShiftStart.ToString("HH:mm"),
                                                  End = shifts.ShiftEnd.ToString("HH:mm"),
                                                  Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) - 1 :
                                                         (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0)) - 1
                                              }).AsQueryable().Distinct().ToList()) :
                                              ((from emp in actima.Employees
                                                join timetable in actima.Timetable on employee.EmployeeId equals timetable.EmployeeId
                                                join alltime in actima.AllTimetables on timetable.TimetableId equals alltime.TimetableId
                                                join timetypes in actima.TimetablesTypes on alltime.TypeId equals timetypes.TypeId
                                                where (
                                                emp.EmployeeId == empdet.EmployeeId &&
                                                emp.EmployeeId == b.GetEmployees(branchId).Find(x => x == timetable.EmployeeId) &&
                                                timetable.Date.Month == month &&
                                                timetable.Date.Year == year &&
                                                timetable.TimetableId == allTimetableId &&
                                                Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2) != 0)
                                                orderby timetable.Date.Day
                                                select new InformationForDay()
                                                {
                                                    Number = timetable.Date.Day,
                                                    TypeDay = "work",
                                                    Start = timetable.ShiftStart.ToString("HH:mm"),
                                                    End = timetable.ShiftEnd.ToString("HH:mm"),
                                                    Plan = ((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours % 0.5 == 0) ?
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 2)) - 1 :
                                                           (Math.Round((timetable.ShiftEnd).Subtract(timetable.ShiftStart).TotalHours, 0)) - 1
                                                }))
                                                  .AsQueryable()
                                                  .ToList()
                                          }).ToList()

                         }).ToList();
                return a;
            }
        }
        private IQueryable getTimeTableNames(int month, int year, int branchId)
        {
            var ttname = from e in actima.AllTimetables
                         where e.TimetablesDate.Month == month && e.BranchId == branchId && e.TimetablesDate.Year == year && e.TypeId < 3
                         select new
                         {
                             allTimetableId = e.TimetableId,
                             name = e.TimetablesName,
                             typeid = e.TypeId,
                             editDate = e.EditDate.ToString("yyyy-MM-dd")
                         };
            return ttname;

        }
        private string getDepByEmp(int empid)
        {
            var dep = (from e in actima.EmployeesDetails
                       join d in actima.Departments on e.DepartmentId equals d.DepartmentId
                       where e.EmployeeId == empid
                       select d.Code).FirstOrDefault();
            return dep;
        }
        private List<string> getDepByBranch(int branchid)
        {
            var dep = (from e in actima.Departments
                       where e.RootId == branchid
                       select e.Code).ToList();
            return dep;
        }
        private IQueryable getAllEmployees(int month, int year, int branchId)
        {
            var userid = Convert.ToInt32(User.Claims.FirstOrDefault(claim => claim.Type == "Id")?.Value);
            BranchEmployeesController b = new BranchEmployeesController(actima);
            var emp = (from e in actima.EmployeesDetails
                       join t in actima.Employees on e.EmployeeId equals t.EmployeeId
                       join d in actima.Departments on e.DepartmentId equals d.DepartmentId
                       join rights in actima.UserDepartmentRights on e.DepartmentId equals rights.DepartmentId
                       join users in actima.Users on rights.UserId equals users.UserId
                       where (e.EmployeeId == b.GetEmployees(branchId).Find(x => x == t.EmployeeId) &&
                               d.DepartmentId > 3 &&
                               Convert.ToDateTime(t.Fired) > DateTime.Now && rights.UserId == userid && rights.Enabled == 1)
                       select new
                       {
                           id = e.EmployeeId,
                           name = e.LastName + " " + e.FirstName.Substring(0, 1).ToUpper() + ". " + e.MiddleName.Substring(0, 1).ToUpper() + "."
                       }).OrderBy(p => p.name).Distinct();


            return emp;
        }

        private IQueryable getAllEmployeesByDepdepartmentId(int month, int year, int branchId, string departmentId)
        {
            BranchEmployeesController b = new BranchEmployeesController(actima);

            if (departmentId != "0")
            {
                var emp = (from e in actima.EmployeesDetails
                           join t in actima.Employees on e.EmployeeId equals t.EmployeeId
                           join d in actima.Departments on e.DepartmentId equals d.DepartmentId
                           where (e.EmployeeId == b.GetEmployees(branchId).Find(x => x == t.EmployeeId) &&
                                  d.Code == departmentId &&
                                  Convert.ToDateTime(t.Fired) > DateTime.Now)
                           select new
                           {
                               id = e.EmployeeId,
                               name = e.LastName + " " + e.FirstName.Substring(0, 1).ToUpper() + ". " + e.MiddleName.Substring(0, 1).ToUpper() + "."
                           }).OrderBy(p => p.name).Distinct();
                return emp;
            }
            else
            {
                var emp = getAllEmployees(month, year, branchId);
                return emp;
            }
        }

        private IQueryable getAllEmployeesByGroupId(int month, int year, int branchId, int groupid)
        {
            BranchEmployeesController b = new BranchEmployeesController(actima);
            if (groupid == -1)
            {
                var emp = getEmpWoGroup(branchId, "0");
                return emp;
            }
            else if (groupid != 0)
            {
                var employees = (from e in actima.EmployeesDetails
                                 join t in actima.Employees on e.EmployeeId equals t.EmployeeId
                                 join d in actima.Departments on e.DepartmentId equals d.DepartmentId
                                 join tt in actima.EmployeeGroups2Employee on e.RecordId equals tt.RecordId
                                 join q in actima.EmployeeGroups on tt.EmployeeGroupId equals q.EmployeeGroupId
                                 where (e.EmployeeId == b.GetEmployees(branchId).Find(x => x == t.EmployeeId) &&
                                        tt.EmployeeGroupId == groupid &&
                                        Convert.ToDateTime(t.Fired) > DateTime.Now)
                                 select new
                                 {
                                     id = e.EmployeeId,
                                     name = e.LastName + " " + e.FirstName.Substring(0, 1).ToUpper() + ". " + e.MiddleName.Substring(0, 1).ToUpper() + "."
                                 }).OrderBy(p => p.name).Distinct();
                return employees;
            }
            else
            {
                var employees = getAllEmployees(month, year, branchId);
                return employees;
            }

        }

        private IQueryable getAllEmployeesByGroupAndDep(int month, int year, int branchId, string departmentId, int groupid)
        {
            BranchEmployeesController b = new BranchEmployeesController(actima);
            if (departmentId != "0" && groupid != 0)
            {
                if (groupid == -1)
                {
                    var emp = getEmpWoGroup(branchId, departmentId);
                    return emp;
                }
                else
                {
                    var employees = (from e in actima.EmployeesDetails
                                     join t in actima.Employees on e.EmployeeId equals t.EmployeeId
                                     join p in actima.Timetable on t.EmployeeId equals p.EmployeeId
                                     join d in actima.Departments on e.DepartmentId equals d.DepartmentId
                                     join tt in actima.EmployeeGroups2Employee on e.RecordId equals tt.RecordId
                                     join q in actima.EmployeeGroups on tt.EmployeeGroupId equals q.EmployeeGroupId
                                     where (e.EmployeeId == b.GetEmployees(branchId).Find(x => x == t.EmployeeId) &&
                                            tt.EmployeeGroupId == groupid &&
                                            d.Code == departmentId &&
                                            Convert.ToDateTime(t.Fired) > DateTime.Now &&
                                            e.EndDate.Month < month &&
                                            p.Date.Month == month &&
                                            p.Date.Year == year)
                                     select new
                                     {
                                         id = e.EmployeeId,
                                         name = e.LastName + " " + e.FirstName.Substring(0, 1).ToUpper() + ". " + e.MiddleName.Substring(0, 1).ToUpper() + "."
                                     }).OrderBy(p => p.name).Distinct();
                    return employees;
                }
            }
            else
            {
                var employees = getAllEmployees(month, year, branchId);
                return employees;
            }
        }
        private List<InformationForDay> getEmpFixedShifts(int branchId, int empid, int month, int year)
        {
            BranchEmployeesController b = new BranchEmployeesController(actima);
            var days = (from emp in actima.Employees
                        join sswt in actima.ShiftScheduleWithTemplate on emp.EmployeeId equals sswt.EmployeeId
                        join sr in actima.ShiftRegimes on sswt.ShiftRegimeId equals sr.ShiftRegimeId
                        join shifts in actima.Shifts on sswt.ShiftId equals shifts.ShiftId
                        where (
                        emp.EmployeeId == empid &&
                        sswt.ScheduleDate.Date.Month == month &&
                        sswt.ScheduleDate.Date.Year == year)
                        orderby sswt.ScheduleDate.Date.Day
                        select new InformationForDay
                        {
                            Number = sswt.ScheduleDate.Date.Day,
                            TypeDay = "work",
                            Start = shifts.ShiftStart.ToString("HH:mm"),
                            End = shifts.ShiftEnd.ToString("HH:mm"),
                            Plan = ((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours % 0.5 == 0) ?
                                   (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 2)) :
                                   (Math.Round((shifts.ShiftEnd).Subtract(shifts.ShiftStart).TotalHours, 0))
                        }).AsQueryable().Distinct().ToList();

            return days;
        }
        private IQueryable getEmpWoGroup(int branchId, string departmentCode)
        {
            BranchEmployeesController b = new BranchEmployeesController(actima);
            if (departmentCode == "0")
            {
                var emp = (from e in actima.EmployeesDetails
                           join d in actima.Departments on e.DepartmentId equals d.DepartmentId
                           where e.EmployeeId == b.GetEmployees(branchId).Find(x => x == e.EmployeeId) && !(from eg in actima.EmployeeGroups2Employee
                                                                                                            select eg.RecordId).Contains((int)e.RecordId)
                                                                                                            && d.DepartmentId > 3
                           select new
                           {
                               id = e.EmployeeId,
                               name = e.LastName + " " + e.FirstName.Substring(0, 1).ToUpper() + ". " + e.MiddleName.Substring(0, 1).ToUpper() + "."

                           }).OrderBy(p => p.name).Distinct(); ;
                return emp;
            }
            else
            {
                var emp = (from e in actima.EmployeesDetails
                           join d in actima.Departments on e.DepartmentId equals d.DepartmentId
                           where e.EmployeeId == b.GetEmployees(branchId).Find(x => x == e.EmployeeId) && !(from eg in actima.EmployeeGroups2Employee
                                                                                                            select eg.RecordId).Contains((int)e.RecordId)
                                                                                                            && d.Code == departmentCode
                           select new
                           {
                               id = e.EmployeeId,
                               name = e.LastName + " " + e.FirstName.Substring(0, 1).ToUpper() + ". " + e.MiddleName.Substring(0, 1).ToUpper() + "."

                           }).OrderBy(p => p.name).Distinct(); ;
                return emp;
            }
        }
        private IQueryable getEmpWithFixedSchedule(int month, int year)
        {
            var emp = (from e in actima.ShiftScheduleWithTemplate
                       where e.ScheduleDate.Month == month &&
                       e.ScheduleDate.Year == year
                       orderby e.EmployeeId
                       select e.EmployeeId).AsQueryable().Distinct();
            return emp;
        }
        private IQueryable getDepart(int branchId)
        {
            var userid = Convert.ToInt32(User.Claims.FirstOrDefault(claim => claim.Type == "Id")?.Value);
            var deps = from e in actima.Departments
                       join rights in actima.UserDepartmentRights on e.DepartmentId equals rights.DepartmentId
                       join users in actima.Users on rights.UserId equals users.UserId
                       where e.RootId == branchId && rights.UserId == userid && rights.Enabled == 1
                       select new
                       {
                           id = e.DepartmentId,
                           name = e.Name,
                           code = e.Code
                       };
            return deps;
        }
        private async Task<List<EmployeeGroups>> getGroups()
        {
            return await actima.EmployeeGroups
                            .AsNoTracking()
                            .ToListAsync();

        }

    }
}
