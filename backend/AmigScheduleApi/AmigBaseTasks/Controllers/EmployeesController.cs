using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using ActimaDbEntities.Exceptions;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Filters;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Models.HelpClasses;
using AmigScheduleApi.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AmigScheduleApi.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class EmployeesController : Controller
    {
        private readonly IEmployeeControllerCollector _collector;

        public EmployeesController(IEmployeeControllerCollector collector)
        {
            _collector=collector;
        }
        /// <summary> Get all employees details from DB by branch-\department-\group</summary>
        /// <remarks>
        /// Sample request:
        /// GET /api/Employees       
        /// </remarks>
        /// <param name="dateTime" >(Required) Day on which sampling will be made in a format yyyy-mm-dd</param>
        /// <param name="allTimetableId" >(Required) AllTImeTable record Id in actima].[dbo].[AllTimetable] </param>
        /// <param name="departmentId">DepartmentId from [actima].[dbo].[Department] </param>
        /// <param name="groupId">Specific group ID</param>
        /// <returns> all required Employees Data</returns>
        /// <response code="200">Return all required Employees Data</response>
        /// <response code="400">branchId isn`t exist in [actima].[dbo].[Department]
        /// or department entity from [actima].[dbo].[Department]  with branchId isn`t branch
        /// or AllTimetable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]
        /// or there is no sub departments for branch row from  [actima].[dbo].[Department]</response>
        /// <response code="422">AllTimetable instance has different branchId from branchId from query
        /// or AllTimetable  instance has different date from dateTime</response>
        //https://localhost:5001/api/employees?dateTime=2019-03-18&branchId=2&allTimeTableId=5
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [HttpGet]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        public async Task<IActionResult> GetEmployeesData([Required,FromQuery] DateTime dateTime,
            [Required,FromQuery] int allTimetableId, [FromQuery] int departmentId = 0, [FromQuery] int groupId = -1)
        {
            try
            {
                return Ok(await _collector.GetEmployeesData(dateTime, allTimetableId, User.Claims.ToList(),
                    departmentId: departmentId, groupId: groupId));
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (UnprocessableEntityException e)
            {
                return UnprocessableEntity(e.Message);
            }
        }

        /// <summary> Get all employees details from DB by branch-\department-\group</summary>
        /// <remarks>
        /// Sample request:
        /// GET /api/Employees       
        /// </remarks>
        /// <param name="dateTime" >(Required) Day on which sampling will be made in a format yyyy-mm-dd</param>
        /// <param name="allTimetableId" >(Required) AllTImeTable record Id in actima].[dbo].[AllTimetable] </param>
        /// <param name="departmentId">DepartmentId from [actima].[dbo].[Department] </param>
        /// <param name="groupId">Specific group ID</param>
        /// <returns> all required Employees Data</returns>
        /// <response code="200">Return all required Employees Data</response>
        /// <response code="400">branchId isn`t exist in [actima].[dbo].[Department]
        /// or department entity from [actima].[dbo].[Department]  with branchId isn`t branch
        /// or AllTimetable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]
        /// or there is no sub departments for branch row from  [actima].[dbo].[Department]</response>
        /// <response code="422">AllTimetable instance has different branchId from branchId from query
        /// or AllTimetable  instance has different date from dateTime</response>
        //https://localhost:5001/api/employees?dateTime=2019-03-18&branchId=2&allTimeTableId=5
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [HttpGet("forMonth")]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        public async Task<IActionResult> GetEmployeesData4Mouth([Required, FromQuery] DateTime dateTime,
            [Required, FromQuery] int allTimetableId, [FromQuery] int departmentId = 0, [FromQuery] int groupId = -1)
        {
            try
            {
                return Ok(await _collector.GetEmployeesData4Month(dateTime, User.Claims.ToList(), allTimetableId,
                    departmentId: departmentId, groupId: groupId));
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (UnprocessableEntityException e)
            {
                return UnprocessableEntity(e.Message);
            }
            catch (NoContentException e)
            {
                return UnprocessableEntity(e.Message);
            }
    }

        /// <summary> Get all employees details from DB by employeeId</summary>
        /// <remarks>
        /// Sample request:
        /// GET /api/Employees/single      
        /// </remarks>
        /// <param name="allTimetableId" > AllTImeTable record Id in actima].[dbo].[AllTimetable] </param>
        /// <param name="dateTime">Date for search</param>
        /// <param name="employeeId" >EmployeeId from [actima].[dbo].[Employee]</param>
        /// <returns> all required Employees Data</returns>
        /// <response code="200">Return all required Employees Data</response>
        /// <response code="400">branchId isn`t exist in [actima].[dbo].[Department]
        /// or department entity from [actima].[dbo].[Department]  with branchId isn`t branch
        /// or AllTimetable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]
        /// or there is no sub departments for branch row from  [actima].[dbo].[Department]</response>
        /// <response code="422">AllTimetable instance has different branchId from branchId from query
        /// or AllTimetable  instance has different date from dateTime</response>
        //https://localhost:5001/api/employees/single?dateTime=2019-04-01%2000:00:00.000&employeeId=69&tableName=test
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        [HttpGet("single")]
        public async Task<IActionResult> GetEmployeesDataBySpecificEmployee([Required,FromQuery] DateTime dateTime, 
            [Required,FromQuery] int employeeId, [Required,FromQuery] int allTimetableId)
        {
            try
            {
                return Ok(await _collector.GetEmployeesDataBySpecificEmployee(dateTime, employeeId, 
                    allTimetableId, User.Claims.ToList()));
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
        }


        /// <summary> Get employee data for month by employee id</summary>
        /// <remarks>
        /// Sample request:
        /// GET /api/Employees       
        /// </remarks>
        /// <param name="dateTime" >(Required) Date on which graphic will be planned, format: yy-mm-any day</param>
        /// <param name="allTimetableId" >(Required) AllTImeTable record Id in [actima].[dbo].[AllTimetable] </param>
        /// <param name="employeeId" >EmployeeId from [actima].[dbo].[Employee]</param>
        /// <returns> all required Employees Data</returns>
        /// <response code="200">Return all required Employees data for month</response>
        /// <response code="400">branchId isn`t exist in [actima].[dbo].[Department]
        /// or department entity from [actima].[dbo].[Department]  with branchId isn`t branch
        /// or AllTimetable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]</response>
        /// <response code="422">AllTimetable instance has different branchId from branchId from query
        /// or AllTimetable  instance has different date from dateTime</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [Authorize]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        [HttpGet("employee-monthplan")]
        public async Task<IActionResult> GetOneEmployeeMonthPlan([Required, FromQuery] int allTimetableId,
           [Required, FromQuery] DateTime dateTime,
           [Required, FromQuery] int employeeId)
        {
            try
            {
                return Ok(await _collector.GetOneEmployeeInfo(dateTime, User.Claims.ToList(), allTimetableId, employeeId));
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
                
            }
        }
    }
}