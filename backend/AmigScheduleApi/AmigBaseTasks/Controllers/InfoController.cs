using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Filters;
using AmigScheduleApi.Models;
using AmigScheduleApi.Models.CodesResponseJsonObject;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Models.HelpClasses;
using AmigScheduleApi.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

namespace AmigScheduleApi.Controllers
{    
    [ApiController]
    [Authorize]
    [Route("/api/[controller]")]
    public class InfoController:Controller
    {
        private readonly IDepartmentLogic _departmentLogic;
        private readonly IEmployeeDetailsLogic _employeeDetails;
        private readonly INormatives _normatives;
        private readonly ITimeTablesLogic _tablesLogic;
        private readonly IAuthentication _authentication;

        public InfoController(IDepartmentLogic departmentLogic, IEmployeeDetailsLogic employeeDetails,
            INormatives normatives, ITimeTablesLogic tablesLogic, IAuthentication authentication)
        {
            _departmentLogic = departmentLogic;
            _employeeDetails = employeeDetails;
            _normatives = normatives;
            _tablesLogic = tablesLogic;
            _authentication = authentication;
        }
        
        /// <summary>Get all departments from [actima].[dbo].[Department] and groups details
        /// from [actima].[dbo].[EmployeeGroups]</summary>
        /// <remarks>
        /// Sample request:
        /// GET /info/departments       
        /// </remarks>
        /// <param name="branchId"> (Required) Department Id that is branch in [actima].[dbo].[Department] </param>
        /// <returns>all departments from [actima].[dbo].[Department] and groups details
        /// from [actima].[dbo].[EmployeeGroups] </returns>
        /// <response code="200">all departments from [actima].[dbo].[Department] and groups details</response>
        /// <response code="400"> Department with Id Like branchId isn`t branch</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpGet("departments")]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        public async Task<IActionResult> GetCompanyCodes([RequiredFromQuery]int branchId)
        {
            var branch = await _departmentLogic.GetBranchWithSubDepartments(branchId);
            var availableDepartmentIds = _authentication.GetDepartmentsListFromClaims(User.Claims.ToList());
            var availableSubDepartments = branch.InverseRoot
                .Where(departments => availableDepartmentIds.Contains(departments.DepartmentId))
                .ToList();
            var subDepartmentsIds = branch.InverseRoot
                .Select(departments => departments.DepartmentId)
                .ToList();
            var fullRights = subDepartmentsIds
                .Intersect(availableDepartmentIds).Count() == subDepartmentsIds.Count;
            var groups = await _employeeDetails.GetDictionaryForGroupNamesWoTracking();
            var companyCodes = new CompanyCodes()
            {
                BranchCodes= new List<BranchCodes>(),
                GroupsIds =  groups.Select(group => new GroupsInfo
                {
                    Id=group.Key,
                    Name=group.Value
                }).ToList()                
            };
        
            var departmentCodesCollection = availableSubDepartments
                .Select(department => new DepartmentCodes
                {
                    Code = department.Code,
                    Id = department.DepartmentId,
                    Name = department.Name                        
                }).ToList();
            companyCodes.BranchCodes.Add( new BranchCodes
            {
                Code=branch.Code,
                Id =branch.DepartmentId,
                Name = branch.Name,
                FullRights = fullRights,
                DepartmentCodes=departmentCodesCollection
            });

            return Ok(companyCodes);
        }

        /// <summary>Get all holidays from [actima].[dbo].[Holidays] for specific year</summary>
        /// <remarks>
        /// Sample request:
        /// GET /info/holidays       
        /// </remarks>
        /// <param name="year" >(Required) BranchId from [actima].[dbo].[Department])year as integer</param>
        /// <returns>all holidays from [actima].[dbo].[Holidays] for specific year </returns>
        /// <response code="200">all holidays from [actima].[dbo].[Holidays] for specific year</response>
        [ProducesResponseType(200)]
        [HttpGet("holidays")]
        public async Task<IActionResult> GetHolidays([RequiredFromQuery] int year)
        {
            var holidays = await _normatives.GetHolidaysForYear(year);
            if(holidays.Any())
                return Ok( holidays.Select(holiday =>new HolidaysData()
                {
                    HolidayName = holiday.Name,
                    HolidayDate = holiday.HolidayDate
                }));
            return NoContent();
        }

        /// <summary>Get all instance AllTimetablse from [actima].[dbo].[AllTimetables] for specific month  </summary>
        /// <remarks>
        /// Sample request:
        /// GET /info/alltimetable       
        /// </remarks>
        /// <param name="monthDate" >(Required) Date like yyyy-mm-01 if parameter does not match the pattern server will
        /// autocomplete </param>
        /// <param name="branchId">(Required) BranchId for to select specific [actima].[dbo].[AllTimetables] records
        /// </param>
        /// <returns>all instance AllTimetablse for specific month </returns>
        /// <response code="200"> all instance AllTimetablse for specific month</response>
        /// <response code="204"> There isn`t any time tables for specific month</response>
        /// <response code="400"> Department with Id Like branchId isn`t branch</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]        
        [HttpGet("alltimetables")]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        public async Task<IActionResult> GetAlltimeTables([RequiredFromQuery] DateTime monthDate, 
            [RequiredFromQuery] int branchId)
        {
            var monthCorrectDate = new DateTime(monthDate.Year,monthDate.Month,1);
            var branch = await _departmentLogic.GetDepartment(branchId);
            if (branch == null || !_departmentLogic.DepartmentIsBranch(branch))
                return BadRequest($"Department with Id Like {branchId} isn`t branch");
            var allTimeTables = await _tablesLogic.GetAllTimetablesForMonth(monthCorrectDate, branchId);
            if (!allTimeTables.Any()) return NoContent();
            return Ok(allTimeTables.Select(timetables => new AllTimetableFullData()
            {
                Id = timetables.TimetableId,
                EditDate = timetables.EditDate,
                BranchId = timetables.BranchId,
                Name = timetables.TimetablesName?.Trim(),
                TimetableDate = timetables.TimetablesDate,
                TypeId = timetables.TypeId
            }));
        }

        /// <summary>Get short information about employees from specific branch and for specific day</summary>
        /// <remarks>
        /// Sample request:
        /// GET /info/employees       
        /// </remarks>
        /// <param name="day" > (Required) Date like yyyy-mm-dd time will be auto rejected</param>
        /// <param name="branchId"> (Required) BranchId for to select specific [actima].[dbo].[AllTimetables] records
        /// </param>
        /// <returns> Short information about employees from specific branch and for specific day </returns>
        /// <response code="200"> Short information about employees from specific branch and for specific day</response>
        /// <response code="400"> Department with Id Like branchId hasn't sub departments</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]   
        [HttpGet("employees")]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        public async Task<IActionResult> GetEmployeesShortInfo([Required,FromQuery] int branchId,
            [Required,FromQuery] DateTime day)
        {
            var correctDayForm  = new DateTime(day.Year,day.Month,day.Day);
            var departments = await _departmentLogic.GetSubDepartments(branchId);
            var availableDepartments = _authentication.GetDepartmentsListFromClaims(User.Claims.ToList());

                departments = departments
                    .Where(departments1 => availableDepartments.Contains(departments1.DepartmentId))
                    .ToList();
                var employees = await _employeeDetails.GetSimpleEmployeeInfo(
                    departments.Count == 0 ? new List<int> { branchId } :  departments.Select(
                    departments1 => departments1.DepartmentId).ToList(), correctDayForm);
                return Ok(employees.OrderBy(data => data.Code));
                
        }
    }
}