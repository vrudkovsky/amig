using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ActimaDbEntities.Exceptions;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Models.CodesResponseJsonObject;
using AmigScheduleApi.Models.HelpClasses;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace AmigScheduleApi.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IAuthentication _authentication;
        private readonly IDepartmentLogic _department;
        
        public AccountController(IConfiguration configuration, IAuthentication authentication,
            IDepartmentLogic department)
        {
            _configuration = configuration;
            _authentication = authentication;
            _department = department;
        }
        /// <summary>
        ///  LogIn active users from Db. Create Cookie session with or w/o strong persistence for 100 days 
        /// </summary>
        /// <param name="form">Form for SingIn. Persistence determines strong persistence for cookie </param>
        /// <returns>Local redirect to home page or Error statusCode</returns>
        /// <response code ="401">See detail in WWW-Authenticate header</response>
        /// <response code ="500">Server error see logs</response>
        [ProducesResponseType(401)]
        [AllowAnonymous]
        [HttpPost("SingIn")]
        public async Task<IActionResult> SingIn([FromForm, Required] UserLoginFormData form )
        {
            List<Claim> claims;
            try
            {
                claims = await _authentication.GetClaimsForCookies(form.Login, form.Password);
            }
            catch (InternalServerErrorException e)
            {
                return StatusCode(500,e.Message);
            }
            catch (UnauthorizedException)
            {
                //all data in WWW-Authenticate header
                return Unauthorized();
            }
            var claimsIdentity = new ClaimsIdentity(
                claims, 
                CookieAuthenticationDefaults.AuthenticationScheme);

            const int expireDay = 100;
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                new AuthenticationProperties()
                {
                    IsPersistent = form.Persistent,
                    ExpiresUtc =  DateTimeOffset.UtcNow.AddDays(expireDay),
                    AllowRefresh = true,
                    IssuedUtc = DateTimeOffset.UtcNow,
                });
            var userId = claims.FirstOrDefault(claim => claim.Type =="Id")?.Value;
            Log.Information("User with id like  {0} was SingIn to the system with " + 
                (form.Persistent? "strong persistence for {1} days" :"weak persistence "), userId,
                expireDay);
            return Ok();
        }

        /// <summary>
        /// SingOut user from system (delete cookie)
        /// </summary>
        /// <returns>Local redirect to homepage</returns>
        [Authorize]
        [HttpPost("SingOut")]
        public async Task<IActionResult> SingOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var userLogin = User.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Name)?.Value;
            Log.Information("User {0} was SingOut from system" , userLogin);
            return Ok();
        }
        /// <summary>  Get information about account </summary>
        /// <response code="200">information about account</response>
        /// <response code ="500">Server error see logs</response>
        /// <returns>information about account</returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        [Authorize]
        [HttpGet("info")]
        public async Task<IActionResult> AccountInformation()
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(claim => claim.Type == "Id")?.Value);
            var user = await _authentication.GetUserById(userId);
            if (userId == default || user == null)
            {
                Log.Warning("User has no claims about his UserId {0} or hasn't Enabled information " +
                            "in [actima].[dbo].[UserDepartmentRights]", userId);
                return StatusCode(500, "Problem with Cookies, see server logs");
            }

            var branchIds = await _department.GetBranchIds();
            var departmentsIds = _authentication.GetDepartmentsListFromClaims(User.Claims.ToList());
            branchIds = branchIds.Where(i => departmentsIds.Contains(i))
                .ToList();
            var branchCodes = new ShortBranchData()
            {
                Id = user.UserId,
                Description = user.Description,
                Name = user.UserShortName,
                Login = user.UserLogin,
                BranchCodes = new List<BranchCodes>(){}
            };
            foreach (var id in branchIds)
            {
                var branchData = await _department.GetDepartment(id);
                var subDepartments = branchData.InverseRoot;
                var departmentsCodes = branchData.InverseRoot.Where(departments => departmentsIds
                        .Contains(departments.DepartmentId))
                        .ToList()
                        .Select(departments => new DepartmentCodes()
                        {
                            Code= departments.Code,
                            Id = departments.DepartmentId,
                            Name = departments.Name
                        })
                        .ToList();
                var fullRightsForBranch =
                    subDepartments.Select(departments => departments.DepartmentId).Intersect(departmentsIds).Count() ==
                    subDepartments.Count;
                branchCodes.BranchCodes.Add( new BranchCodes()
                {
                    Code = branchData.Code,
                    Id= branchData.DepartmentId,
                    Name = branchData.Name,
                    FullRights = fullRightsForBranch,
                    DepartmentCodes = departmentsCodes
                });
            }
            return Ok(branchCodes);
        }
    }
}