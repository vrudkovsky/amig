﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.Exceptions;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Filters;
using AmigScheduleApi.Models.EmployeesResponseObjectsJson;
using AmigScheduleApi.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace AmigScheduleApi.Controllers
{
    [ApiController]
    [Authorize]
    [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
    [Route("/api/[controller]")]
    public class ExcelController : Controller
    {
        private readonly IEmployeeControllerCollector _collector;
        private readonly IAuthentication _authentication;
        private readonly IFirstExcelReport _firstExcelReport;
        private readonly IPlanFactReportLogic _planFactReport;
        private readonly IThirdExcelReport _thirdExcelReport;
        private readonly IEmployeeDetailsLogic _detailsLogic;
        private readonly ActimaContext _hereWeGoAgain;
        private readonly ISecondExcelReport _secondExcelReport;
        private readonly IMonthPlanningExcelReport _monthPlanningExcelReport;
        private readonly IDepartmentLogic _departmentLogic;

        private const string FileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public ExcelController(IEmployeeControllerCollector collector, IFirstExcelReport excelReport,
            IPlanFactReportLogic planFactReport, IThirdExcelReport thirdExcelReport, IEmployeeDetailsLogic detailsLogic,
            ActimaContext hereWeGoAgain, ISecondExcelReport secondExcelReport, IAuthentication authentication,
            IMonthPlanningExcelReport monthPlanningExcelReport, IDepartmentLogic department)
        {
            _collector = collector;
            _firstExcelReport = excelReport;
            _planFactReport = planFactReport;
            _thirdExcelReport = thirdExcelReport;
            _detailsLogic = detailsLogic;
            _hereWeGoAgain = hereWeGoAgain;
            _secondExcelReport = secondExcelReport;
            _authentication = authentication;
            _monthPlanningExcelReport = monthPlanningExcelReport;
            _departmentLogic = department;
        }
        /// <summary> Get excel with data from DB like for /api/employees</summary>
        /// <remarks>
        /// BranchId takes from [actima].[dbo].[AllTimetable] on allTimeTableId
        /// </remarks>
        /// <param name="dateTime" >     yyyy-mm-dd</param>
        /// <param name="allTimetableId"> AllTimeTableId from [actima].[dbo].[Department]</param>
        /// <param name="employeeId">  EmployeeId from [actima].[dbo].[Employee], if u put value response will contains
        /// only specific employee data</param>
        /// <param name="departmentId">DepartmentId from [actima].[dbo].[Department] </param>
        /// <param name="groupId">Specific group ID</param>
        /// <returns> excel with data from DB like for /api/employees</returns>
        /// <response code="200"> Returns excel with data from DB like for /api/employees</response>
        /// <response code="400"> branchId isn`t exist in [actima].[dbo].[Department]
        /// or department entity from [actima].[dbo].[Department]  with branchId isn`t branch
        /// or AllTimeTable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]
        /// or there is no sub departments for branch row from  [actima].[dbo].[Department]
        /// </response>
        /// <response code="422">AllTimetable  instance has different date from dateTime</response>
        [HttpGet("day")]
        public async Task<IActionResult> GenerateExcelForEmployees([Required, FromQuery]DateTime dateTime,
            [Required, FromQuery]int allTimetableId, [FromQuery]int departmentId = default,
            [FromQuery]int groupId = -1, [FromQuery]int employeeId = default)
        {
            BranchJsonData branchData;
            if (employeeId == default)
            {
                try
                {
                    branchData =
                        await _collector.GetEmployeesData(dateTime, allTimetableId, User.Claims.ToList(),
                            departmentId: departmentId, groupId: groupId);
                }
                catch (BadRequestException e)
                {
                    return BadRequest(e.Message);
                }
                catch (UnprocessableEntityException e)
                {
                    return UnprocessableEntity(e.Message);
                }
            }
            else
            {
                try
                {
                    branchData = await _collector.GetEmployeesDataBySpecificEmployee(dateTime, employeeId,
                        allTimetableId, User.Claims.ToList());
                }
                catch (BadRequestException e)
                {
                    return BadRequest(e.Message);
                }
            }
            var memoryStream = new MemoryStream();
            var package = new ExcelPackage(memoryStream);
            _firstExcelReport.CreateExcel(branchData, package);
            var allTImetableData = branchData.AllTimetables;
            var fileName = _firstExcelReport.GetAlltimetableName(allTImetableData.Name, allTImetableData.TypeId.Value,
                branchData.Date) + ".xlsx";
            return File(memoryStream, FileType, fileName);
        }

        [HttpGet("planfact")]


        public async Task<IActionResult> GeneratePlanFactReport([Required, FromQuery] string departmentCode,
            [Required, FromQuery]int group, [Required, FromQuery] int idEmployee, [Required, FromQuery] int month,
            [Required, FromQuery]int year, [Required, FromQuery]int branchId)
        {
            try
            {

                var monthDate = new DateTime(year, month, 1);
                var user = await _authentication.GetUserById(int.Parse(HttpContext.User.Claims
                    .FirstOrDefault(claim => claim.Type == "Id")?.Value));
                var userName = user.UserShortName;
                var userId = user.UserId;
                var planFactorData =
                    await _planFactReport.GetReport(departmentCode, group, idEmployee, month, year.ToString(),
                        branchId, userId);
                var memoryStream = new MemoryStream();
                var package = new ExcelPackage(memoryStream);
                if (!planFactorData.Any()) return NoContent();
                _thirdExcelReport.GenerateExcel(planFactorData, monthDate, package, userName);
                var fileName = _thirdExcelReport.BaseGraphicName(monthDate) + ".xlsx";
                return File(memoryStream, FileType, fileName);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("planfact/Unwrap")]
        public async Task<IActionResult> GeneratePlanFactWithUnwrap([Required, FromQuery] int branchId,
            [Required, FromQuery] int year, [Required, FromQuery] int month, [Required, FromQuery] int employeeId)
        {
            try
            {
                var monthDate = new DateTime(year, month, 1);
                var user = await _authentication.GetUserById(int.Parse(HttpContext.User.Claims
                    .FirstOrDefault(claim => claim.Type == "Id")?.Value));
                var userName = user.UserShortName;
                var userId = user.UserId;
                var employee = await _detailsLogic.GetEmployee(employeeId);
                if (employee == null)
                    throw new NoContentException();
                var planFactorData =
                    await _planFactReport.GetReport(0.ToString(), 0, employeeId, month,
                        year.ToString(), branchId, userId);
                if (!planFactorData.Any())
                    throw new NoContentException();
                var memoryStream = new MemoryStream();
                var package = new ExcelPackage(memoryStream);
                var empName = _thirdExcelReport.GenerateExcelForSingleEmpWitUnwrap(planFactorData, monthDate, package,
                    userName, employee.AccountNumber.ToString(CultureInfo.GetCultureInfo("uk-UA")));
                var fileName = _thirdExcelReport.UnwrapExcelName(monthDate, empName) + ".xlsx";
                return File(memoryStream, FileType, fileName);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (NoContentException)
            {
                return NoContent();
            }
        }

        [HttpGet("permouth")]
        public async Task<IActionResult> GeneratePerMouthGraphic([Required, FromQuery] int allTimetableId,
            [Required, FromQuery]int groupId, [Required, FromQuery] int month, [Required, FromQuery] int year,
            [Required, FromQuery] int branchId, [Required, FromQuery] string departmentCode, [Required, FromQuery] int empId)
        {
            try
            {
                //Oh shit, here we go again
                var appearanceController = new EmployeeAppearanceController(_hereWeGoAgain, _authentication);
                var monthDate = new DateTime(year, month, 1);
                var data = await appearanceController.GetEmployeeCollector(branchId, groupId, month, year,
                    allTimetableId, departmentCode, empId, HttpContext.User.Claims.ToList());
                if (data == null)
                    return NoContent();
                var memoryStream = new MemoryStream();
                var package = new ExcelPackage(memoryStream);
                var filterDescription = await _secondExcelReport.GetFiltersDescription(departmentCode, groupId);
                _secondExcelReport.GenerateExcelPerMouth(package, data, monthDate, filterDescription);
                var fileName = _secondExcelReport.Header(monthDate) + ".xlsx";
                return File(memoryStream, FileType, fileName);
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        // (-_-)
        /// <summary>Generate excel file:"Month planning" with employees data from [actima].[dbo]</summary>
        /// <remarks>
        /// BranchId takes from [actima].[dbo].[AllTimetable] on allTimeTableId
        /// </remarks>
        /// <param name="year" >Year<param>
        /// /// <param name="month" >Month<param>
        /// <param name="allTimetableId"> AllTimeTableId from [actima].[dbo].[Department]</param>
        /// <param name="employeeId">  EmployeeId from [actima].[dbo].[Employee], if u put value response will contains
        /// only specific employee data</param>
        /// <param name="departmentCode">department code from [actima].[dbo].[Department](enter 0 if u want date from all departments)</param>
        /// <param name="groupId">Specific group ID (enter -1 if u want date from all departments)</param>
        /// <param name="branchId">Enter branchId</param>
        /// <returns> excel with data from DB like for /api/employees</returns>
        /// <response code="200"> Returns genererated excel file</response>
        /// <response code="400"> branchId isn`t exist in [actima].[dbo].[Department]
        /// or department entity from [actima].[dbo].[Department]  with branchId isn`t branch
        /// or AllTimeTable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]
        /// or there is no sub departments for branch row from  [actima].[dbo].[Department]
        /// </response>
        /// <response code="422">AllTimetable instance has different date from dateTime</response>
        [HttpGet("monthplan")]
        public async Task<IActionResult> GenerateMonthPlan([Required, FromQuery] int allTimetableId,
            [Required, FromQuery]int groupId, [Required, FromQuery] int month, [Required, FromQuery] int year,
            [Required, FromQuery] int branchId, [Required, FromQuery] string departmentCode, [Required, FromQuery] int empId)
        {
            var monthDate = new DateTime(year, month, 1);
            Branch4MonthJsonData branchData;
            int departmentId = 0;   
            try
            {
                var department = await _departmentLogic.GetDepartmentByCode(departmentCode);

                if (departmentCode == "0" )
                {
                    departmentId = department != null ? 0 : branchId;
                }
                if (departmentCode != "0")
                    if (department != null)
                        departmentId = department.DepartmentId;

                branchData =
                        await _collector.GetEmployeesData4Month(monthDate, User.Claims.ToList(), allTimetableId,
                            groupId: groupId, departmentId: departmentId);

            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (UnprocessableEntityException e)
            {
                return UnprocessableEntity(e.Message);
            }
            try
            {
                var memoryStream = new MemoryStream();
                var package = new ExcelPackage(memoryStream);
                await _monthPlanningExcelReport.GenerateMonthPlanExcel(package, branchData, monthDate, allTimetableId, groupId, departmentId);
                var fileName = _secondExcelReport.Header(monthDate) + ".xlsx";
                return File(memoryStream, FileType, fileName);

            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }



    }
}