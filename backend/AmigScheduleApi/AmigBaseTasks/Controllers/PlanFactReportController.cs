﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using AmigScheduleApi.Models.PlanFactReport;
using ActimaDbEntities.Entities;
using AmigScheduleApi.Models.HelpClasses;
using Microsoft.AspNetCore.Authorization;
using AmigScheduleApi.Services.IServices;
using System.Net.Http;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Filters;
using Microsoft.AspNetCore.Cors;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AmigScheduleApi.Controllers
{


    [Route("/api/[controller]")]
    [ApiController]
    [EnableCors("AllowSpecificOrigins")]
    public class PlanFactReportController : Controller
    {
        private readonly IPlanFactReportLogic _report;
        private readonly IAuthentication _authentication;

        public PlanFactReportController(IPlanFactReportLogic report, IAuthentication authentication)
        {
            _report = report;
            _authentication = authentication;
        }


        //https://localhost:5001/api/PlanFactReport?codeDepartment=0&Group=0&IdEmployee=75&Month=3&Year=2019&Branch=3
        //http://13.80.102.45:5001/api/PlanFactReport?codeDepartment=0&Group=1&IdEmployee=0&Month=3&Year=2019&branchId=3
        // код подразделения, id группы, id сотрудника, месяц, год, филиал
        [HttpGet]
        //[Authorize]
        //[TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        // codeDepartment==0 - выбраны все подразделения
        public async Task<IActionResult> getReport([RequiredFromQuery]string codeDepartment, [RequiredFromQuery]int Group, [RequiredFromQuery] int IdEmployee, [RequiredFromQuery] int Month, [RequiredFromQuery]string Year, [RequiredFromQuery]int branchId)
        {          
            try
            {
                int userId = -1;
                if (User.Claims.Count() != 0)
                {
                    userId = int.Parse(HttpContext.User.Claims.FirstOrDefault(claim => claim.Type == "Id")?.Value);
                    var user = await _authentication.GetUserById(userId);
                   
                }
                return Ok(await _report.GetReport(codeDepartment, Group, IdEmployee, Month, Year, branchId, userId));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
