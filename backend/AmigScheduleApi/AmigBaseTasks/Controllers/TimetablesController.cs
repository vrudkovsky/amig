using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.Exceptions;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Filters;
using AmigScheduleApi.Models.HelpClasses;
using AmigScheduleApi.Models.TimetablesReceiveJsonObject;
using Microsoft.AspNetCore.Mvc;

namespace AmigScheduleApi.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class AllTimetableController : Controller
    {
        private readonly IEmployeeDetailsLogic _employeeLogic;
        private readonly ITimeTablesLogic _timeTables;
        private readonly IDepartmentLogic _departmentLogic;
        private readonly IAuthentication _authentication;
        public AllTimetableController(IEmployeeDetailsLogic employeeLogic, ITimeTablesLogic timeTables,
            IDepartmentLogic departmentLogic, IAuthentication authentication)
        {
            _employeeLogic = employeeLogic;
            _timeTables = timeTables;
            _departmentLogic = departmentLogic;
            _authentication = authentication;
        }


        /// <summary> Update exist AlltimeTable with typeId</summary>
        /// <remarks>
        /// Sample request:
        /// PUT /api/alltimetable       
        /// </remarks>
        /// <returns>A newly updated AllTimetables</returns>
        /// <response code="200">Returns the newly updated item or empty object if AllTimetable entity already
        /// have given typeId</response>
        /// <response code="400">AllTimetable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]
        /// </response>
        /// <response code="422">AllTimetable with id like {allTimetableId} is main/main overtime schedule</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(422)]
        [HttpPost("changeType")]
        [TypeFilter(typeof(UserFullBranchRightsValidationAuthorizeFilter))]
        public async Task<IActionResult> UpdateExistAllTImeTable([FromBody, Required] AllTImeTablesUpdateRequestBody body)
        {
            try
            {
                await _timeTables.UpdateAllTimeTableWithReplaceForMainTypeId(body.AllTimetableId);
            }
            catch (UnprocessableEntityException e)
            {
                return UnprocessableEntity(e.Message);
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        /// <summary> Create new row in [amig].[dbo].[AllTimetable] </summary>
        /// <remarks>
        /// Sample request:
        /// Post /api/alltimetable       
        /// </remarks>
        /// <param name="body" > Request body</param>        
        /// <returns>A newly created AllTimeTables</returns>
        /// <response code="200">Returns the newly created</response>
        /// <response code="400">branchId isn`t exist in [actima].[dbo].[Department]  OR Some json field has empty data
        /// </response>
        /// <response code="422">branchId Isn`t belongs to branch record in [amig].[dbo].[Department]</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(422)]
        [ProducesResponseType(400)]
        [TypeFilter(typeof(UserFullBranchRightsValidationAuthorizeFilter))]
        [HttpPost]
        public async Task<IActionResult> CreateAllTimeTableForMonth([FromBody, Required] CreateAllTimetablesBody body)
        {
            if (body.Name == default || body.BranchId == default || body.MonthDate == default ||
                body.TableTypeId == default)
                return BadRequest("Some json field has empty data");
            //we can only create not basic graphics with typeId like 2 and 4
            if (body.TableTypeId != 2 && body.TableTypeId != 4)
                return BadRequest($"tableTypeId :{body.TableTypeId} was not recognized as version schedule");

            var monthDateCorrect = new DateTime(body.MonthDate.Year, body.MonthDate.Month, 1);
            var branch = await _departmentLogic.GetDepartment(body.BranchId);
            if (branch == null)
                return BadRequest($"There isn`t department with id like {body.BranchId}");
            if (!_departmentLogic.DepartmentIsBranch(branch))
                return UnprocessableEntity($"department with id like {body.BranchId} isn`t branch");
            var newAllTImeTables = await _timeTables.CreateNewAllTimetables(branch.DepartmentId, monthDateCorrect,
                body.EditDate ?? DateTime.Now, body.Name, body.TableTypeId);

            return Ok(new
            {
                Id = newAllTImeTables.TimetableId,
                Date = newAllTImeTables.TimetablesDate
            });
        }
        /// <summary> Delete version row in [amig].[dbo].[AllTimetable] </summary>
        /// <remarks>
        /// Sample request:
        /// Delete /api/alltimetable       
        /// </remarks>
        /// <returns>Deleted [amig].[dbo].[AllTimetable] instance Id</returns>
        /// <response code="200">Deleted [amig].[dbo].[AllTimetable] instance Id</response>
        /// <response code="400">AllTimeTable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable] or
        /// If AllTimetable record with same id has main type (1,3)</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpPost("delete")]
        [TypeFilter(typeof(UserFullBranchRightsValidationAuthorizeFilter))]
        public async Task<IActionResult> DeleteAllTimeTableById([Required, FromBody] AlltimetablesDeleteBody body)
        {
            var allTimeTable = await _timeTables.GetAllTimeTables(body.AllTimetableId);
            if (allTimeTable == null)
            {
                return BadRequest($"AllTimeTables wit hid like {body.AllTimetableId} isn`t exist");
            }
            //1 - главный гибкий график 3 - главный график сверхурочных 
            if (allTimeTable.TypeId == 1 || allTimeTable.TimetableId == 3)
            {
                return BadRequest($"AllTimeTable with id like : {body.AllTimetableId} is main schedule " +
                                       $"or main overtime schedule");
            }

            await _timeTables.RemoveAllTimeTables(allTimeTable.TimetableId);
            return Ok(body.AllTimetableId);
        }
        /// <summary> Create new rows in [amig].[dbo].[Timetable] </summary>
        /// <remarks>
        /// Sample request:
        /// Post /api/alltimetable/timetables
        /// </remarks>
        /// <param name="timetablesData"> Array of all changes in shifts and base information
        /// to find allTimeTables and to make some changes to DB</param>        
        /// <returns>EmployeeIds array for which changes were made</returns>
        /// <response code="200">EmployeeIds array for which changes were made</response>
        /// <response code="400">AllTimeTable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]
        /// or json haven`t employees objects  or shits in employee object is empty</response>
        /// <response code="422">Alltimetable instanse has typeId different from (2,4)
        ///or some employee have un flexible schedule on the day from json or
        /// ShiftStart/End date in Shifts isn`t equal to date from root or
        /// Shift start after end </response>
        [ProducesResponseType(200)]
        [ProducesResponseType(422)]
        [ProducesResponseType(400)]
        [HttpPost("timetable-add")]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        //get date yyyy-mm-dd 00:00:00.000 -yyyy-mm-dd 23:59:59.000
        public async Task<IActionResult> AddEmployeesTimetable ([FromBody, Required] TimetablesData timetablesData)
        {
            //TODO Add check with departmentId Name
            var allTimeTables = await _timeTables.GetAllTimeTables(timetablesData.AllTimetableId);
            var employeeIds = timetablesData.Employees
                .Select(emp => emp.EmployeeId).ToList();

            if (allTimeTables == null)
                return BadRequest($" AllTimeTable object with id like : {timetablesData.AllTimetableId} was not found");
            if (allTimeTables.TypeId != 2 && allTimeTables.TypeId != 4)
                return UnprocessableEntity("You cannot change timetable with typeId different from typeId = 2 or 4 ");
            if (!timetablesData.Employees.Any())
                return BadRequest("There is no objects in \"employees\":");

            var monthDate = allTimeTables.TimetablesDate;
            foreach (var employeeData in timetablesData.Employees)
            {
                foreach (var shift in employeeData.Shifts.Where(shift => shift.ShiftEnd.HasValue &&
                                                         shift.ShiftStart.HasValue))
                {
                    var employees = await _employeeLogic.GetFlexibleEmployeesByIdsCollection(employeeIds, allTimeTables.BranchId,
                     shift.ShiftStart.Value.Date);

                    var timeTables =
                        await _timeTables.GetTimeTablesForAllTimeTables(shift.ShiftStart.Value.Date, allTimeTables.TimetableId, employeeIds);

                    var employee = employees.SingleOrDefault(details => details.EmployeeId == employeeData.EmployeeId);

                    var timeTable = timeTables.Where(timetable => timetable.EmployeeId == employeeData.EmployeeId).ToList();
                    // интервал с null-ами приводит к удалению всех интервалов за день               
                    //сначала мы удаляем все данные по пользователю а потом их добавляем
                    //если хоть 1 из интервалов не подходит, тогда весь запрос отменяется и данные не сохраняются

                    if (timeTable.Any())
                        _timeTables.RemoveTimetable(timeTable.Select(timetable => timetable.RowId));
                    if (employeeData.Shifts == null || !employeeData.Shifts.Any())
                        continue;

                    if ((shift.ShiftStart.Value.Hour != 0 && shift.ShiftEnd.Value.Hour != 0))
                    {
                        if (shift.ShiftStart >= shift.ShiftEnd)
                            return UnprocessableEntity("Shift start after end or at the same time");
                    }
                    var requiredDepartmentsIds = employees
                        .Where(details => details.DepartmentId.HasValue && details.DepartmentId != 0)
                        .Select(details => details.DepartmentId.Value)
                        .Distinct()
                        .ToList();
                    var availableDepartmentsIds = _authentication.GetDepartmentsListFromClaims(User.Claims.ToList()).ToList();
                    if (requiredDepartmentsIds.Intersect(availableDepartmentsIds).Count() != requiredDepartmentsIds.Count)
                    {
                        HttpContext.Response.Headers.Add("XXX-Metadata", "Request forbidden due to user can't change schedule " +
                                                            "for employees from department, to that he hasn't rights to access");
                        return Forbid();
                    }

                    if (employees.Count != employeeIds.Count)
                    {
                        var employeesIdsFromDb = employees.Select(details => details.EmployeeId);
                        return UnprocessableEntity($"Employees with EmployeeId such as :" +
                                               $"[{employeeIds.Except(employeesIdsFromDb).Select(emp => $"{emp}, ")}] " +
                                               $"has unFlexible schedule or doesnt have ShiftRegimes or EmployeeDetails at all" +
                                               $"or some EmployeeIds repeat");
                    }

                    var correctTimeTable = new Timetable()
                    {
                        Date = shift.ShiftStart.Value.Date,
                        ShiftStart = shift.ShiftStart.Value,
                        ShiftEnd = shift.ShiftEnd.Value,
                        EmployeeId = employee.EmployeeId,
                        TimetableId = allTimeTables.TimetableId
                    };
                    _timeTables.AddTimeTable(correctTimeTable);
                }
            }
            allTimeTables.EditDate = timetablesData.EditDate ?? DateTime.Now;
            _timeTables.UpdateAllTimeTable(allTimeTables);
            await _timeTables.SaveChangesAsync();
            return Ok(employeeIds);
        }




        [ProducesResponseType(200)]
        [ProducesResponseType(422)]
        [ProducesResponseType(400)]
        [HttpPost("timetables")]
        [TypeFilter(typeof(CookieDataValidationAuthorizeFilter))]
        //get date yyyy-mm-dd 00:00:00.000 -yyyy-mm-dd 23:59:59.000
        public async Task<IActionResult> PostEmployeesData([FromBody, Required] TimetablesData timetablesData)
        {
            //TODO Add check with departmentId Name

            var date = new DateTime(timetablesData.Date.Year, timetablesData.Date.Month, timetablesData.Date.Day);
            var allTimeTables = await _timeTables.GetAllTimeTables(timetablesData.AllTimetableId);
            if (allTimeTables == null)
                return BadRequest($" AllTimeTable object with id like : {timetablesData.AllTimetableId} was not found");
            if (allTimeTables.TypeId != 2 && allTimeTables.TypeId != 4)
                return UnprocessableEntity("You cannot change timetable with typeId different from typeId = 2 or 4 ");
            if (!timetablesData.Employees.Any())
                return BadRequest("There is no objects in \"employees\":");
            var monthDate = allTimeTables.TimetablesDate;
            //оставляем пустые записи, ведь так мы просто удалим старые,
            //либо ничего делать не будем
            var employeeIds = timetablesData.Employees
                .Select(emp => emp.EmployeeId).ToList();
            var employees = await _employeeLogic.GetFlexibleEmployeesByIdsCollection(employeeIds, allTimeTables.BranchId,
                date);
            var timeTables =
                await _timeTables.GetTimeTablesForAllTimeTables(date, allTimeTables.TimetableId, employeeIds);
            var requiredDepartmentsIds = employees
                .Where(details => details.DepartmentId.HasValue && details.DepartmentId != 0)
                .Select(details => details.DepartmentId.Value)
                .Distinct()
                .ToList();
            var availableDepartmentsIds = _authentication.GetDepartmentsListFromClaims(User.Claims.ToList()).ToList();
            if (requiredDepartmentsIds.Intersect(availableDepartmentsIds).Count() != requiredDepartmentsIds.Count)
            {
                HttpContext.Response.Headers.Add("XXX-Metadata", "Request forbidden due to user can't change schedule " +
                                                    "for employees from department, to that he hasn't rights to access");
                return Forbid();
            }

            if (employees.Count != employeeIds.Count)
            {
                var employeesIdsFromDb = employees.Select(details => details.EmployeeId);
                return UnprocessableEntity($"Employees with EmployeeId such as :" +
                                       $"[{employeeIds.Except(employeesIdsFromDb).Select(emp => $"{emp}, ")}] " +
                                       $"has unFlexible schedule or doesnt have ShiftRegimes or EmployeeDetails at all" +
                                       $"or some EmployeeIds repeat");
            }
            foreach (var employeeData in timetablesData.Employees)
            {
                var employee = employees.SingleOrDefault(details => details.EmployeeId == employeeData.EmployeeId);
                var timeTable = timeTables.Where(timetable => timetable.EmployeeId == employeeData.EmployeeId).ToList();
                // интервал с налами приводит к удалению всех интервалов за день               
                //сначала мы удаляем все данные по пользователю а потом их добавляем
                //если хоть 1 из интервалов не подходит, тогда весь запрос отменяется и данные не сохраняются

                if (timeTable.Any())
                    _timeTables.RemoveTimetable(timeTable.Select(timetable => timetable.RowId));
                if (employeeData.Shifts == null || !employeeData.Shifts.Any())
                    continue;
                foreach (var shift in employeeData.Shifts.Where(shift => shift.ShiftEnd.HasValue &&
                                                                         shift.ShiftStart.HasValue))
                {
                    if (shift.ShiftStart.Value.Date != date ||
                        shift.ShiftEnd.Value.Date != date)
                    {
                        return UnprocessableEntity("ShiftStart/End date in Shifts isn`t equal to date from root");
                    }
                    if (shift.ShiftStart >= shift.ShiftEnd)
                        return UnprocessableEntity("Shift start after end or at the same time");

                    var correctTimeTable = new Timetable()
                    {
                        Date = date,
                        ShiftStart = shift.ShiftStart.Value,
                        ShiftEnd = shift.ShiftEnd.Value,
                        EmployeeId = employee.EmployeeId,
                        TimetableId = allTimeTables.TimetableId
                    };
                    _timeTables.AddTimeTable(correctTimeTable);
                }
            }
            allTimeTables.EditDate = timetablesData.EditDate ?? DateTime.Now;
            _timeTables.UpdateAllTimeTable(allTimeTables);
            await _timeTables.SaveChangesAsync();
            return Ok(employeeIds);
        }

        /// <summary>
        /// Copy existing AllTimeTable instance in [amig].[dbo].[AllTimetable] 
        /// </summary>
        /// Sample request:
        /// Post /api/alltimetable/copy
        /// <remarks>
        /// </remarks>
        /// <param name="body">Id row from table [amig].[dbo].[AllTimetable]
        /// Optional name for new alltimetables</param>
        /// <returns> New copied [amig].[dbo].[AllTimetable]  instance </returns>                                        
        /// <response code="200">New copied [amig].[dbo].[AllTimetable]  instance </response>
        /// <response code="400">
        /// AllTimeTable row with Id from params isn`t exist in [actima].[dbo].[AllTimetable]
        /// </response>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpPost("copy")]
        [TypeFilter(typeof(UserFullBranchRightsValidationAuthorizeFilter))]
        public async Task<IActionResult> CopyAllTimetablesWithSubTimetables(
            [FromBody, Required]AlltimetablesCopyRequestBody body)
        {
            if (body.AllTimetableId == default) return NotFound();
            try
            {
                var allTimeTable = await _timeTables.CopyAllTimeTables(body.AllTimetableId, body.Name);
                var allTimeTableFullData = new AllTimetableFullData
                {
                    Id = allTimeTable.TimetableId,
                    BranchId = allTimeTable.BranchId,
                    EditDate = allTimeTable.EditDate,
                    Name = allTimeTable.TimetablesName,
                    TimetableDate = allTimeTable.TimetablesDate,
                    TypeId = allTimeTable.TypeId
                };
                return Ok(allTimeTableFullData);
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}