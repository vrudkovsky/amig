using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.IServices;
using ActimaDbEntities.Services;
using AmigScheduleApi.Models.Auth;
using AmigScheduleApi.MQ.Listener;
using AmigScheduleApi.MQ.Sender;
using AmigScheduleApi.Services;
using AmigScheduleApi.Services.IServices;
using AmigScheduleApi.Services.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;


namespace AmigScheduleApi
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private IHostingEnvironment Environment { get; }

        public Startup(IConfiguration conf,IHostingEnvironment environment)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(environment.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true);
            Configuration = conf;
            Environment = environment;

        }
        private void MqServices(IServiceCollection services)
        {
            services.AddSingleton<IMqListener, MqListener>();
            services.AddSingleton<IMqSender, MqSender>();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services )
        {
            //MqServices(services); if MQ needed
            
            if(Environment.IsProduction())
            {

            }

            services.AddTransient<IEmployeeDetailsLogic, EmployeeDetailsLogic>();
            services.AddTransient<IDepartmentLogic, DepartmentLogic>();
            services.AddTransient<INormatives, TimeNormatives>();
            services.AddTransient<ITimeTablesLogic, TimeTablesLogic>();
            services.AddTransient<IEmployeeControllerCollector, EmployeeControllerCollector>();
            services.AddTransient<IFirstExcelReport, FirstExcelReport>();
            services.AddTransient<ISecondExcelReport, SecondExcelReport>();
            services.AddTransient<IThirdExcelReport,ThirdExcelReport>();
            services.AddTransient<IMonthPlanningExcelReport, MonthPlanningExcelReport>();
            services.AddTransient<IAuthentication, AuthenticationLogic>();
            services.AddTransient<IAuthParametersRead, AuthParametersRead>();
            
            services.AddTransient<IEmployeeResponseObjectLogic, EmployeeResponseObjectLogic>();
            services.AddTransient<IPlanFactReportLogic, PlanFactReportLogic>();

            services.AddHttpContextAccessor();

            services.AddHostedService<LogCompressorBackgroundService>();
            
            // for auth
            services.AddTransient<CheckChangesCookieAuthenticationEvents>();
            
            services.AddDbContext<ActimaContext>(builder =>
            {
                builder.UseSqlServer(Configuration["Data:ConnectionStrings:ActimaDb"],
        optionsBuilder => optionsBuilder.MigrationsAssembly("AmigScheduleApi"));
            });

            services.AddCors(options => options.AddPolicy("AllowSpecificOrigins",
            builder =>
            {
                builder.WithOrigins(Environment.IsDevelopment() ? "http://localhost:4200" : "http://13.80.102.45")
                    .WithMethods(HttpMethods.Get, HttpMethods.Post)
                    .WithExposedHeaders("content-disposition")
                    .AllowCredentials()
                    .AllowAnyHeader();
            }));


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.EventsType = typeof(CheckChangesCookieAuthenticationEvents);
                    options.Cookie.Name = ".Amig.Auth";
                    options.Cookie.SameSite = SameSiteMode.None;
                    options.Cookie.HttpOnly = false;
                });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios,
                //see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c=>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                }
            );
            app.UseCors("AllowSpecificOrigins");
            //Add HttpContext.User
            app.UseCookiePolicy(new CookiePolicyOptions()
            {
                MinimumSameSitePolicy = SameSiteMode.None
            });
            app.UseAuthentication();
            
            //app.UseRabbitListener(); TODO When we star to work with MQ uncomment
            //app.UseHttpsRedirection();
            //app.UseStaticFiles();
            app.UseMvc();
        }
    }
}