﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace AmigScheduleApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();     
            
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging((context, builder) => 
                    {
                        builder.ClearProviders();                    
                    })
                .UseSerilog(((context, configuration) =>
                    {
                        configuration.ReadFrom.Configuration(context.Configuration);
                    }))
                .UseStartup<Startup>()
                .Build();
    }
}
