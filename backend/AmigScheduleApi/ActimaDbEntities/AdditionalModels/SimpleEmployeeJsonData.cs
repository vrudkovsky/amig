
namespace AmigScheduleApi.Models
{
    public class SimpleEmployeeJsonData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Code { get; set; }
    }
}