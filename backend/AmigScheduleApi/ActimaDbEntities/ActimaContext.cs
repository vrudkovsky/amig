﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ActimaDbEntities.Entities
{
    public partial class ActimaContext : DbContext
    {
        public ActimaContext()
        {
        }

        public ActimaContext(DbContextOptions<ActimaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AbsenceCauses> AbsenceCauses { get; set; }
        public virtual DbSet<AbsenceSchedule> AbsenceSchedule { get; set; }
        public virtual DbSet<AllTimetables> AllTimetables { get; set; }
        public virtual DbSet<CardsEntity> CardsEntity { get; set; }
        public virtual DbSet<Departments> Departments { get; set; }
        public virtual DbSet<EmployeeGroups> EmployeeGroups { get; set; }
        public virtual DbSet<EmployeeGroups2Employee> EmployeeGroups2Employee { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<EmployeesDetails> EmployeesDetails { get; set; }
        public virtual DbSet<EmployeesDetailsAstu> EmployeesDetailsAstu { get; set; }
        public virtual DbSet<Holidays> Holidays { get; set; }
        public virtual DbSet<Professions> Professions { get; set; }
        public virtual DbSet<ShiftRegimeDetails> ShiftRegimeDetails { get; set; }
        public virtual DbSet<ShiftRegimes> ShiftRegimes { get; set; }
        public virtual DbSet<ShiftScheduleWithTemplate> ShiftScheduleWithTemplate { get; set; }
        public virtual DbSet<Shifts> Shifts { get; set; }
        public virtual DbSet<Timetable> Timetable { get; set; }
        public virtual DbSet<TimetablesTypes> TimetablesTypes { get; set; }
        public virtual DbSet<TmpT13countData> TmpT13countData { get; set; }
        public virtual DbSet<UserDepartmentRights> UserDepartmentRights { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<WorkTimeNormatives> WorkTimeNormatives { get; set; }
        public virtual DbSet<Normatives> Normatives { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<AbsenceCauses>(entity =>
            {
                entity.HasKey(e => e.AbsenceCauseId)
                    .HasName("PK__AbsenceC__267D9B78D44E6605");

                entity.Property(e => e.AbsenceCauseId)
                    .HasColumnName("AbsenceCauseID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AbsenceSchedule>(entity =>
            {
                entity.HasKey(e => new { e.EmployeeId, e.ScheduleDate })
                    .HasName("PK__AbsenceS__83295E7231016922");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ScheduleDate).HasColumnType("datetime");

                entity.Property(e => e.AbsenceCauseId).HasColumnName("AbsenceCauseID");

                entity.Property(e => e.AbsenceComment)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.AbsenceCause)
                    .WithMany(p => p.AbsenceSchedule)
                    .HasForeignKey(d => d.AbsenceCauseId)
                    .HasConstraintName("FK_AbsenceSchedule_AbsenceCau1");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.AbsenceSchedule)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AbsenceSchedule_Employees");
            });

            modelBuilder.Entity<AllTimetables>(entity =>
            {
                entity.HasKey(e => e.TimetableId)
                    .HasName("PK__AllTimet__68413F403866FAF8");

                entity.Property(e => e.TimetableId).HasColumnName("TimetableID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.EditDate).HasColumnType("datetime");

                entity.Property(e => e.TimetablesDate).HasColumnType("datetime");

                entity.Property(e => e.TimetablesName)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.AllTimetables)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK__AllTimeta__TypeI__20E1DCB5");
            });

            modelBuilder.Entity<CardsEntity>(entity =>
            {
                entity.HasKey(e => e.CardEntityId)
                    .HasName("PK__CardsEnt__16000B01E8AB8EDB");

                entity.Property(e => e.CardEntityId)
                    .HasColumnName("CardEntityID")
                    .ValueGeneratedNever();

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.OwnerId).HasColumnName("OwnerID");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Departments>(entity =>
            {
                entity.HasKey(e => e.DepartmentId)
                    .HasName("PK__Departme__B2079BCD0A067338");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RootId).HasColumnName("RootID");

                entity.HasOne(d => d.PatronNavigation)
                    .WithMany(p => p.Departments)
                    .HasForeignKey(d => d.Patron)
                    .HasConstraintName("FK_Departments_Employees");

                entity.HasOne(d => d.Root)
                    .WithMany(p => p.InverseRoot)
                    .HasForeignKey(d => d.RootId)
                    .HasConstraintName("FK_Departments_Departments");
            });

            modelBuilder.Entity<EmployeeGroups>(entity =>
            {
                entity.HasKey(e => e.EmployeeGroupId);

                entity.Property(e => e.EmployeeGroupId)
                    .HasColumnName("EmployeeGroupID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmployeeGroups2Employee>(entity =>
            {
                entity.HasKey(e => e.EmplGroup2EmplId);

                entity.Property(e => e.EmplGroup2EmplId)
                    .HasColumnName("EmplGroup2EmplID")
                    .ValueGeneratedNever();

                entity.Property(e => e.EmployeeGroupId).HasColumnName("EmployeeGroupID");

                entity.Property(e => e.RecordId).HasColumnName("RecordID");

                entity.HasOne(d => d.EmployeeGroup)
                    .WithMany(p => p.EmployeeGroups2Employee)
                    .HasForeignKey(d => d.EmployeeGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeeGroups2Employee_EmployeeGroups");

                entity.HasOne(d => d.Record)
                    .WithMany(p => p.EmployeeGroups2Employee)
                    .HasForeignKey(d => d.RecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeeGroups2Employee_EmployeesDetails");
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasKey(e => e.EmployeeId)
                    .HasName("PK__Employee__7AD04FF1A5C4E858");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountNumber).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Engaged).HasColumnType("datetime");

                entity.Property(e => e.Fired).HasColumnType("datetime");

                entity.Property(e => e.TaxNumber).HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<EmployeesDetails>(entity =>
            {
                entity.HasKey(e => e.RecordId)
                    .HasName("PK__Employee__FBDF78C9B45203A6");

                entity.Property(e => e.RecordId)
                    .HasColumnName("RecordID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AddInfo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProfessionId).HasColumnName("ProfessionID");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.EmployeesDetails)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK_EmployeesDetails_Departmen1");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.EmployeesDetails)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeesDetails_Employees");

                entity.HasOne(d => d.Profession)
                    .WithMany(p => p.EmployeesDetails)
                    .HasForeignKey(d => d.ProfessionId)
                    .HasConstraintName("FK_EmployeesDetails_Professio1");
            });

            modelBuilder.Entity<EmployeesDetailsAstu>(entity =>
            {
                entity.HasKey(e => e.RecordId)
                    .HasName("PK__Employee__FBDF78C933119CDF");

                entity.ToTable("EmployeesDetailsASTU");

                entity.Property(e => e.RecordId)
                    .HasColumnName("RecordID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CycleStart).HasColumnType("datetime");

                entity.Property(e => e.ShiftRegimeId).HasColumnName("ShiftRegimeID");

                entity.Property(e => e.TemplateId).HasColumnName("TemplateID");

                entity.HasOne(d => d.Record)
                    .WithOne(p => p.EmployeesDetailsAstu)
                    .HasForeignKey<EmployeesDetailsAstu>(d => d.RecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeesDetailsASTU_Emplo1");

                entity.HasOne(d => d.ShiftRegime)
                    .WithMany(p => p.EmployeesDetailsAstu)
                    .HasForeignKey(d => d.ShiftRegimeId)
                    .HasConstraintName("FK_EmployeesDetailsASTU_Shift1");
            });

            modelBuilder.Entity<Holidays>(entity =>
            {
                entity.HasKey(e => e.HolidayId)
                    .HasName("PK__Holidays__2D35D59A97CA9FC6");

                entity.Property(e => e.HolidayId)
                    .HasColumnName("HolidayID")
                    .ValueGeneratedNever();

                entity.Property(e => e.HolidayDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Professions>(entity =>
            {
                entity.HasKey(e => e.ProfessionId)
                    .HasName("PK__Professi__3F309E1F2697A72F");

                entity.Property(e => e.ProfessionId)
                    .HasColumnName("ProfessionID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProfessionGroupId).HasColumnName("ProfessionGroupID");
            });

            modelBuilder.Entity<ShiftRegimeDetails>(entity =>
            {
                entity.HasKey(e => e.ShiftRegimeId)
                    .HasName("PK__ShiftReg__74AA38665CDB7DB6");

                entity.Property(e => e.ShiftRegimeId)
                    .HasColumnName("ShiftRegimeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DefaultInOutInterval).HasColumnType("datetime");

                entity.Property(e => e.PairMarksInterval).HasColumnType("datetime");

                entity.Property(e => e.RouteId).HasColumnName("RouteID");

                entity.HasOne(d => d.ShiftRegime)
                    .WithOne(p => p.ShiftRegimeDetails)
                    .HasForeignKey<ShiftRegimeDetails>(d => d.ShiftRegimeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ShiftRegimeDetails_ShiftRe1");
            });

            modelBuilder.Entity<ShiftRegimes>(entity =>
            {
                entity.HasKey(e => e.ShiftRegimeId)
                    .HasName("PK__ShiftReg__74AA38668FCF68F2");

                entity.Property(e => e.ShiftRegimeId)
                    .HasColumnName("ShiftRegimeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.EveningEnd).HasColumnType("datetime");

                entity.Property(e => e.EveningStart).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NightEnd).HasColumnType("datetime");

                entity.Property(e => e.NightStart).HasColumnType("datetime");
            });

            modelBuilder.Entity<ShiftScheduleWithTemplate>(entity =>
            {
                entity.HasKey(e => e.RowId)
                    .HasName("PK__ShiftSch__FFEE745175FC3257");

                entity.Property(e => e.RowId)
                    .HasColumnName("RowID")
                    .ValueGeneratedNever();

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ScheduleDate).HasColumnType("datetime");

                entity.Property(e => e.ShiftId).HasColumnName("ShiftID");

                entity.Property(e => e.ShiftRegimeId).HasColumnName("ShiftRegimeID");
            });

            modelBuilder.Entity<Shifts>(entity =>
            {
                entity.HasKey(e => e.ShiftId)
                    .HasName("PK__Shifts__C0A838E186641E45");

                entity.Property(e => e.ShiftId)
                    .HasColumnName("ShiftID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccessRegimeId).HasColumnName("AccessRegimeID");

                entity.Property(e => e.BreakEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakStart).HasColumnType("datetime");

                entity.Property(e => e.ComingEnd).HasColumnType("datetime");

                entity.Property(e => e.ComingStart).HasColumnType("datetime");

                entity.Property(e => e.EarlyLeaving).HasColumnType("datetime");

                entity.Property(e => e.LateComing).HasColumnType("datetime");

                entity.Property(e => e.LeavingEnd).HasColumnType("datetime");

                entity.Property(e => e.LeavingStart).HasColumnType("datetime");

                entity.Property(e => e.ShiftEnd).HasColumnType("datetime");

                entity.Property(e => e.ShiftRegimeId).HasColumnName("ShiftRegimeID");

                entity.Property(e => e.ShiftStart).HasColumnType("datetime");

                entity.HasOne(d => d.ShiftRegime)
                    .WithMany(p => p.Shifts)
                    .HasForeignKey(d => d.ShiftRegimeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Shifts_ShiftRegimes");
            });

            modelBuilder.Entity<Timetable>(entity =>
            {
                entity.HasKey(e => e.RowId)
                    .HasName("PK__Timetabl__FFEE745114A744E7");

                entity.Property(e => e.RowId).HasColumnName("RowID");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ShiftEnd).HasColumnType("datetime");

                entity.Property(e => e.ShiftStart).HasColumnType("datetime");

                entity.Property(e => e.TimetableId).HasColumnName("TimetableID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Timetable)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__Timetable__Emplo__77AABCF8");

                entity.HasOne(d => d.TimetableNavigation)
                    .WithMany(p => p.Timetable)
                    .HasForeignKey(d => d.TimetableId)
                    .HasConstraintName("FK__Timetable__Timet__789EE131");
            });

            modelBuilder.Entity<TimetablesTypes>(entity =>
            {
                entity.HasKey(e => e.TypeId)
                    .HasName("PK__Timetabl__516F0395416E5E37");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.TypeName).HasMaxLength(30);
            });

            modelBuilder.Entity<TmpT13countData>(entity =>
            {
                entity.HasKey(e => e.RowId)
                    .HasName("PK__TmpT13Co__FFEE74514DA69C2E");

                entity.ToTable("TmpT13CountData");

                entity.Property(e => e.RowId)
                    .HasColumnName("RowID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId1c)
                    .HasColumnName("DepartmentID1C")
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeId1c)
                    .HasColumnName("EmployeeID1C")
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.OverTime).HasColumnType("datetime");

                entity.Property(e => e.RecordId).HasColumnName("RecordID");

                entity.Property(e => e.ShiftId).HasColumnName("ShiftID");

                entity.Property(e => e.ShiftRegimeId).HasColumnName("ShiftRegimeID");

                entity.Property(e => e.ShiftTime).HasColumnType("datetime");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.ZoneName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserDepartmentRights>(entity =>
            {
                entity.HasKey(e => e.UserDepartmentRightId)
                    .HasName("PK__UserDepa__53FC158F97C9B06D");

                entity.Property(e => e.UserDepartmentRightId)
                    .HasColumnName("UserDepartmentRightID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.UserDepartmentRights)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserDepartmentRights_Depar1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserDepartmentRights)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserDepartmentRights_Users");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK__Users__1788CCACF738B1C3");

                entity.HasIndex(e => e.UserLogin)
                    .HasName("Users UNIQUE (UserLogin)");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Certificate)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserGroupId).HasColumnName("UserGroupID");

                entity.Property(e => e.UserLogin)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.UserPassword)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.UserShortName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkTimeNormatives>(entity =>
            {
                entity.HasKey(e => e.WorkTimeNormativeId)
                    .HasName("PK__WorkTime__C43E028F040200FE");

                entity.Property(e => e.WorkTimeNormativeId)
                    .HasColumnName("WorkTimeNormativeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.RecordId).HasColumnName("RecordID");

                entity.HasOne(d => d.Record)
                    .WithMany(p => p.WorkTimeNormatives)
                    .HasForeignKey(d => d.RecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkTimeNormatives_WorkTimeNormatives");
            });

            modelBuilder.Entity<Normatives>(entity =>
            {
                entity.HasKey(e => new { e.ShiftRegimeID, e.NormYear, e.NormMonth });
            });
        }
    }
}
