﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class ShiftRegimes
    {
        public ShiftRegimes()
        {
            EmployeesDetailsAstu = new HashSet<EmployeesDetailsAstu>();
            Shifts = new List<Shifts>();
        }

        public int ShiftRegimeId { get; set; }
        public string Name { get; set; }
        public DateTime EveningStart { get; set; }
        public DateTime EveningEnd { get; set; }
        public int EveningMin { get; set; }
        public int EveningMax { get; set; }
        public DateTime NightStart { get; set; }
        public DateTime NightEnd { get; set; }
        public int NightMin { get; set; }
        public int NightMax { get; set; }
        public int Overtime { get; set; }
        public int Undertime { get; set; }
        public int ActiveRegime { get; set; }
        public int? MinOvertime { get; set; }
        public int? LegalUndertime { get; set; }
        public int? FlexibleSchedule { get; set; }

        public virtual ShiftRegimeDetails ShiftRegimeDetails { get; set; }
        public virtual ICollection<EmployeesDetailsAstu> EmployeesDetailsAstu { get; set; }
        public virtual List<Shifts> Shifts { get; set; }
    }
}
