﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class Holidays
    {
        public int HolidayId { get; set; }
        public string Name { get; set; }
        public DateTime HolidayDate { get; set; }
    }
}
