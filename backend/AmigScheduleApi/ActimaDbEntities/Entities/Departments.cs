﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class Departments
    {
        public Departments()
        {
            EmployeesDetails = new HashSet<EmployeesDetails>();
            InverseRoot = new HashSet<Departments>();
            UserDepartmentRights = new HashSet<UserDepartmentRights>();
        }

        public int DepartmentId { get; set; }
        public int? RootId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? Patron { get; set; }

        public virtual Employees PatronNavigation { get; set; }
        public virtual Departments Root { get; set; }
        public virtual ICollection<EmployeesDetails> EmployeesDetails { get; set; }
        public virtual ICollection<Departments> InverseRoot { get; set; }
        public virtual ICollection<UserDepartmentRights> UserDepartmentRights { get; set; }
    }
}
