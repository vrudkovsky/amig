﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class Users
    {
        public Users()
        {
            UserDepartmentRights = new HashSet<UserDepartmentRights>();
        }

        public int UserId { get; set; }
        public int? UserGroupId { get; set; }
        public string UserLogin { get; set; }
        public string UserPassword { get; set; }
        public int Override { get; set; }
        public int Active { get; set; }
        public string UserShortName { get; set; }
        public string Certificate { get; set; }
        public string Description { get; set; }

        public virtual ICollection<UserDepartmentRights> UserDepartmentRights { get; set; }
    }
}
