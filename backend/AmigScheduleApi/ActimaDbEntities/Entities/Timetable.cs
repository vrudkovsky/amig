﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class Timetable
    {
        public int RowId { get; set; }
        public int? TimetableId { get; set; }
        public DateTime Date { get; set; }
        public DateTime ShiftStart { get; set; }
        public DateTime ShiftEnd { get; set; }
        public int? EmployeeId { get; set; }

        public virtual Employees Employee { get; set; }
        public virtual AllTimetables TimetableNavigation { get; set; }
    }
}
