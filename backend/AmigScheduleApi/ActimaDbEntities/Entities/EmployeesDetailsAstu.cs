﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class EmployeesDetailsAstu
    {
        public int RecordId { get; set; }
        public int? ShiftRegimeId { get; set; }
        public int? AccessPriority { get; set; }
        public int? ExactTime { get; set; }
        public int? TemplateId { get; set; }
        public DateTime CycleStart { get; set; }
        public int? WorkTimeOnSched { get; set; }
        public int? Combiner { get; set; }
        public int? LegalUnderTime { get; set; }

        public virtual EmployeesDetails Record { get; set; }
        public virtual ShiftRegimes ShiftRegime { get; set; }
    }
}
