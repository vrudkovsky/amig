﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class ShiftScheduleWithTemplate
    {
        public int RowId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime ScheduleDate { get; set; }
        public int? ShiftId { get; set; }
        public int? ShiftRegimeId { get; set; }
    }
}
