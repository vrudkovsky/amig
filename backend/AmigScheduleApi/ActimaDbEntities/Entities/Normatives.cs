﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActimaDbEntities.Entities
{
    public class Normatives
    {
        public int ShiftRegimeID { get; set; }

        public int NormYear { get; set; }

        public int NormMonth { get; set; }

        public double NormHours { get; set; }

        public int NormAppearTimes { get; set; }

    }
}
