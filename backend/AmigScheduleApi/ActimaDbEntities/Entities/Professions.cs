﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class Professions
    {
        public Professions()
        {
            EmployeesDetails = new HashSet<EmployeesDetails>();
        }

        public int ProfessionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int ProfessionGroupId { get; set; }

        public virtual ICollection<EmployeesDetails> EmployeesDetails { get; set; }
    }
}
