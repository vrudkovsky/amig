﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class Shifts
    {
        public int ShiftId { get; set; }
        public int ShiftRegimeId { get; set; }
        public int ShiftNumber { get; set; }
        public DateTime ComingStart { get; set; }
        public DateTime ComingEnd { get; set; }
        public DateTime ShiftStart { get; set; }
        public DateTime ShiftEnd { get; set; }
        public DateTime LeavingStart { get; set; }
        public DateTime LeavingEnd { get; set; }
        public DateTime LateComing { get; set; }
        public DateTime EarlyLeaving { get; set; }
        public DateTime BreakStart { get; set; }
        public DateTime BreakEnd { get; set; }
        public int? AccessRegimeId { get; set; }

        public virtual ShiftRegimes ShiftRegime { get; set; }
    }
}
