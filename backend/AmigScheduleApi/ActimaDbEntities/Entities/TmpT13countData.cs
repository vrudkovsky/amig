﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class TmpT13countData
    {
        public int RowId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public int? ShiftId { get; set; }
        public DateTime? ShiftTime { get; set; }
        public DateTime? OverTime { get; set; }
        public int? NightTime { get; set; }
        public int? EveningTime { get; set; }
        public int? ShiftRegimeId { get; set; }
        public int? RecordId { get; set; }
        public int? VirtualMarkType { get; set; }
        public int? BeforeMdntTime { get; set; }
        public int? AfterMdntTime { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? StartDevice { get; set; }
        public int? EndDevice { get; set; }
        public string EmployeeId1c { get; set; }
        public string DepartmentId1c { get; set; }
        public string ZoneName { get; set; }
    }
}
