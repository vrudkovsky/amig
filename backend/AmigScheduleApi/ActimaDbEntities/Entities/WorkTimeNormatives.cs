﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class WorkTimeNormatives
    {
        public int WorkTimeNormativeId { get; set; }
        public int RecordId { get; set; }
        public DateTime Date { get; set; }
        public int NormDays { get; set; }
        public int NormHours { get; set; }

        public virtual EmployeesDetails Record { get; set; }
    }
}
