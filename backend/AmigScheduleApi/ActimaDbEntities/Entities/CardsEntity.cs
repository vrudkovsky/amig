﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class CardsEntity
    {
        public int CardEntityId { get; set; }
        public int OwnerId { get; set; }
        public int OwnerType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
