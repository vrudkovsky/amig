﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class TimetablesTypes
    {
        public TimetablesTypes()
        {
            AllTimetables = new HashSet<AllTimetables>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<AllTimetables> AllTimetables { get; set; }
    }
}
