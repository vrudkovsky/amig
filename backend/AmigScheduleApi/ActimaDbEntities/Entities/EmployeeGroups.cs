﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class EmployeeGroups
    {
        public EmployeeGroups()
        {
            EmployeeGroups2Employee = new HashSet<EmployeeGroups2Employee>();
        }

        public int EmployeeGroupId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public virtual ICollection<EmployeeGroups2Employee> EmployeeGroups2Employee { get; set; }
    }
}
