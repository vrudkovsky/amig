﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class EmployeeGroups2Employee
    {
        public int EmplGroup2EmplId { get; set; }
        public int EmployeeGroupId { get; set; }
        public int RecordId { get; set; }

        public virtual EmployeeGroups EmployeeGroup { get; set; }
        public virtual EmployeesDetails Record { get; set; }
    }
}
