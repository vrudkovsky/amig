﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class AbsenceCauses
    {
        public AbsenceCauses()
        {
            AbsenceSchedule = new HashSet<AbsenceSchedule>();
        }

        public int AbsenceCauseId { get; set; }
        public int AbsenceType { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Assume { get; set; }
        public double Coefficient { get; set; }
        public int? ShowInReport { get; set; }
        public int? CalcAppear { get; set; }

        public virtual ICollection<AbsenceSchedule> AbsenceSchedule { get; set; }
    }
}
