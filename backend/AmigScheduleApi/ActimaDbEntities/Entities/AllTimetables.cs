﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class AllTimetables
    {
        public AllTimetables()
        {
            Timetable = new List<Timetable>();
        }

        public int TimetableId { get; set; }
        public DateTime TimetablesDate { get; set; }
        public DateTime EditDate { get; set; }
        public int? TypeId { get; set; }
        public string TimetablesName { get; set; }
        public int BranchId { get; set; }

        public virtual TimetablesTypes Type { get; set; }
        public virtual List<Timetable> Timetable { get; set; }
    }
}
