﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class Employees
    {
        public Employees()
        {
            AbsenceSchedule = new List<AbsenceSchedule>();
            Departments = new HashSet<Departments>();
            EmployeesDetails = new HashSet<EmployeesDetails>();
            Timetable = new List<Timetable>();
        }

        public int EmployeeId { get; set; }
        public decimal AccountNumber { get; set; }
        public DateTime? Engaged { get; set; }
        public DateTime? Fired { get; set; }
        public decimal? TaxNumber { get; set; }

        public virtual List<AbsenceSchedule> AbsenceSchedule { get; set; }
        public virtual ICollection<Departments> Departments { get; set; }
        public virtual ICollection<EmployeesDetails> EmployeesDetails { get; set; }
        public virtual List<Timetable> Timetable { get; set; }
    }
}
