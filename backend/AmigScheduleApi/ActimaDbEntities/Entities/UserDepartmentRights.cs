﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class UserDepartmentRights
    {
        public int UserDepartmentRightId { get; set; }
        public int UserId { get; set; }
        public int DepartmentId { get; set; }
        public int Enabled { get; set; }

        public virtual Departments Department { get; set; }
        public virtual Users User { get;    set; }
    }
}
