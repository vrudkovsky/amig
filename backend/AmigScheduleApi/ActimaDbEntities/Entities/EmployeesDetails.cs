﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class EmployeesDetails
    {
        public EmployeesDetails()
        {
            EmployeeGroups2Employee = new HashSet<EmployeeGroups2Employee>();
            WorkTimeNormatives = new HashSet<WorkTimeNormatives>();
        }

        public int RecordId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int? DepartmentId { get; set; }
        public int? ProfessionId { get; set; }
        public int? ManualyCreated { get; set; }
        public string AddInfo { get; set; }

        public virtual Departments Department { get; set; }
        public virtual Employees Employee { get; set; }
        public virtual Professions Profession { get; set; }
        public virtual EmployeesDetailsAstu EmployeesDetailsAstu { get; set; }
        public virtual ICollection<EmployeeGroups2Employee> EmployeeGroups2Employee { get; set; }
        public virtual ICollection<WorkTimeNormatives> WorkTimeNormatives { get; set; }
    }
}
