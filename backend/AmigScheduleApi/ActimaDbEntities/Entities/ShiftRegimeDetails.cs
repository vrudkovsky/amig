﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class ShiftRegimeDetails
    {
        public int ShiftRegimeId { get; set; }
        public int ShortFriday { get; set; }
        public int ShortPreHoliday { get; set; }
        public DateTime? PairMarksInterval { get; set; }
        public int? DividePairByDay { get; set; }
        public int? WriteFactDate { get; set; }
        public DateTime? DefaultInOutInterval { get; set; }
        public int? RouteId { get; set; }

        public virtual ShiftRegimes ShiftRegime { get; set; }
    }
}
