﻿using System;
using System.Collections.Generic;

namespace ActimaDbEntities.Entities
{
    public partial class AbsenceSchedule
    {
        public int EmployeeId { get; set; }
        public DateTime ScheduleDate { get; set; }
        public int? AbsenceCauseId { get; set; }
        public string AbsenceComment { get; set; }
        public int? ManualyCreated { get; set; }

        public virtual AbsenceCauses AbsenceCause { get; set; }
        public virtual Employees Employee { get; set; }
    }
}
