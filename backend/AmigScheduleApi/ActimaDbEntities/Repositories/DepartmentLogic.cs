using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.IServices;
using Microsoft.EntityFrameworkCore;

namespace ActimaDbEntities.Services
{
    public sealed class DepartmentLogic:IDepartmentLogic
    {
        private readonly ActimaContext _context;
        private  int CompanyAsDepartmentId { get; } = 1;
        public DepartmentLogic(ActimaContext context)
        {
            _context = context;
        }

        public async Task<List<int>> GetBranchIds()
        {
            return await _context.Departments.AsNoTracking()
                .Where(departments => departments.Root.DepartmentId == CompanyAsDepartmentId)
                .Select(departments =>departments.DepartmentId )
                .ToListAsync();
        }

        public bool DepartmentIsBranch(Departments branch)
        {
            return branch.RootId.HasValue && branch.RootId.Value == CompanyAsDepartmentId;
        }

        public async Task<Departments> GetDepartment(int departmentId)
        {
            return await _context.Departments
                .AsNoTracking()
                .Include(departments =>departments.Root) 
                .Include(departments =>departments.InverseRoot)
                .FirstOrDefaultAsync(dep => dep.DepartmentId == departmentId);
        }
        public async Task<List<Departments>> GetSubDepartments(int branchId, int departmentId = 0)
        {
            //var department = await GetDepartment(departmentCode);
            if (departmentId != 0)
                return new List<Departments>
                {
                    await GetDepartment(departmentId)
                };
            //var departmentId = department.DepartmentId;
            return await _context.Departments
                .Where(departments => departments.Root.DepartmentId == branchId)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Departments> GetBranchWithSubDepartments(int branchId)
        {
            return await _context.Departments
                .Where(departments =>departments.DepartmentId == branchId && 
                                     departments.RootId == CompanyAsDepartmentId)
                .Include(departments =>departments.Root )
                .Include(departments =>departments.InverseRoot )
                    .ThenInclude(departments =>departments.InverseRoot)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        public async Task<Departments> GetCompanyAsDepartmentWithSubDepartments()
        {
            return await _context.Departments
                .Include(departments => departments.InverseRoot)
                .SingleOrDefaultAsync( departments =>departments.RootId == null );
        }

        public async Task<Departments> GetDepartmentByCode(string statusCode)
        {
            return await _context.Departments
                .SingleOrDefaultAsync(departments => departments.Code.Equals(statusCode));
        }
    }
}