using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.IServices;
using AmigScheduleApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ActimaDbEntities.Services
{
    public sealed class EmployeeDetailsLogic : IEmployeeDetailsLogic
    {
        private readonly ActimaContext _context;
        public EmployeeDetailsLogic(ActimaContext context)
        {
            _context = context;
        }


        public async Task<Dictionary<int, string>> GetDictionaryForGroupNamesWoTracking(int groupId = -1)
        {
            switch (groupId)
            {
                case -1:
                    {
                        var dictionary = await _context.EmployeeGroups
                            .AsNoTracking()
                            .ToDictionaryAsync((groups => groups.EmployeeGroupId),
                                groups => groups.Name);
                        dictionary.Add(0, null);
                        return dictionary;
                    }
                case 0:
                    return new Dictionary<int, string> { { 0, null } };
                default:
                    return await _context.EmployeeGroups
                        .AsNoTracking()
                        .Where(groups => groups.EmployeeGroupId == groupId)
                        .ToDictionaryAsync((groups => groups.EmployeeGroupId),
                            groups => groups.Name);
            }
        }

        private IQueryable<EmployeesDetails> GetRelationships4Employees(IQueryable<EmployeesDetails> queryable)
        {
            return queryable.Join(_context.CardsEntity.Where(card => card.OwnerType == 0),
                    emp => emp.EmployeeId,
                    card => card.OwnerId,
                    (employees, entity) => employees)
                .Include(emp => emp.Department)
                .Include(emp => emp.EmployeeGroups2Employee)
                .Include(emp => emp.Profession)
                .Include(emp => emp.Employee)
                //в общем, сначала мы загружаем все shiftRegime, потом при гибком графике,
                //мы читаем выходные и если они есть то выставляем граффик для выходных
                .Include(emp => emp.EmployeesDetailsAstu)
                .ThenInclude(astu => astu.ShiftRegime)
                .ThenInclude(regimes => regimes.ShiftRegimeDetails)
                .AsNoTracking()
                .AsQueryable();
        }
        private IQueryable<EmployeesDetails> GetAllEmployeesWithIncludeDataQuery(DateTime date)
        {

            var empDetails = _context.EmployeesDetails
                .Where(details => details.StartDate <= date && details.EndDate >= date);
            return GetRelationships4Employees(empDetails);
        }

        private async Task<EmployeesDetails> GetShiftsForUnFlexibleEmployee(EmployeesDetails employee, DateTime date)
        {
            if (employee.EmployeesDetailsAstu == null ||
                employee.EmployeesDetailsAstu.ShiftRegime.FlexibleSchedule == 1) return employee;
            var shiftForUnFlexibleEmployee = await _context.ShiftScheduleWithTemplate
                .AsNoTracking()
                .FirstOrDefaultAsync(template => template.ScheduleDate == date &&
                                                 template.EmployeeId == employee.EmployeeId);
            var shift = await _context.Shifts.FirstOrDefaultAsync(shifts =>
                shifts.ShiftId == shiftForUnFlexibleEmployee.ShiftId);
            employee.EmployeesDetailsAstu.ShiftRegime.Shifts.Add(shift);
            return employee;
        }

        private async Task<List<EmployeesDetails>> GetShiftsForUnFlexibleEmployees(List<EmployeesDetails> employees,
            DateTime date)
        {
            var unFlexibleEmployees = employees
                .Where(employee => employee.EmployeesDetailsAstu.ShiftRegime?.FlexibleSchedule == 0)
                .Select(employee => employee.EmployeeId)
                .ToList();
            if (!unFlexibleEmployees.Any()) return employees;
            var shiftForUnFlexibleEmployees = await _context.ShiftScheduleWithTemplate
                .AsNoTracking()
                .Where(template => template.ScheduleDate == date &&
                                  unFlexibleEmployees.Contains(template.EmployeeId))
                .Join(_context.Shifts.AsNoTracking(), template => template.ShiftId, shifts => shifts.ShiftId,
                    (template, shifts) => new Tuple<int, Shifts>(template.EmployeeId, shifts))
                .ToListAsync();
            foreach (var shift in shiftForUnFlexibleEmployees)
            {
                //они ссылаются на одну и ту же запись в ShiftRegimes, и если добавлять в эту коллекцию, тогда
                //записи будут накапливатся
                employees.FirstOrDefault(details => details.EmployeeId == shift.Item1 &&
                                                   !details.EmployeesDetailsAstu.ShiftRegime.Shifts.Any())?
                    .EmployeesDetailsAstu.ShiftRegime.Shifts.Add(shift.Item2);
            }

            return employees;
        }

        public async Task<List<EmployeesDetails>> GetEmployeesByDepartment4Month(int departmentId,
            DateTime monthDate)
        {
            var monthFirstDate = monthDate.AddDays(1 - monthDate.Day);
            var monthLastDate = monthDate.AddDays(-monthDate.Day).AddMonths(1);
            var empDetails = _context.EmployeesDetails
                .AsNoTracking()
                .Where(emp => emp.StartDate <= monthLastDate && emp.EndDate >= monthFirstDate)
                .AsQueryable();
            return await GetRelationships4Employees(empDetails).Where(emp => emp.DepartmentId == departmentId)
                .ToListAsync();

        }
        public async Task<List<EmployeesDetails>> GetEmployeesByDepartment(int departmendId,
            DateTime date)
        {
            var employees = await GetAllEmployeesWithIncludeDataQuery(date)
                    .Where(emp => emp.Department.DepartmentId == departmendId)
                    .ToListAsync();
            return await GetShiftsForUnFlexibleEmployees(employees, date);
        }

        public async Task<Employees> GetEmployee(int employeeId)
        {
            return await _context.Employees.FindAsync(employeeId);
        }

        public async Task<List<EmployeesDetails>> GetFlexibleEmployeesByIdsCollection(List<int> employeeIds,
            int? branchId, DateTime? date)
        {

            // normal department means it's following structure in Departmens talbe (like 115 and 112 does)
            var isNormalDepartmentsTableStructure = await Task.Run(() => _context.EmployeesDetails.Any(empDet => empDet.Department.Root.DepartmentId == branchId));
            if (isNormalDepartmentsTableStructure)
            {
                return await _context.EmployeesDetails
                    .Where(emp => employeeIds.Contains(emp.EmployeeId) &&
                                  emp.StartDate <= date &&
                                  emp.EndDate >= date &&
                                  emp.Department.Root.DepartmentId == branchId &&
                                  emp.EmployeesDetailsAstu.ShiftRegime.FlexibleSchedule == 1)
                    .Include(emp => emp.EmployeesDetailsAstu)
                    .ThenInclude(astu => astu.ShiftRegime)
                    .ToListAsync();
            }
            else
            {
                return await _context.EmployeesDetails
                    .Where(emp => employeeIds.Contains(emp.EmployeeId) &&
                                  emp.StartDate <= date &&
                                  emp.EndDate >= date &&
                                  emp.DepartmentId == branchId &&
                                  emp.EmployeesDetailsAstu.ShiftRegime.FlexibleSchedule == 1)
                    .Include(emp => emp.EmployeesDetailsAstu)
                    .ThenInclude(astu => astu.ShiftRegime)
                    .ToListAsync();
            }
        }

        public List<EmployeesDetails> GetEmployeeByGroup(int groupId, IEnumerable<EmployeesDetails> employees)
        {
            if (groupId == 0)
            {
                return employees.Where(emp => !emp.EmployeeGroups2Employee.Any())
                    .ToList();
            }
            return employees.Where(emp =>
                emp.EmployeeGroups2Employee.FirstOrDefault(empGroup => empGroup.EmployeeGroupId == groupId) != null)
                .ToList();
        }
        public async Task<EmployeesDetails> GetEmployeeDetails(DateTime date, int employeeId)
        {
            var emp = await GetAllEmployeesWithIncludeDataQuery(date)
                .Include(details => details.EmployeeGroups2Employee)
                .ThenInclude(employee => employee.EmployeeGroup)
                .AsNoTracking()
                .FirstOrDefaultAsync(details => details.EmployeeId == employeeId);
            return await GetShiftsForUnFlexibleEmployee(emp, date);
        }

        public async Task<List<SimpleEmployeeJsonData>> GetSimpleEmployeeInfo(List<int> subDepartmentsIds, DateTime date)
        {
            return await GetAllEmployeesWithIncludeDataQuery(date).Where(details => details.DepartmentId.HasValue &&
                                                                                    subDepartmentsIds.Contains(details
                                                                                        .DepartmentId.Value))
                .AsNoTracking()
                .Select(
                details => new SimpleEmployeeJsonData()
                {
                    Code = details.Employee.AccountNumber,
                    Id = details.EmployeeId,
                    Name =
                        $"{details.LastName} {details.FirstName.Substring(0, 1)}. " +
                        $"{details.MiddleName.Substring(0, 1)}."
                })
                .ToListAsync();
        }

        public async Task<string> GetEmployeeProffesionById(int id)
        {
            return await (from employee in _context.Employees
                          join empDet in _context.EmployeesDetails on employee.EmployeeId equals empDet.EmployeeId
                          join proffesions in _context.Professions on empDet.ProfessionId equals proffesions.ProfessionId
                          where employee.EmployeeId == id
                          select proffesions.Name).FirstOrDefaultAsync();
        }

        public async Task<List<int>> GetEmployeesIdWithShortDays()
        {
            return await (from employee in _context.Employees
                          join empDet in _context.EmployeesDetails on employee.EmployeeId equals empDet.EmployeeId
                          join empDetAstu in _context.EmployeesDetailsAstu on empDet.RecordId equals empDetAstu.RecordId
                          join shiftRegime in _context.ShiftRegimes on empDetAstu.ShiftRegimeId equals shiftRegime.ShiftRegimeId
                          join shiftRegimeDetails in _context.ShiftRegimeDetails on shiftRegime.ShiftRegimeId equals shiftRegimeDetails.ShiftRegimeId
                          where shiftRegimeDetails.ShortFriday == 1 || shiftRegimeDetails.ShortPreHoliday == 1
                          select employee.EmployeeId
                          ).ToListAsync();
        }

        public async Task<List<EmployeeShiftData>> GetIntervalsFromDateTime(int empid, DateTime dateTime)
        {
            var list = await ((from employee in _context.Employees
                               join shiftScheduleWithTemplate in _context.ShiftScheduleWithTemplate on employee.EmployeeId equals shiftScheduleWithTemplate.EmployeeId
                               join shifts in _context.Shifts on shiftScheduleWithTemplate.ShiftId equals shifts.ShiftId
                               where employee.EmployeeId == empid && shiftScheduleWithTemplate.ScheduleDate.Month == dateTime.Month
                               select new EmployeeShiftData
                               { ShiftStart = shifts.ShiftStart, ShiftEnd = shifts.ShiftEnd,  ShiftDate = shiftScheduleWithTemplate.ScheduleDate})
                               .AsNoTracking().ToListAsync());
            if (list.Count() == 0)
                 return null;
                return list;
        }


        public sealed class EmployeeShiftData
        {
            public DateTime? ShiftStart { get; set; }
            public DateTime? ShiftEnd { get; set; }
            public DateTime? ShiftDate { get; set; }
        }

        public async Task<Tuple<int,int,string>> GetDepartIdAndGroupIdFromEmployee(int recordId)
        {
            var departId = await (from employeesDetails in _context.EmployeesDetails
                                  join department in _context.Departments on employeesDetails.DepartmentId equals department.DepartmentId
                                  where employeesDetails.RecordId == recordId
                                  select department.DepartmentId).FirstOrDefaultAsync();

            var groupName = await (
                                    from employeesDetails in _context.EmployeesDetails
                                    join emploees2Groups in _context.EmployeeGroups2Employee on employeesDetails.RecordId equals emploees2Groups.RecordId
                                    join employeeGroup in _context.EmployeeGroups on emploees2Groups.EmployeeGroupId equals employeeGroup.EmployeeGroupId
                                    where employeesDetails.RecordId == recordId
                                    select employeeGroup.Name).FirstOrDefaultAsync();

            if (groupName == null)
                groupName = "Без групи";

            int groupId = await (
                from employeesDetails in _context.EmployeesDetails
                join emploees2Groups in _context.EmployeeGroups2Employee on employeesDetails.RecordId equals emploees2Groups.RecordId
                join employeeGroup in _context.EmployeeGroups on emploees2Groups.EmployeeGroupId equals employeeGroup.EmployeeGroupId
                where employeesDetails.RecordId == recordId
                select employeeGroup.EmployeeGroupId).FirstOrDefaultAsync();



            return new Tuple<int, int, string> (item1: departId, item2:groupId, item3: groupName );




        }

        public async Task<List<int>> GetRecordIdFromEmployeeId(int empId)
        {
            return await (from employeeDet in _context.EmployeesDetails
                          where employeeDet.EmployeeId == empId
                          select employeeDet.RecordId).ToListAsync();
        }

        public async Task<List<Tuple<int,int>>> GetEmployeeAbsenceSchedule(int month)
        {
            return await _context.AbsenceSchedule.Where(schedule => schedule.ScheduleDate.Month == month)
                .Select(c => new Tuple<int, int>(c.EmployeeId, c.ScheduleDate.Day)).ToListAsync();
        }

    }    
}