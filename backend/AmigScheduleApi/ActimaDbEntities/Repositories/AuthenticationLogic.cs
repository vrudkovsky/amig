using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.Exceptions;
using ActimaDbEntities.IServices;
using AMI.Actima.Modules.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace ActimaDbEntities.Services
{
    public class AuthenticationLogic: IAuthentication
    {
        private readonly IConfiguration _configuration;
        private readonly ActimaContext _context;
        private readonly HttpContext _httpContext;
        private readonly IDepartmentLogic _departmentLogic;
        

        public AuthenticationLogic(IConfiguration configuration, IHttpContextAccessor httpContext, ActimaContext context,
            IDepartmentLogic departmentLogic)
        {
            _configuration = configuration;
            _context = context;
            _departmentLogic = departmentLogic;
            _httpContext = httpContext.HttpContext;
        }
        public async Task<List<Claim>> GetClaimsForCookies(string login, string password)
        {
            var responseUserId   = ActimaAuthCheck
                .LoginUser(_configuration["Data:ConnectionStrings:ActimaDb"],
                    login, password);
            if (responseUserId <= 0)
            {
                switch (responseUserId)
                {
                    case (int) LoginResult.UserDisabled:
                        _httpContext.Response.Headers.Add("WWW-Authenticate", "User disabled");
                        break;
                    case (int) LoginResult.Unknown:
                        _httpContext.Response.Headers.Add("WWW-Authenticate", "Unknown");
                        break;
                    case (int) LoginResult.IncorrectPassword:
                        _httpContext.Response.Headers.Add("WWW-Authenticate", "Incorrect password");
                        break;
                    case (int) LoginResult.IncorrectUserName:
                        _httpContext.Response.Headers.Add("WWW-Authenticate", "Incorrect Login");
                        break;
                    case (int) LoginResult.IncorrectConnectionString:
                        const string errorMessage = "Db with connection string as hasn't Users table";
                        Log.Fatal(errorMessage);
                        throw new InternalServerErrorException(errorMessage);
                    default :
                        break;
                }
                throw new  UnauthorizedException();
            }
            
            var user = await GetUserById(responseUserId);
            if (user != null)
            {
                var companyDepartment = await _departmentLogic.GetCompanyAsDepartmentWithSubDepartments();
                var companyDepartmentId = companyDepartment.DepartmentId;
                var subDepartmentsIds = companyDepartment.InverseRoot
                    .Select(departments => departments.DepartmentId);
                var departmentsRightsIds = user.UserDepartmentRights.Select(rights => rights.DepartmentId).ToList();
                
                if( departmentsRightsIds.Count() >= 3 && departmentsRightsIds.Contains(companyDepartmentId) &&
                    subDepartmentsIds.Any(i => departmentsRightsIds.Contains(i)))
                    return new List<Claim>
                    {
                        new Claim("Id", user.UserId.ToString()),
                        new Claim("DepartmentId",
                            JsonConvert.SerializeObject(departmentsRightsIds)),
                        new Claim(ClaimTypes.Hash, user.UserPassword)
                    };
                var departmentsIdsTroubleMessage = $"User was found, but have invalid department rights";
                _httpContext.Response.Headers.Add("WWW-Authenticate", departmentsIdsTroubleMessage);
                Log.Warning(departmentsIdsTroubleMessage);
                throw new UnauthorizedException();
            }

            var message = string.Format("Cannot LogIn user with Login : {0}, because user disabled", login);
            _httpContext.Response.Headers.Add("WWW-Authenticate", message);
            Log.Information(message);
            throw new UnauthorizedException();

        }

        public async Task<Users> GetUserById(int userId)
        {
            var user = await _context.Users
                .AsNoTracking()
                .Include(users =>users.UserDepartmentRights )
                .FirstOrDefaultAsync(users => users.UserId == userId  &&  users.Active == 1);
            if (user is null)
                return null;
            user.UserDepartmentRights = user.UserDepartmentRights.Where(rights => rights.Enabled == 1)
                .OrderBy(rights =>rights.DepartmentId)
                .ToList();
            return user;
        }


        public List<int> GetDepartmentsListFromClaims(List<Claim> claims)
        {
            var allowedDepartmentsJson = claims.FirstOrDefault(user => user.Type == "DepartmentId")?.Value;

            return JsonConvert.DeserializeObject<int[]>(allowedDepartmentsJson).ToList();
        }
    }
}