using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.IServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ActimaDbEntities.Services
{
    public sealed class TimeNormatives : INormatives
    {
        private readonly ActimaContext _context;
        public TimeNormatives(ActimaContext context)
        {
            _context = context;
        }

        public async Task<List<AbsenceSchedule>> GetAllAbsenceForDepartment(DateTime day, string departmentCode)
        {
            var montStartDate = new DateTime(day.Year,day.Month,1);
            return await _context.EmployeesDetails
                .Where(details => details.Department.Code == departmentCode &&
                                  details.StartDate <= day && 
                                  details.EndDate >= day)
                .SelectMany(details => details.Employee.AbsenceSchedule.Where(schedule =>
                    schedule.ScheduleDate >= montStartDate && schedule.ScheduleDate <= day))
                .Include(schedule =>schedule.AbsenceCause)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<AbsenceSchedule>> GetAll4MonthAbsenceForDepartment(DateTime day, string departmentCode)
        {
            //-day if we retrive day.Day!=1
            return await GetAllAbsenceForDepartment(day.Date.AddMonths(1).AddDays(-day.Day), departmentCode);
        }

        public async Task<List<AbsenceSchedule>> GetAbsenceForEmployeeFromMonthStart(DateTime day, string departmentCode, int employeeId)
        {
            var monthStartDay = new DateTime(day.Year,day.Month,1);
            return await _context.EmployeesDetails
                .Where(details => details.Department.Code == departmentCode &&
                                  details.StartDate <= day &&
                                  details.EndDate >= day)
                .SelectMany(details => details.Employee.AbsenceSchedule.Where(schedule =>
                    schedule.ScheduleDate >= monthStartDay &&
                    schedule.ScheduleDate <= day &&
                    schedule.Employee.EmployeeId == employeeId))
                .Include(schedule => schedule.AbsenceCause)
                .AsNoTracking()
                .ToListAsync();
        }
        public async Task<List<WorkTimeNormatives>> GetWorkTimeNormativesForDepartment(DateTime monthDate,
            string departmentCode)
        {
            return await _context.WorkTimeNormatives
                .Where(normatives => normatives.Date == monthDate &&
                                     normatives.Record.Department.Code == departmentCode)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<WorkTimeNormatives>> GetWorkTimeNormatives4MonthForDepartment(DateTime monthDate,
            string departmentCode)
        {
            var monthStart = monthDate.Date.AddDays(1 - monthDate.Day);
            var monthEnd = monthStart.AddMonths(1).AddDays(-1);
            return await _context.WorkTimeNormatives
                .Where(normatives => normatives.Date >= monthStart &&
                                     normatives.Date <= monthEnd &&
                                     normatives.Record.Department.Code == departmentCode)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Dictionary<int?, Tuple<double, int>>> GetDepartmentNormativesForMonth(DateTime date)
        {
            return await _context.Normatives
                .Where(norm => norm.NormMonth.Equals(date.Date.Month) && norm.NormYear.Equals(date.Date.Year))
                .Select(normatives =>
                new KeyValuePair<int?, Tuple<double,int>>(normatives.ShiftRegimeID, 
                new Tuple<double, int>(normatives.NormHours, normatives.NormAppearTimes)))
                .ToDictionaryAsync(normatives => normatives.Key, normatives => normatives.Value);
        }

        public async Task<WorkTimeNormatives> GetWorkTimeNormativesForEmployee(DateTime monthDate,
            int recordId)
        {
            return await _context.WorkTimeNormatives
                .AsNoTracking()
                .FirstOrDefaultAsync(normatives => normatives.Date == monthDate &&
                                                   normatives.RecordId == recordId);

        }

        public async Task<List<Holidays>> GetHolidaysForYear(int year)
        {
            return await _context.Holidays
                .Where(holidays => holidays.HolidayDate.Year == year)
                .ToListAsync();
        }

        public async Task<List<Holidays>> GetHolidaysForMonth(DateTime monthDate)
        {
            return await _context.Holidays
                .Where(holidays => holidays.HolidayDate.Year == monthDate.Year 
                                   && holidays.HolidayDate.Month == monthDate.Month)
                .ToListAsync();
        }

        public async Task<List<int>> GetHolidaysForMonthToIntList(DateTime monthDate)
        {
            var holidays = await GetHolidaysForMonth(monthDate);
            return await Task.FromResult(holidays.Select(x => x.HolidayDate.Day).ToList());
        }


        public async Task<bool> NextDayIsHoliday(DateTime date)
        {
            return await _context.Holidays
                .AsNoTracking()
                .AnyAsync(holidays => holidays.HolidayDate == date.AddDays(1));
        }
    }
}