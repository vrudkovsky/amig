using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.Exceptions;
using ActimaDbEntities.IServices;
using Microsoft.EntityFrameworkCore;
using static ActimaDbEntities.Services.EmployeeDetailsLogic;


namespace ActimaDbEntities.Services
{
    public sealed class TimeTablesLogic : ITimeTablesLogic
    {
        private int[] _mainTime = { 1, 2 };
        private int[] _overtime = { 3, 4 };
        private readonly ActimaContext _context;
        private const int MainScheduleTypeId = 1;
        private const int SecondaryScheduleTypeId = 2;
        private const int MainOvertimeScheduleTypeId = 3;
        private const int SecondaryOvertimeScheduleTypeId = 4;

        public TimeTablesLogic(ActimaContext context)
        {
            _context = context;
        }

        private IQueryable<Timetable> GetTimeTablesToMonthLastDayQueryable(int allTimeTableId, DateTime day)
        {
            var firstMonthDay = new DateTime(day.Year, day.Month, 1);
            return _context.Timetable
                .Where(timetable => timetable.TimetableId == allTimeTableId &&
                                    timetable.Date.Month == firstMonthDay.Month &&
                                    timetable.Date.Year == firstMonthDay.Year)
                .AsNoTracking()
                .AsQueryable();
        }

        private async Task<List<Timetable>> GetOvertimeTimetablesForSpecificPeriod(DateTime start, DateTime end)
        {
            return await _context.AllTimetables.AsNoTracking()
                .Where(timetables => timetables.TypeId == MainOvertimeScheduleTypeId &&
                                     timetables.TimetablesDate >= start &&
                                     timetables.TimetablesDate <= end)
                .SelectMany(timetables => timetables.Timetable)
                .ToListAsync();
        }

        public async Task<List<Timetable>> GetTimetablesForOvertimeOverYear(int allTimeTableId, DateTime year)
        {
            var monthDate = new DateTime(year.Year, year.Month, 1);
            var lastMonthDate = year.Month.Equals(1) ? (DateTime?)null : monthDate.AddMonths(-1);
            var timetablesToDate = await GetTimetablesForMonth(allTimeTableId, year);
            if (lastMonthDate != null)
            {
                var firstDayOfYear = new DateTime(year.Year, 1, 1);
                var mainTimetablesSinceYearStartToDate =
                    await GetOvertimeTimetablesForSpecificPeriod(firstDayOfYear, lastMonthDate.Value);
                timetablesToDate.AddRange(mainTimetablesSinceYearStartToDate);
                const int maxMonthCount = 12;
                if (lastMonthDate.Value.Month != maxMonthCount)
                {
                    var monthAfter = monthDate.AddMonths(1);
                    const int maxDaysInDecember = 31;
                    var lastYearDay = new DateTime(monthDate.Year, maxMonthCount, maxDaysInDecember);
                    var mainTimetablesAfterRequiredMonth =
                        await GetOvertimeTimetablesForSpecificPeriod(monthAfter, lastYearDay);
                    timetablesToDate.AddRange(mainTimetablesAfterRequiredMonth);
                }
            }

            return timetablesToDate.OrderBy(timetable => timetable.Date).ToList();
        }

        public async Task<List<Timetable>> GetTimetablesForMonth(int allTimeTablesId, DateTime day)
        {
            return await GetTimeTablesToMonthLastDayQueryable(allTimeTablesId, day)
                .ToListAsync();
        }

        //        private IQueryable<Timetable> GetTimetablesToDate(int alltimeTableId, DateTime day)
        //        {
        //            var firstDay = new DateTime(day.Year,day.Month,1);
        //            return _context.Timetable
        //                .Where(timetable => timetable.TimetableId == alltimeTableId &&
        //                                    timetable.Date >= firstDay &&
        //                                    timetable.Date <= day)
        //                .AsNoTracking()
        //                .AsQueryable();
        //        }
        //
        //        public async Task<List<Timetable>> GetTimetablesToDateForEmployees(int allTimetable, DateTime day)
        //        {
        //            return await GetTimetablesToDate(allTimetable, day).ToListAsync();
        //        }
        //
        //        public async Task<List<Timetable>> GetTimetablesToDateForOneEmployee(int allTimetable, DateTime day, int employeeId)
        //        {
        //            return await GetTimetablesToDate(allTimetable, day).Where(timetable=>timetable.EmployeeId == employeeId)
        //                .ToListAsync();
        //        }

        public async Task<List<Timetable>> GetTimetablesForOvertimeForEmployeeToDate(int allTimeTableId, DateTime day,
            int employeeId, string departmentCode)
        {
            var timetables = await GetTimetablesForOvertimeOverYear(allTimeTableId, day);
            return timetables.Where(timetable => timetable.EmployeeId == employeeId).ToList();
        }

        public async Task<List<Timetable>> GetTimetablesForEmployeeToDate(int allTImeTAbleId,
            DateTime day, int employeeId, string departmentCode)
        {
            return await GetTimeTablesToMonthLastDayQueryable(allTImeTAbleId, day)
                .Where(timetable => timetable.EmployeeId == employeeId &&
                                    timetable.Employee.EmployeesDetails
                                        .Any(details => details.Department.Code == departmentCode))
                .ToListAsync();
        }

        public async Task<AllTimetables> GetAllTimeTables(DateTime monthDate, int branchId, int tableType,
            string timetableName)
        {
            return await _context.AllTimetables
                .AsNoTracking()
                .FirstOrDefaultAsync(timetables => timetables.TypeId == tableType &&
                                                   timetables.BranchId == branchId &&
                                                   timetables.TimetablesName == timetableName &&
                                                   timetables.TimetablesDate == monthDate);
        }

        public async Task<AllTimetables> GetAllTimeTables(int allTimeTableId)
        {
            return await _context.AllTimetables
                .AsNoTracking()
                .FirstOrDefaultAsync(timetables => timetables.TimetableId == allTimeTableId);
        }

        public async Task<AllTimetables> GetAllTimetableMainSchedule(int allTimeTableId)
        {
            return await _context.AllTimetables
                .AsNoTracking()
                .FirstOrDefaultAsync(timetables => timetables.TimetableId == allTimeTableId &&
                                                   timetables.TypeId.HasValue);
        }

        public async Task<AllTimetables> GetAllTimetablesForMonth(int allTimeTableId)
        {
            return await _context.AllTimetables.AsNoTracking()
                .Include(table => table.Timetable)
                .FirstOrDefaultAsync(timetables => timetables.TimetableId == allTimeTableId &&
                                                            timetables.TypeId.HasValue);
        }

        public async Task<List<Timetable>> GetTimeTablesForAllTimeTables(DateTime date, int allTimeTableId,
            List<int> employeeIds)
        {
            return await _context.Timetable.Where(timetable => timetable.TimetableId == allTimeTableId &&
                                                               timetable.Date == date &&
                                                               employeeIds.Contains(timetable.EmployeeId.Value))
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<IEnumerable<int>> GetAllTimeTablesTypeIds()
        {
            return await _context.TimetablesTypes.Select(types => types.TypeId).ToListAsync();
        }

        public async Task<AllTimetables> CreateNewAllTimetables(int branchId, DateTime monthDate, DateTime editDate,
            string name, int type)
        {
            var newAllTimeTables = new AllTimetables
            {
                BranchId = branchId,
                TimetablesDate = monthDate,
                EditDate = editDate,
                TimetablesName = name,
                TypeId = type
            };
            _context.AllTimetables.Add(newAllTimeTables);
            await SaveChangesAsync();
            return newAllTimeTables;
        }

        public async Task<AllTimetables> GetAlltimeTablesWithAllTables(int alltimetablesId)
        {
            return await _context.AllTimetables
                .Include(timetables => timetables.Timetable)
                .AsNoTracking()
                .FirstOrDefaultAsync(timetables => timetables.TimetableId == alltimetablesId);
        }

        private string IncrementCopyNumber(string allTimetableName)
        {
            var lastDigitIncremented = allTimetableName.Last() + 1;
            return allTimetableName.Substring(0, allTimetableName.Length - 1) + lastDigitIncremented;
        }

        public async Task<AllTimetables> CopyAllTimeTables(int allTimeableId, string name)
        {
            var existAlltimetable = await GetAlltimeTablesWithAllTables(allTimeableId);
            if (existAlltimetable == null)
                throw new BadRequestException($"alltimetable with id :{allTimeableId} isn`t exist");
            var typeId = existAlltimetable.TypeId == 1 || existAlltimetable.TypeId == 2
                ? 2
                : 4;

            string alltimetableCopyName = null;
            if (name == null)
            {
                var existAlltimetableName = existAlltimetable.TimetablesName.Trim();
                var copyAnnex = " - copy";
                var lastCharIsNumber = char.IsDigit(existAlltimetableName.Last());
                alltimetableCopyName =
                    existAlltimetableName.Contains(copyAnnex, StringComparison.InvariantCultureIgnoreCase) &&
                    lastCharIsNumber
                        ? IncrementCopyNumber(existAlltimetableName)
                        : existAlltimetableName + copyAnnex + 1;
            }

            var newAlltimetable = await CreateNewAllTimetables(existAlltimetable.BranchId,
                existAlltimetable.TimetablesDate, DateTime.Now, name ?? alltimetableCopyName, typeId);
            var tables = existAlltimetable.Timetable;
            if (tables.Any())
            {
                foreach (var table in tables)
                {
                    table.TimetableId = newAlltimetable.TimetableId;
                    table.RowId = 0;
                }

                _context.Timetable.AddRange(tables);
            }

            await SaveChangesAsync();
            return newAlltimetable;
        }

        public void AddTimeTable(Timetable timetable)
        {
            _context.Timetable.Add(timetable);
        }

        public void RemoveTimeTable(Timetable timetable)
        {
            _context.Timetable.Remove(timetable);
        }


        public void RemoveTimetable(IEnumerable<int> rowIds)
        {
            foreach (var rowId in rowIds)
            {
                var timeTable = new Timetable { RowId = rowId };
                _context.Timetable.Remove(timeTable);
            }

            ;
        }

        public async Task SaveChangesAsync()
        {
            if (_context.ChangeTracker.HasChanges())
                await _context.SaveChangesAsync();
        }

        public async Task RemoveAllTimeTables(int allTimeTablesId)
        {
            var allTimeTablesToRemove = new AllTimetables { TimetableId = allTimeTablesId };
            _context.AllTimetables.Remove(allTimeTablesToRemove);
            await SaveChangesAsync();
        }

        private async Task<AllTimetables> GetAlltimetablesByTypeIdMain(int typeId, DateTime mountsDate)
        {
            switch (typeId)
            {
                case MainScheduleTypeId:
                    return await _context.AllTimetables
                        .AsNoTracking()
                        .SingleOrDefaultAsync(timetables => timetables.TypeId == MainScheduleTypeId &&
                                                            timetables.TimetablesDate == mountsDate);
                case MainOvertimeScheduleTypeId:
                    return await _context.AllTimetables
                        .AsNoTracking()
                        .SingleOrDefaultAsync(timetables => timetables.TypeId == MainOvertimeScheduleTypeId &&
                                                            timetables.TimetablesDate == mountsDate);
                default:
                    return null;
            }
        }

        public async Task UpdateAllTimeTableWithReplaceForMainTypeId(int allTimetableId)
        {
            var requestedAllTimetables = await GetAllTimeTables(allTimetableId);
            if (requestedAllTimetables == null)
                throw new BadRequestException($"There isn`t allTimetables with id like {allTimetableId}");
            AllTimetables currentMainAlltimeTable;
            switch (requestedAllTimetables.TypeId)
            {
                case SecondaryScheduleTypeId:
                    {
                        currentMainAlltimeTable = await GetAlltimetablesByTypeIdMain(MainScheduleTypeId,
                            requestedAllTimetables.TimetablesDate);
                        if (currentMainAlltimeTable != null)
                            currentMainAlltimeTable.TypeId = SecondaryScheduleTypeId;
                        requestedAllTimetables.TypeId = MainScheduleTypeId;
                        break;
                    }

                case SecondaryOvertimeScheduleTypeId:
                    {
                        currentMainAlltimeTable = await GetAlltimetablesByTypeIdMain(MainOvertimeScheduleTypeId,
                            requestedAllTimetables.TimetablesDate);
                        if (currentMainAlltimeTable != null)
                            currentMainAlltimeTable.TypeId = SecondaryOvertimeScheduleTypeId;
                        requestedAllTimetables.TypeId = MainOvertimeScheduleTypeId;
                        break;
                    }

                default:
                    throw new UnprocessableEntityException($"AllTimetable with id like {allTimetableId} " +
                                                           "is main/main overtime schedule");
            }

            if (currentMainAlltimeTable != null)
                _context.AllTimetables.UpdateRange(requestedAllTimetables, currentMainAlltimeTable);
            else
                _context.AllTimetables.Update(requestedAllTimetables);
            await SaveChangesAsync();
        }

        public async Task<List<AllTimetables>> GetAllTimetablesForMonth(DateTime monthDate, int branchId)
        {
            return await _context.AllTimetables
                .Where(timetables => timetables.TimetablesDate == monthDate &&
                                     timetables.TypeId.HasValue &&
                                     timetables.BranchId == branchId)
                .ToListAsync();
        }

        public void UpdateAllTimeTable(AllTimetables allTimetables)
        {
            _context.AllTimetables.Update(allTimetables);
        }


        public async Task<List<EmployeeShiftData>> GetEmployeesTimeTable(int employeeId, int timeTableId)
        {
            var list = await (from timeTable in _context.Timetable
                              where employeeId == timeTable.EmployeeId && timeTable.TimetableId == timeTableId
                              select new EmployeeShiftData
                              { ShiftDate = timeTable.Date, ShiftEnd = timeTable.ShiftEnd, ShiftStart = timeTable.ShiftStart }).ToListAsync();
            if (list.Count() == 0)
                return null;
            return list;
        }

        public async Task<List<EmployeeShiftData>> GetMonthPlanningHoursForDay(int month, int groupId, int timetableId, int departmentId, string nameGroup)
        {
            return await (from timetable in _context.Timetable
                          join employeesDetails in _context.EmployeesDetails on timetable.EmployeeId equals employeesDetails.EmployeeId
                          join groups in _context.EmployeeGroups2Employee on employeesDetails.RecordId equals groups.RecordId
                          join employeeGroups in _context.EmployeeGroups on groups.EmployeeGroupId equals employeeGroups.EmployeeGroupId
                          join department in _context.Departments on employeesDetails.DepartmentId equals department.DepartmentId
                          where timetable.Date.Month == month && employeeGroups.EmployeeGroupId == groupId && timetable.TimetableId == timetableId 
                          && department.DepartmentId == departmentId && timetable.EmployeeId == employeesDetails.EmployeeId && employeeGroups.Name == nameGroup
                                  select new EmployeeShiftData { ShiftDate = null, ShiftEnd = timetable.ShiftEnd, ShiftStart = timetable.ShiftStart }).ToListAsync();
        }

        public async Task<string> GetTimetableName(int id)
        {
            var name =
                await (from timetable in _context.AllTimetables
                       where timetable.TimetableId == id
                       select timetable.TimetablesName
                          ).FirstAsync();
            return name;
        }

        public async Task<List<EmployeeShiftData>> GetUnflexibleEmployeeShift(int employeeId, int month)
        {
            return await (from shiftScheduleWithTemplate in _context.ShiftScheduleWithTemplate
                          join shifts in _context.Shifts on shiftScheduleWithTemplate.ShiftId equals shifts.ShiftId
                          where shiftScheduleWithTemplate.EmployeeId == employeeId  && shiftScheduleWithTemplate.ScheduleDate.Month == month
                          select new EmployeeShiftData { ShiftStart = shifts.ShiftStart, ShiftEnd = shifts.ShiftEnd, ShiftDate = shiftScheduleWithTemplate.ScheduleDate }).ToListAsync();
        }

    }
} 