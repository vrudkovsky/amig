using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using ActimaDbEntities.Services;
using AmigScheduleApi.Models;
using static ActimaDbEntities.Services.EmployeeDetailsLogic;

namespace ActimaDbEntities.IServices
{
    public interface IEmployeeDetailsLogic
    {
        Task<List<EmployeesDetails>> GetEmployeesByDepartment(int departmentId,
            DateTime date);
        List<EmployeesDetails> GetEmployeeByGroup(int groupId, IEnumerable<EmployeesDetails> employees);
        Task<Dictionary<int, string>> GetDictionaryForGroupNamesWoTracking (int groupId = -1);
        Task<EmployeesDetails> GetEmployeeDetails(DateTime date, int employeeId);
        Task<Employees> GetEmployee(int employeeId);
        Task<List<EmployeesDetails>> GetFlexibleEmployeesByIdsCollection(List<int> employeeIds,
            int? branchId, DateTime? date);
        Task<List<EmployeesDetails>> GetEmployeesByDepartment4Month(int departmentId,
            DateTime monthDate);
        Task<List<SimpleEmployeeJsonData>> GetSimpleEmployeeInfo(List<int> subDepartmentsIds, DateTime date);
        Task<string> GetEmployeeProffesionById(int id);
        Task<List<int>> GetEmployeesIdWithShortDays();
        Task<List<EmployeeShiftData>> GetIntervalsFromDateTime(int id, DateTime dateTime);
        Task<Tuple<int, int, string>> GetDepartIdAndGroupIdFromEmployee(int empId);
        Task<List<int>> GetRecordIdFromEmployeeId(int empId);
        Task<List<Tuple<int, int>>> GetEmployeeAbsenceSchedule(int month);
    }
}