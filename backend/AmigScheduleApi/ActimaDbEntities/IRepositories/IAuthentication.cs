using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;

namespace ActimaDbEntities.IServices
{
    public interface IAuthentication
    {
        Task<List<Claim>> GetClaimsForCookies(string login, string password);
        Task<Users> GetUserById(int userId);
        List<int> GetDepartmentsListFromClaims(List<Claim> claims);
    }
}