
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;
using static ActimaDbEntities.Services.EmployeeDetailsLogic;

namespace ActimaDbEntities.IServices
{
    public interface ITimeTablesLogic
    {
        Task<List<Timetable>> GetTimetablesForOvertimeOverYear(int allTimeTableId, DateTime day);
        Task<List<Timetable>> GetTimetablesForMonth(int allTimeTableId, DateTime day);
        
        Task<List<Timetable>> GetTimetablesForOvertimeForEmployeeToDate(int allTimeTableId, DateTime day, 
            int employeeId, string departmentCode);

        Task<List<Timetable>> GetTimetablesForEmployeeToDate(int allTimeTableId, DateTime day, 
            int employeeId, string departmentCode);

        Task<AllTimetables> GetAllTimeTables(DateTime monthDate, int branchId, int tableType,
            string timetableName);

        Task<AllTimetables> GetAllTimeTables(int timeTableId);
        Task<AllTimetables> GetAllTimetableMainSchedule(int allTimeTableId);

        Task<List<AllTimetables>> GetAllTimetablesForMonth(DateTime monthDate, int branchId);
        Task<AllTimetables> GetAllTimetablesForMonth(int alltimetableId);

        Task<List<Timetable>> GetTimeTablesForAllTimeTables(DateTime date, int allTimeTableId,
            List<int> employeeIds);

        Task<IEnumerable<int>> GetAllTimeTablesTypeIds();
        
        Task<AllTimetables> CreateNewAllTimetables(int branchId, DateTime monthDate, DateTime editDate,
            string name, int type);

        void AddTimeTable(Timetable timetable);
        Task<AllTimetables> CopyAllTimeTables(int allTimeableId, string name);
        Task<AllTimetables> GetAlltimeTablesWithAllTables(int alltimetablesId);
        void RemoveTimetable(IEnumerable<int> rowIds);
        Task SaveChangesAsync();
        Task RemoveAllTimeTables(int allTimeTablesId);
        Task UpdateAllTimeTableWithReplaceForMainTypeId(int allTimetableId);
        void UpdateAllTimeTable(AllTimetables allTimetables);
        Task<List<EmployeeShiftData>> GetEmployeesTimeTable(int employeeId, int timeTableId);
        Task<string> GetTimetableName(int id);
        Task<List<EmployeeShiftData>> GetUnflexibleEmployeeShift(int employeeId, int month);
    }
}