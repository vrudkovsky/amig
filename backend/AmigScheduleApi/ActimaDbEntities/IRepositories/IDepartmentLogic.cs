using System.Collections.Generic;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;

namespace ActimaDbEntities.IServices
{
    public interface IDepartmentLogic
    {
        Task<List<int>> GetBranchIds();
        bool DepartmentIsBranch(Departments branch);
        Task<Departments> GetDepartment(int departmentId);
        Task<List<Departments>> GetSubDepartments(int branchId, int departmentId = 0);
        Task<Departments> GetBranchWithSubDepartments(int branchId);
        Task<Departments> GetCompanyAsDepartmentWithSubDepartments();
        Task<Departments> GetDepartmentByCode(string statusCode);
    }
}