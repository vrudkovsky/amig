using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ActimaDbEntities.Entities;

namespace ActimaDbEntities.IServices
{
    public interface INormatives
    {                             
        Task<List<AbsenceSchedule>> GetAllAbsenceForDepartment(DateTime day, string departmentCode);
        Task<List<AbsenceSchedule>> GetAll4MonthAbsenceForDepartment(DateTime day, string departmentCode);
        Task<List<AbsenceSchedule>> GetAbsenceForEmployeeFromMonthStart(DateTime day, string departmentCode,
            int employeeId);
        Task<List<WorkTimeNormatives>> GetWorkTimeNormativesForDepartment(DateTime monthDate, string departmentCode);
        Task<List<WorkTimeNormatives>> GetWorkTimeNormatives4MonthForDepartment(DateTime monthDate,
            string departmentCode);
        Task<WorkTimeNormatives> GetWorkTimeNormativesForEmployee(DateTime monthDate,
            int recordId);
        Task<List<Holidays>> GetHolidaysForYear(int year);
        Task<List<Holidays>> GetHolidaysForMonth(DateTime monthDate);
        Task<List<int>> GetHolidaysForMonthToIntList(DateTime monthDate);
        Task<bool> NextDayIsHoliday(DateTime date);
        Task<Dictionary<int?, Tuple<double, int>>> GetDepartmentNormativesForMonth(DateTime date);
    }
}