using System;

namespace ActimaDbEntities.Exceptions
{
    public class NoContentException:Exception
    {
        public NoContentException(string message) : base(message)
        {
            
        }
        public NoContentException() 
        {

        }
    }
}